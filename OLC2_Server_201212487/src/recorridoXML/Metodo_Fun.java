/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recorridoXML;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 *
 * @author abautista
 */
public class Metodo_Fun {
    public String Nombre_MetodoFun;
    public ArrayList<Parametro> LstParametros_MetodoFun;
//    public Hashtable HashParametros_MetodoFun;
    public String Codigo_MetodoFun;
    public String Retorno_MetodoFun;

    public Metodo_Fun(String Nombre_MetodoFun, ArrayList<Parametro> LstParametros_MetodoFun, String Codigo_MetodoFun, String Retorno_MetodoFun) {
        this.Nombre_MetodoFun = Nombre_MetodoFun;
        this.LstParametros_MetodoFun = LstParametros_MetodoFun;
//        this.HashParametros_MetodoFun = LstParametros_MetodoFun;
        this.Codigo_MetodoFun = Codigo_MetodoFun;
        this.Retorno_MetodoFun = Retorno_MetodoFun;
    }
    
    
}
