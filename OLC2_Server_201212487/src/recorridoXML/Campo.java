/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recorridoXML;

import java.util.ArrayList;

/**
 *
 * @author abautista
 */
public class Campo {
    public String Tipo_campo;
    public String Nombre_Campo;
    public ArrayList<String> Propiedades;

    public Campo(String Tipo_campo, String Nombre_Campo, ArrayList<String> Propiedades) {
        this.Tipo_campo = Tipo_campo;
        this.Nombre_Campo = Nombre_Campo;
        this.Propiedades = Propiedades;
    }
    
    
}
