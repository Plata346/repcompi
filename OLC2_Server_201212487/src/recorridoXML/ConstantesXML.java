/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recorridoXML;

import ejecutarUSQL.ConstantesUSQL;
import java.util.ArrayList;

/**
 *
 * @author Plata
 */
public class ConstantesXML {
    
    public static final Maestro_BD BaseDatosGeneral = new Maestro_BD();
    
    public static final int TipoNumero = 1;
    public static final int TipoDouble = 2;
    public static final int TipoCadena = 3;
    public static final int TipoFecha = 4;
    public static final int TipoFechaHora = 5;
    public static final int TipoBooleano = 6;
    
        
    public static String verdadero = "verdadero";
    public static String falso = "falso";
    
    public static ArrayList<errorXML> LstErrores = new ArrayList<errorXML>();
    
    public static String pathArchivos = "C:\\compi2\\repcompi\\ARCHIVOS\\";
    public static boolean EsCondicion = false;
    
    public static void AddError(String Tipo, String descripcion, int linea, int columna)
    {
        LstErrores.add(new errorXML(Tipo,descripcion,linea,columna));
        ConstantesUSQL.CadenaMsj += "Error -> "+Tipo+" Descripcion -> "+descripcion+" Linea: "+linea+" Col: "+columna+" "+ConstantesUSQL.Separador; 
        ConstantesUSQL.ultimoResultado += "Error -> "+Tipo+" Descripcion -> "+descripcion; 
    
    }
}
