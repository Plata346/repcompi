/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recorridoXML;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 *
 * @author abautista
 */
public class Base_Datos {
    
    public String Nombre_BD;
    public String Path_BD;
    public ArrayList<String> Usuarios_BD;
    //public ArrayList<Metodo_Fun> LstMetodos_Fun;
    public Hashtable HashMetodos_Fun;
    //public ArrayList<Objeto> LstObjetos;
    public Hashtable HashObjetos;
    //public ArrayList<Tabla> LstTablas;
    public Hashtable HashTablas;
    
    public Base_Datos(String nombre, String path, ArrayList<String> usuarios)
    {
        this.Nombre_BD = nombre;
        this.Path_BD = path;
        this.Usuarios_BD = usuarios;
        //this.LstMetodos_Fun = new ArrayList<Metodo_Fun>();
        HashMetodos_Fun = new Hashtable();
        //this.LstObjetos = new ArrayList<Objeto>();
        this.HashObjetos = new Hashtable();
        //this.LstTablas = new ArrayList<Tabla>();
        this.HashTablas = new Hashtable();
    }
}
