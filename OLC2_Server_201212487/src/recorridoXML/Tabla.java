/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recorridoXML;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 *
 * @author abautista
 */
public class Tabla {
    public String Nombre_Tabla;
    public String Path_Tabla;
    public ArrayList<Tupla> LstTuplas_Tabla;
//    public Hashtable HashTuplas_Tabla;
    public ArrayList<Campo> LstCampos_Tabla;
//    public Hashtable HashCampos_Tabla;
    
    public Tabla(String nombre, String path,ArrayList<Campo> listaCampos, ArrayList<Tupla> registros)
    {
        this.Nombre_Tabla = nombre;
        this.Path_Tabla = path;
        this.LstCampos_Tabla = listaCampos;
//        this.HashCampos_Tabla = listaCampos;
        this.LstTuplas_Tabla = registros;
//        this.HashTuplas_Tabla = registros;
    }
}
