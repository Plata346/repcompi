/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recorridoXML;

import analizador.Analizador;
import analizador.AnalizadorTreeConstants;
import ejecutarUSQL.ConstantesUSQL;
import ejecutarUSQL.ElementoExp;
import ejecutarUSQL.ejecucion;
import interpreteXML.AnalizadorXML;
import interpreteXML.AnalizadorXMLTreeConstants;
import interpreteXML.SimpleNode;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import olc2_server_201212487.Handler_XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author Plata
 */
public class recorridoXML {
    
    public void ejecutarXML(SimpleNode nodo){
        
        String  nombreNodo = AnalizadorXMLTreeConstants.jjtNodeName[nodo.id];
        
        switch (nombreNodo){
            case "INICIO":
                ejecutarXML((SimpleNode)nodo.children[0]);
                break;
            case "TIPOS_ARCHIVOS":
                
                ejecutarXML((SimpleNode)nodo.children[0]);  
                
                break;
            case "ARCHIVO_MAESTRO":
                for(int i = 0; i<nodo.children.length;i++)
                {
                    SimpleNode baseDatos = (SimpleNode)nodo.children[i];
                    CargarBD_Completa((SimpleNode)baseDatos);  
                }
                break;
                
        }
    }
    
    public void CargarBD_Completa( SimpleNode nodoBDmaster)
    {
        SimpleNode NodoNombre = (SimpleNode)nodoBDmaster.children[0];
        SimpleNode NodoPath = (SimpleNode)nodoBDmaster.children[1];
        SimpleNode NodoUsuarios = (SimpleNode)nodoBDmaster.children[2];        
        
        if(VerificarArchivoBaseDatos(NodoPath.name.replace("<![CDATA[", "").replace("]]>", "")))
        {
            //crear nueva Base de datosMaestra
            Base_Datos BD = new Base_Datos(NodoNombre.name,NodoPath.name.replace("<![CDATA[", "").replace("]]>", ""),NumerarUsuarios(NodoUsuarios));
            //cargar la informacion de esa base de datos
            
            //BD.LstMetodos_Fun = ExtraerMetosFunciones(BD.Path_BD);
            BD.HashMetodos_Fun = ExtraerMetosFunciones(BD.Path_BD);
            
            //BD.LstObjetos = ExtraerObjetos(BD.Path_BD);
            BD.HashObjetos = ExtraerObjetos(BD.Path_BD);
            
            //BD.LstTablas = ExtraerTablas(BD.Path_BD);
            BD.HashTablas = ExtraerTablas(BD.Path_BD);
            
            //ConstantesXML.BaseDatosGeneral.LstBasesSistema.add(BD);
            
            //prueba hashtable
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(NodoNombre.name, BD);
        }else
        {
            //ERROR CARGA BASE DE DATOS POR RUTA INCORRECTA
            ConstantesXML.AddError("XML Maestro", "No se puede cargar el archivo para BD: "+NodoNombre.name +"\n el archivo no existe", NodoPath.linea, NodoPath.columna);
        }
        
        
    }
    
    //metood que verifica si el archivo existe en la ruta especificada
    public boolean VerificarArchivoBaseDatos(String ruta)
    {
        try {
            FileReader f = new FileReader(ruta);
            return true;
        }catch(Exception e) {
            return false;
        }
    }
    
    //metodo que retorna el texto del archivo XML
    public String LeerArchivo(String Path)
    {
        String aux,cadena="";
        try {
            FileReader f = new FileReader(Path);
            BufferedReader b = new BufferedReader(f);
            while((aux = b.readLine())!=null) {
                //System.out.println(aux);
                cadena += aux +"\n";
            }
            b.close();
            
            //System.out.println(cadena);
            return cadena;            
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se pudo leer el archivo", "ERROR", JOptionPane.ERROR_MESSAGE);                       
            return cadena;
        }
    }
    
    //metodo que retorna los usuarios separados por coma
    public ArrayList<String> NumerarUsuarios(SimpleNode nodoUsrs)
    {
        ArrayList<String> Users = new ArrayList<String>();
        
        for (int i = 0; i < nodoUsrs.children.length; i++) {
                SimpleNode USrAux = (SimpleNode) nodoUsrs.children[i];
                Users.add(USrAux.name);
        }
        return Users;
    }
    
    //metodo que retorna la lista de Procedimientos y funciones
    //public ArrayList<Metodo_Fun> ExtraerMetosFunciones(String Path)
    public Hashtable ExtraerMetosFunciones(String Path)        
    {
//        ArrayList<Metodo_Fun> lst = new ArrayList<Metodo_Fun>();
        Hashtable lst = new Hashtable();
        
        //mandamos a leer el archivo registro
        String str_XMLRegistro = LeerArchivo(Path);        
        //obtenermos en nodo raiz del archivo registro analizado
        SimpleNode raizRegistro = ObtererRaizXML(str_XMLRegistro);
        //obtenemro unicamente el path donde estan alojdos los metodos y funciones
        String pathMetodos = getPathMetodosFun(raizRegistro);
        
        if(pathMetodos!="" && VerificarArchivoBaseDatos(pathMetodos))
        {
            //leemos archivo de metodos y funciones y obtenemos su raiz
            String str_XMLMetodoFunciones = LeerArchivo(pathMetodos);        
            SimpleNode raizMetodoFunciones = ObtererRaizXML(str_XMLMetodoFunciones);
            //obtener nodo Archivo Metodo y recorrer cada hijo
            //esto retornara un objeto tipo Metodo  que se agregara a lst
            SimpleNode Tipo_Archivo = (SimpleNode)raizMetodoFunciones.children[0];
            SimpleNode ArchivoMetodos = (SimpleNode)Tipo_Archivo.children[0];
            
            if( ArchivoMetodos.children != null)
            {
                for (int i = 0; i < ArchivoMetodos.children.length; i++) {
                    SimpleNode ProFun = (SimpleNode)ArchivoMetodos.children[i];

                    Metodo_Fun MF = ExtraerMetodo(ProFun);
                    if(MF!=null)
                    {
                        //lst.add(MF);
                        lst.put(MF.Nombre_MetodoFun, MF);
                    }                
                }
            }
            
            
            
            return lst;
        }else
        {
           return lst; 
        }                
    }
    
    //meoto que retorna lista de objetos
//    public ArrayList<Objeto> ExtraerObjetos(String Path)
    public Hashtable ExtraerObjetos(String Path)
    {
//        ArrayList<Objeto> lst = new ArrayList<Objeto>();
        Hashtable lst = new Hashtable();
        
        //mandamos a leer el archivo registro
        String str_XMLRegistro = LeerArchivo(Path);        
        //obtenermos en nodo raiz del archivo registro analizado
        SimpleNode raizRegistro = ObtererRaizXML(str_XMLRegistro);
        //obtenemro unicamente el path donde estan alojdos los objetos
        String pathObjetos = getPathObjetos(raizRegistro);
        
        if(pathObjetos!="" && VerificarArchivoBaseDatos(pathObjetos))
        {            
            String str_XMLMetodoObjetos = LeerArchivo(pathObjetos);        
            SimpleNode raizObjetos = ObtererRaizXML(str_XMLMetodoObjetos);
                        
            SimpleNode Tipo_Archivo = (SimpleNode)raizObjetos.children[0];
            SimpleNode ArchivoObjetos = (SimpleNode)Tipo_Archivo.children[0];                        
            
            if(ArchivoObjetos.children!= null){
                for (int i = 0; i < ArchivoObjetos.children.length; i++) {
                    SimpleNode Obj = (SimpleNode)ArchivoObjetos.children[i];

                    SimpleNode NombreObj = (SimpleNode)Obj.children[0];
                    SimpleNode NodoParametrosObj = (SimpleNode)Obj.children[1];

                    ArrayList<Parametro> LstParamMF = new ArrayList<Parametro>();
//                    Hashtable LstParamMF = new Hashtable();

                    if(NodoParametrosObj.children != null)
                    {
                       for (int n = 0; n < NodoParametrosObj.children.length; n++) {

                            SimpleNode ParamObj = (SimpleNode) NodoParametrosObj.children[n];

                            SimpleNode Tipo = (SimpleNode) ParamObj.children[0];
                            SimpleNode TipoParam = (SimpleNode) Tipo.children[0];

                            SimpleNode nombreParam = (SimpleNode) ParamObj.children[1];

                            LstParamMF.add(new Parametro(nombreParam.name, TipoParam.name));
//                              LstParamMF.put(nombreParam.name,new Parametro(nombreParam.name, TipoParam.name));
                        } 
                    }
                    
//                        lst.add(new Objeto(NombreObj.name, LstParamMF));
                        lst.put(NombreObj.name,new Objeto(NombreObj.name, LstParamMF));

                }
                
            }
                        
            
            return lst;
        }else
        {
           return lst; 
        }                
    }
    
    //metood que retorna las tablas de una base de datos
    
//    public ArrayList<Tabla> ExtraerTablas(String Path)
    public Hashtable ExtraerTablas(String Path)
    {
        //ArrayList<Tabla> lst = new ArrayList<Tabla>();
        Hashtable lst  = new Hashtable();
        
        //mandamos a leer el archivo registro
        String str_XMLRegistro = LeerArchivo(Path);        
        //obtenermos en nodo raiz del archivo registro analizado
        SimpleNode raizRegistro = ObtererRaizXML(str_XMLRegistro);
        
        SimpleNode Tipo_Archivo = (SimpleNode)raizRegistro.children[0];
        SimpleNode ArchivoRegistro = (SimpleNode)Tipo_Archivo.children[0];
        
        //se obtiene el nodo de las tablas
        SimpleNode NodoTablas = (SimpleNode)ArchivoRegistro.children[2];
        
        if(NodoTablas.children != null)
        {
            //para cada tabla retornar un objeto tipo tabla
            for (int i = 0; i < NodoTablas.children.length; i++) {
                SimpleNode Tabla = (SimpleNode)NodoTablas.children[i];
                
                SimpleNode NombreTabla = (SimpleNode)Tabla.children[0];
                SimpleNode PathRegistrosTabla = (SimpleNode)Tabla.children[1];                
                SimpleNode NodoCamposTabla = (SimpleNode)Tabla.children[2];
                
                ArrayList<Campo> LstCamposTabla = new ArrayList<Campo>();                
//                Hashtable LstCamposTabla = new Hashtable();                
                LstCamposTabla = ExtraerCamposTabla(NodoCamposTabla);
                
                ArrayList<Tupla> lsttuplas = new ArrayList<Tupla>();
//                Hashtable lsttuplas = new Hashtable();
                
                if(VerificarArchivoBaseDatos(PathRegistrosTabla.name.replace("<![CDATA[", "").replace("]]>", "")))
                {
                    lsttuplas = ExtraerTuplasTabla(PathRegistrosTabla.name.replace("<![CDATA[", "").replace("]]>", ""));
                }else
                {
                    //ERROR CARGA BASE DE DATOS POR RUTA INCORRECTA
                    ConstantesXML.AddError("XML TABLA", "No se puede cargar el archivo para Tabla: "+NombreTabla.name +"\n el archivo no existe", NombreTabla.linea, NombreTabla.columna);
                }
                
                
//                lst.add(new Tabla(NombreTabla.name, PathRegistrosTabla.name.replace("<![CDATA[", "").replace("]]>", ""), LstCamposTabla, lsttuplas));
                lst.put(NombreTabla.name,new Tabla(NombreTabla.name, PathRegistrosTabla.name.replace("<![CDATA[", "").replace("]]>", ""), LstCamposTabla, lsttuplas));
                
            }
        }                                        
        return lst;                        
    }
    
    
    //metodo que extrae los registros para las tablas
    public ArrayList<Tupla> ExtraerTuplasTabla(String path)
//    public Hashtable ExtraerTuplasTabla(String path)
    {
        ArrayList<Tupla> lstTuplas = new ArrayList<Tupla>();
//        ArrayList<Registro> lstReg;
//        Hashtable lstTuplas = new Hashtable();
        Hashtable lstReg;
        
        //mandamos a leer el archivo Tabla
        String str_XMLTabla = LeerArchivo(path);        
        //obtenermos en nodo raiz del archivo registro analizado
        SimpleNode raizTabla = ObtererRaizXML(str_XMLTabla);
        
        SimpleNode TipoArchivo = (SimpleNode) raizTabla.children[0];
        SimpleNode ArchivoTabla = (SimpleNode) TipoArchivo.children[0];
        
        if(ArchivoTabla.children != null)
        {
            for (int i = 0; i < ArchivoTabla.children.length; i++) {
            // se obtiene cada uno de los conjuntos de rtegistros
            SimpleNode regist = (SimpleNode) ArchivoTabla.children[i];

            String valor="", nombre ="";

            //Nueva lista de registros
            lstReg = new Hashtable();

            //aqui viene los registros ID, tipo, ID, Tipo......                        
            for (int j = 0; j < regist.children.length; j++) {

                SimpleNode nd = (SimpleNode) regist.children[j];
                String idNodo = AnalizadorXMLTreeConstants.jjtNodeName[nd.id];

                if(idNodo.equals("identificador"))
                {
                    nombre = nd.name;
                }else if(idNodo.equals("variable"))
                {
                    nombre = nd.name;
                }else
                {
                    if(nd.children != null){
                        SimpleNode Val_nd = (SimpleNode) nd.children[0];             
                        valor = Val_nd.name;
                    }else
                    {
                        valor = "";
                    }
                    
//                    lstReg.add(new Registro(nombre, valor));
                    lstReg.put(nombre,new Registro(nombre, valor));
                }               
            }
            //se agrega una nueva tupla
            lstTuplas.add(new Tupla(lstReg));
            } 
        }
               
        return lstTuplas;
    }
    
    
    //metodo que retoran la lista de parametros de una tabla
    public ArrayList<Campo> ExtraerCamposTabla(SimpleNode nodo)
//    public Hashtable ExtraerCamposTabla(SimpleNode nodo)
    {
        ArrayList<Campo> lstCampos = new ArrayList<Campo>();
//        Hashtable lstCampos = new Hashtable();
        
        for (int i = 0; i < nodo.children.length; i++) {
            SimpleNode campo = (SimpleNode) nodo.children[i];
            
            SimpleNode NodoTipo = (SimpleNode) campo.children[0];
            SimpleNode tipo = (SimpleNode) NodoTipo.children[0];
            
            SimpleNode NombreCampo = (SimpleNode) campo.children[1];
            
            SimpleNode NodoPropiedades = (SimpleNode) campo.children[2];
            
            lstCampos.add(new Campo(tipo.name, NombreCampo.name,ObtenerListaPropiedades(NodoPropiedades)));            
//            lstCampos.put(NombreCampo.name,new Campo(tipo.name, NombreCampo.name,ObtenerListaPropiedades(NodoPropiedades)));            
        }
        
        return lstCampos;
    }
    
    //metodo que retorna un arrayList<String> de las propiedades
    public ArrayList<String> ObtenerListaPropiedades(SimpleNode nodo)
    {
        ArrayList<String> lista = new ArrayList<String>();
        
        if(nodo.children != null)
        {
            for (int i = 0; i < nodo.children.length; i++) {
                SimpleNode propiedad = (SimpleNode) nodo.children[i];            
                lista.add(propiedad.name);            
            } 
        }
                
        return lista;
    }
    
    //metodo que retorna la ruta de las funciones en una BD
    public String getPathMetodosFun(SimpleNode nodo){
 
        try {
            SimpleNode Tipo_Archivo = (SimpleNode)nodo.children[0];
            SimpleNode ArchivoRegistro = (SimpleNode)Tipo_Archivo.children[0];
            SimpleNode Metodos_Funciones = (SimpleNode)ArchivoRegistro.children[0];
            SimpleNode MetodoFun = (SimpleNode)Metodos_Funciones.children[0];
            SimpleNode rutaMF = (SimpleNode)MetodoFun.children[0];

            return rutaMF.name.replace("<![CDATA[", "").replace("]]>", "");
        } catch (Exception e) {
            System.out.println(e.toString());
            return "";
        }       
    }
    
    //metodo que retorna el path de obetos en una DB
    public String getPathObjetos(SimpleNode nodo){
 
        try {
            SimpleNode Tipo_Archivo = (SimpleNode)nodo.children[0];
            SimpleNode ArchivoRegistro = (SimpleNode)Tipo_Archivo.children[0];
            SimpleNode Objetos = (SimpleNode)ArchivoRegistro.children[1];
            SimpleNode Obj = (SimpleNode)Objetos.children[0];
            SimpleNode rutaObj = (SimpleNode)Obj.children[0];

            return rutaObj.name.replace("<![CDATA[", "").replace("]]>", "");
        } catch (Exception e) {
            System.out.println(e.toString());
            return "";
        }       
    }
    
    public Metodo_Fun ExtraerMetodo(SimpleNode pro_fun)
    {
        try {
            ArrayList<Parametro> LstParamMF = new ArrayList<Parametro>();
//            Hashtable LstParamMF = new Hashtable();
            
            SimpleNode NodoNombreMF = (SimpleNode) pro_fun.children[0];            
            SimpleNode NodoParametrosMF = (SimpleNode) pro_fun.children[1];
            SimpleNode NodoCodigoMF = (SimpleNode) pro_fun.children[2];
            SimpleNode NodoRetornoMF = (SimpleNode) pro_fun.children[3];
            
            if(NodoParametrosMF.children != null)
            {
                for (int i = 0; i < NodoParametrosMF.children.length; i++) {

                SimpleNode ParamMF = (SimpleNode) NodoParametrosMF.children[i];

                SimpleNode Tipo = (SimpleNode) ParamMF.children[0];
                SimpleNode TipoParam = (SimpleNode) Tipo.children[0];

                SimpleNode nombreParam = (SimpleNode) ParamMF.children[1];

                LstParamMF.add(new Parametro(nombreParam.name, TipoParam.name));
//                LstParamMF.put(nombreParam.name, new Parametro(nombreParam.name, TipoParam.name));
                }
            }
            
            
            SimpleNode CodigoMF = (SimpleNode) NodoCodigoMF.children[0];
            
            //falta agregar nodo codigo y nodo retonro
            if(NodoRetornoMF.children != null)
            {
                SimpleNode TipoRetorno = (SimpleNode) NodoRetornoMF.children[0];
                SimpleNode Retorno = (SimpleNode) TipoRetorno.children[0];
                
                return new Metodo_Fun(NodoNombreMF.name, LstParamMF, CodigoMF.name.replace("<![CDATA[", "").replace("]]>", ""), Retorno.name);
                
            }else
            {
                return new Metodo_Fun(NodoNombreMF.name, LstParamMF, CodigoMF.name.replace("<![CDATA[", "").replace("]]>", ""), null);
            }
            
            
        } catch (Exception e) {
            return null;
        }
    }
    
    public void CargarBD_Completa()
    {
        //buscar y leer el archivo
        String aux,cadena="";
//        String myDocumentPath = System.getProperty("user.home") + "\\Documents\\repcompi\\ARCHIVOS\\";
        String myDocumentPath = ConstantesXML.pathArchivos;
        myDocumentPath += "Maestro.usac";
        
        cadena = LeerArchivo(myDocumentPath);
                
        //mandarlo a analizar        
        
        try {
            
            SimpleNode raiz = ObtererRaizXML(cadena);
            if(raiz!=null)
            {
                ejecutarXML(raiz);
                System.out.println("SE CARGO TODA LA BD");
            }else
            {
                System.out.println("Error al obtener raiz de XML");
            }
        
        } catch (Exception e) {
            System.err.println(e.toString());
            JOptionPane.showMessageDialog(null, e.toString(), "ERROR XML", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //metodo que reocrre un archivo del tipo XML lo anlaiza y lo grafica
    //retorna un simplenode raiz
    public SimpleNode ObtererRaizXML(String cadena)
    {
        InputStream is = new ByteArrayInputStream(cadena.getBytes());
        AnalizadorXML analisis = new AnalizadorXML(is);
        
        interpreteXML.SimpleNode raiz;
        try {
            raiz = analisis.INICIO();
                                    
            Handler_XML handler = new Handler_XML();
            handler.agregarNumero(raiz);
            handler.iniciarGrafica(raiz);
            handler.GenerarImagen();
            System.out.println("Analisis XML...");
            //JOptionPane.showMessageDialog(null, "Se grafico XML", "EXITO", JOptionPane.INFORMATION_MESSAGE);                                         
              
            return raiz;
        } catch (Exception e) {
            System.err.println(e.toString());
            JOptionPane.showMessageDialog(null, e.toString(), "ERROR XML", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }
    
    public void AgregarBD(String nombreBD){
        if(!ConstantesXML.BaseDatosGeneral.HashBasesSistema.containsKey(nombreBD))
        {
            ArrayList<String> usuarios = new ArrayList<String>();
            usuarios.add("admin");
            if(!ejecutarUSQL.ConstantesUSQL.UsuarioActual.equals("admin") || !ejecutarUSQL.ConstantesUSQL.UsuarioActual.equals(""))
            {

            }else
            {
                usuarios.add(ejecutarUSQL.ConstantesUSQL.UsuarioActual);
            }

            //crear nuevo registro
            String pathNuevoRegistroBD = ConstantesXML.pathArchivos + "RegistroBD_"+nombreBD+".usac";
            CrearArchivoRegistro(pathNuevoRegistroBD,nombreBD);

            //se crea la BD
            Base_Datos nuewBD = new Base_Datos(nombreBD, pathNuevoRegistroBD, usuarios);
            // se agrega junto con todas las BD del sistema
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(nombreBD, nuewBD);
            ConstantesUSQL.CadenaMsj += " Crear -> Base de datos creada correctamente. "+ConstantesUSQL.Separador;
            ConstantesUSQL.ultimoResultado = " Base de datos creada correctamente. ";
        }else
        {
            //ERROR BD ya existe
            ConstantesXML.AddError("Base Datos", "La base de datos ya existe", 0, 0);
        }      
    }
    
    public void AgregarTabla(String nombre, String BD,analizador.SimpleNode Parametros){
        if(!ConstantesUSQL.BDActual.HashTablas.containsKey(nombre))
        {           
            //creamos el path de la tabla
            String pathTabla = ConstantesXML.pathArchivos +"tabla_"+nombre+"_"+BD+".usac";
            
            //creamos el archivo para almacenar los registros
            CrearArchivoTabla(pathTabla,nombre);
            
            //creamos HAsh para campos 
//            Hashtable hashCampos = new Hashtable();
            ArrayList<Campo> LstCampos = new ArrayList<Campo>();
            //extraemos los campos
            for(int i = 0; i<Parametros.children.length;i++)
            {
                analizador.SimpleNode parametro = (analizador.SimpleNode)Parametros.children[i];
                
                analizador.SimpleNode NodoTipo_campo = (analizador.SimpleNode)parametro.children[0];
                analizador.SimpleNode Tipo_campo = (analizador.SimpleNode)NodoTipo_campo.children[0];
                
                analizador.SimpleNode nombre_campo = (analizador.SimpleNode)parametro.children[1];
                
                if(parametro.children.length == 3)
                {
                    //TIPO_DATO() (identificador()|identificador2()) (COMPLEMENTOS_CAMPO())?    
                    analizador.SimpleNode COMPLEMENTOS_CAMPO = (analizador.SimpleNode)parametro.children[2];
                    
                    ArrayList<String> complementos = new ArrayList<String>();
                    
                    for(int j = 0; j<COMPLEMENTOS_CAMPO.children.length;j++)
                    {
                        analizador.SimpleNode COMPLEMENTO = (analizador.SimpleNode)COMPLEMENTOS_CAMPO.children[j];
                        complementos.add(COMPLEMENTO.name);
                    }
                    Campo Camp = new Campo(Tipo_campo.name, nombre_campo.name, complementos);
//                    hashCampos.put(Tipo_campo.name, Camp);
                    LstCampos.add(Camp);
                }
                else
                {
                    //TIPO_DATO() (identificador()|identificador2()) 
                    Campo Camp = new Campo(Tipo_campo.name, nombre_campo.name, new ArrayList<String>());
//                    hashCampos.put(nombre_campo.name, Camp);
                    LstCampos.add(Camp);
                }
                
            }
            
            
            //se crea la Tabla
//            Tabla nuewTab = new Tabla(nombre, pathTabla,hashCampos, new ArrayList<Tupla>());
            Tabla nuewTab = new Tabla(nombre, pathTabla,LstCampos, new ArrayList<Tupla>());
            // se agrega a la base de datos actual
            ConstantesUSQL.BDActual.HashTablas.put(nombre, nuewTab);
            //reemplazar en el listado de BD general
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.remove(ConstantesUSQL.BDActual.Nombre_BD);
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(ConstantesUSQL.BDActual.Nombre_BD,ConstantesUSQL.BDActual);
            
            ConstantesUSQL.CadenaMsj += " Crear -> Tabla de datos creada correctamente. "+ConstantesUSQL.Separador;
            ConstantesUSQL.ultimoResultado = " Tabla creada correctamente. ";
            
        }else
        {
            //ERROR Tabla ya existe
            ConstantesXML.AddError("Base Datos", "La Tabla "+ nombre+ " ya existe", 0, 0);
        }
    }
    
    public void AgregarObjeto(String nombre, String BD,analizador.SimpleNode Parametros){
        if(!ConstantesUSQL.BDActual.HashObjetos.containsKey(nombre))
        {                       
            
            //creamos HAsh para campos 
//            Hashtable hashCampos = new Hashtable();
            ArrayList<Parametro> LstCampos = new ArrayList<Parametro>();
            //extraemos los campos
            for(int i = 0; i<Parametros.children.length;i++)
            {
                analizador.SimpleNode parametro = (analizador.SimpleNode)Parametros.children[i];
                
                analizador.SimpleNode NodoTipo_campo = (analizador.SimpleNode)parametro.children[0];
                analizador.SimpleNode Tipo_campo = (analizador.SimpleNode)NodoTipo_campo.children[0];
                
                analizador.SimpleNode nombre_campo = (analizador.SimpleNode)parametro.children[1];
                                
                //TIPO_DATO() (identificador()|identificador2()) 
                Parametro Param = new Parametro(nombre_campo.name, Tipo_campo.name);
//                hashCampos.put(nombre_campo.name, Param);  
                LstCampos.add(Param);
                
            }            
            
            //se crea el objeto
//            Objeto nuewObj = new Objeto(nombre, hashCampos);
            Objeto nuewObj = new Objeto(nombre, LstCampos);
            // se agrega a la base de datos actual
            ConstantesUSQL.BDActual.HashObjetos.put(nombre, nuewObj);
            //reemplazar en el listado de BD general
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.remove(ConstantesUSQL.BDActual.Nombre_BD);
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(ConstantesUSQL.BDActual.Nombre_BD,ConstantesUSQL.BDActual);
            
            ConstantesUSQL.CadenaMsj += " Crear -> Objeto creado correctamente. "+ConstantesUSQL.Separador;
            ConstantesUSQL.ultimoResultado = " Objeto creado correctamente. ";
            
        }else
        {
            //ERROR Tabla ya existe
            ConstantesXML.AddError("Base Datos", "El objeto "+ nombre+ " ya existe", 0, 0);
        }
    }
    
    public void AgregarProcedimiento(String nombre, String BD,analizador.SimpleNode Parametros, String codigo){
        if(!ConstantesUSQL.BDActual.HashMetodos_Fun.containsKey(nombre))
        {                       
            
            //creamos HAsh para campos 
//            Hashtable hashCampos = new Hashtable();
            ArrayList<Parametro> LstCampos = new ArrayList<Parametro>();
            //extraemos los campos
            if(Parametros!=null){
                for(int i = 0; i<Parametros.children.length;i++)
                {
                    analizador.SimpleNode parametro = (analizador.SimpleNode)Parametros.children[i];

                    analizador.SimpleNode NodoTipo_campo = (analizador.SimpleNode)parametro.children[0];
                    analizador.SimpleNode Tipo_campo = (analizador.SimpleNode)NodoTipo_campo.children[0];

                    analizador.SimpleNode nombre_campo = (analizador.SimpleNode)parametro.children[1];

                    //TIPO_DATO() (identificador()|identificador2()) 
                    Parametro Param = new Parametro(nombre_campo.name, Tipo_campo.name);
//                    hashCampos.put(nombre_campo.name, Param);    
                    LstCampos.add(Param);
                } 
            }else
            {
                
            }
                       
            
            //se crea el objeto
//            Metodo_Fun nuewMF = new Metodo_Fun(nombre, hashCampos,codigo,"");
            Metodo_Fun nuewMF = new Metodo_Fun(nombre, LstCampos,codigo,"");
            // se agrega a la base de datos actual
            ConstantesUSQL.BDActual.HashMetodos_Fun.put(nombre, nuewMF);
            //reemplazar en el listado de BD general
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.remove(ConstantesUSQL.BDActual.Nombre_BD);
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(ConstantesUSQL.BDActual.Nombre_BD,ConstantesUSQL.BDActual);
            
            ConstantesUSQL.CadenaMsj += " Crear -> Procedimineto creado correctamente. "+ConstantesUSQL.Separador;
            ConstantesUSQL.ultimoResultado = " Procedimineto creado correctamente. ";
            
        }else
        {
            //ERROR Tabla ya existe
            ConstantesXML.AddError("Base Datos", "El objeto "+ nombre+ " ya existe", 0, 0);
        }
    }
    
    public void AgregarFuncion(String nombre, String BD,analizador.SimpleNode Parametros,analizador.SimpleNode TipoDato ,String codigo){
        if(!ConstantesUSQL.BDActual.HashMetodos_Fun.containsKey(nombre))
        {                                   
            //creamos HAsh para campos 
//            Hashtable hashCampos = new Hashtable();
            ArrayList<Parametro> LstCampos = new ArrayList<Parametro>();
            //extraemos los campos
            if(Parametros!=null){
                for(int i = 0; i<Parametros.children.length;i++)
                {
                    analizador.SimpleNode parametro = (analizador.SimpleNode)Parametros.children[i];

                    analizador.SimpleNode NodoTipo_campo = (analizador.SimpleNode)parametro.children[0];
                    analizador.SimpleNode Tipo_campo = (analizador.SimpleNode)NodoTipo_campo.children[0];

                    analizador.SimpleNode nombre_campo = (analizador.SimpleNode)parametro.children[1];

                    //TIPO_DATO() (identificador()|identificador2()) 
                    Parametro Param = new Parametro(nombre_campo.name, Tipo_campo.name);
//                    hashCampos.put(nombre_campo.name, Param);    
                    LstCampos.add(Param);

                } 
            }else
            {
                
            }
            
            
            analizador.SimpleNode nodo = ((analizador.SimpleNode)TipoDato.children[0]);
            Metodo_Fun nuewMF;
            
            if(AnalizadorTreeConstants.jjtNodeName[nodo.id].equals("identificador2"))
            {
//                nuewMF = new Metodo_Fun(nombre, hashCampos,codigo,nodo.name);
                nuewMF = new Metodo_Fun(nombre, LstCampos,codigo,nodo.name);
            }else
            {
//                nuewMF = new Metodo_Fun(nombre, hashCampos,codigo,nodo.name.toUpperCase());
                nuewMF = new Metodo_Fun(nombre, LstCampos,codigo,nodo.name);
            }
            //se crea el objeto
            
            // se agrega a la base de datos actual
            ConstantesUSQL.BDActual.HashMetodos_Fun.put(nombre, nuewMF);
            //reemplazar en el listado de BD general
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.remove(ConstantesUSQL.BDActual.Nombre_BD);
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(ConstantesUSQL.BDActual.Nombre_BD,ConstantesUSQL.BDActual);
            
            ConstantesUSQL.CadenaMsj += " Crear -> Funcion creada correctamente. "+ConstantesUSQL.Separador;
            ConstantesUSQL.ultimoResultado = " Funcion creada correctamente. ";
            
        }else
        {
            //ERROR Tabla ya existe
            ConstantesXML.AddError("Base Datos", "El Procedimineto/funcion "+ nombre+ " ya existe", 0, 0);
        }
    }
    
    public void InsertarTabla(String nombreTabla,String ambito,analizador.SimpleNode NodoCamposInsertar,analizador.SimpleNode nodoValoresInsertar){
        if(ConstantesUSQL.BDActual.HashTablas.containsKey(nombreTabla))
        { 
            Tabla Tab = (Tabla) ConstantesUSQL.BDActual.HashTablas.get(nombreTabla);
            
            Tupla tup = new Tupla(new Hashtable());
            
            boolean exito = true;
            
            Campo Camp=null;
            if(NodoCamposInsertar!=null){
//                if(NodoCamposInsertar.children.length == nodoValoresInsertar.children.length &&  Tab.HashCampos_Tabla.size()== nodoValoresInsertar.children.length)
                if(NodoCamposInsertar.children.length == nodoValoresInsertar.children.length &&  Tab.LstCampos_Tabla.size()== nodoValoresInsertar.children.length)
                {
                    //buscar cada campo en la lista de nodos y buscarlo en hashCampos para verificar he insertar
                    for (int i = 0; i < NodoCamposInsertar.children.length; i++) {
                        //obtener el campo a insertar
                        analizador.SimpleNode nodoCampo = (analizador.SimpleNode)NodoCamposInsertar.children[i];
                        
                        for (int j = 0; j < Tab.LstCampos_Tabla.size(); j++) {
                            if(((Campo) Tab.LstCampos_Tabla.get(j)).Nombre_Campo.equals(nodoCampo.name))
                            {
                                Camp = (Campo) Tab.LstCampos_Tabla.get(j);
                                break;
                            }
                                
                        }
//                        Camp = (Campo) Tab.HashCampos_Tabla.get(nodoCampo.name);
                        
                        if(Camp!=null && exito)
                        {
                            analizador.SimpleNode nodoValor = (analizador.SimpleNode)nodoValoresInsertar.children[i];
                        
                            //obtener el valor a insertar
                            ejecutarUSQL.ejecucion RecExp = new ejecucion();
                            ElementoExp Elem = RecExp.RecorrerEXP(nodoValor, ambito);
                            
                            //obtenemos todas las propiedades del campo
                            boolean nulo= false, no_nulo= false, autoincrementable= false, llave_primaria= false, llave_foranea= false, unico = false;
                            String foranea="";
                            
                            for (int j = 0; j < Camp.Propiedades.size(); j++) {
                                
                                if(Camp.Propiedades.get(j).toUpperCase().equals("NULO")) nulo = true;
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("NO NULO")) no_nulo = true;
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("AUTOINCREMENTABLE")) autoincrementable = true;
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("LLAVE_PRIMARIA")) llave_primaria = true;
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("LLAVE_FORANEA")){ llave_foranea = true; foranea = Camp.Propiedades.get(j+1);}
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("UNICO")) unico = true;                               
                            }
                            
                            if(llave_foranea){
                                
                                Campo campF = VerificarForanea(foranea);
                                
                                if(campF != null)
                                {
                                    
                                    if((llave_primaria || unico) && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo)) 
                                    {
                                        exito = true;
                                        //obtener el ultimo valor numerico introducido
                                        for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {                                                                        
                                            if(Elem.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(Camp.Nombre_Campo)).ValorCampo_Registro)) exito = false;
                                        }

                                        if(exito){

                                            if(campF.Tipo_campo.equals(Camp.Tipo_campo))
                                            {
                                                boolean existeDato= false;
                                                for (int j = 0; j < ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.size(); j++) {
                                                    if(((Registro)ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(campF.Nombre_Campo)).ValorCampo_Registro.equals(Elem.Valor))
                                                    {
                                                        existeDato = true;
                                                    }                                            
                                                }
                                                if(existeDato)
                                                {
                                                    tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                                    exito = true;
                                                }else
                                                {
                                                    exito = false;
                                                    ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es una llave foranea valida.", 0, 0);
                                                }
                                            }else
                                            {
                                                exito = false;
                                                ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es del mismo tipo que en el campo "+campF.Nombre_Campo+" de la tabla"+ConstantesUSQL.UltimaTablaForanea.Nombre_Tabla, 0, 0);
                                            }
                                        }else
                                        {
                                            exito = false;
                                            ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" es primaria o unica po rlo tanto no puede repetirse.", 0, 0);
                                        }

                                    }else if(Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo))
                                    {
                                        if(campF.Tipo_campo.equals(Camp.Tipo_campo))
                                        {
                                            boolean existeDato= false;
                                            for (int j = 0; j < ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.size(); j++) {
                                                if(((Registro)ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(campF.Nombre_Campo)).ValorCampo_Registro.equals(Elem.Valor))
                                                {
                                                    existeDato = true;
                                                }                                            
                                            }
                                            if(existeDato)
                                            {
                                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                                exito = true;
                                            }else
                                            {
                                                exito = false;
                                                ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es una llave foranea valida.", 0, 0);
                                            }
                                        }else
                                        {
                                            exito = false;
                                            ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es del mismo tipo que en el campo "+campF.Nombre_Campo+" de la tabla"+ConstantesUSQL.UltimaTablaForanea.Nombre_Tabla, 0, 0);
                                        }
                                    }else
                                    {
                                        exito = false;
                                        ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es del mismo tipo que en el campo "+Camp.Nombre_Campo, 0, 0);
                                    }
                                }
                                
                            }else if(autoincrementable && ObtenerTipoInt(Camp.Tipo_campo) == ConstantesUSQL.TipoNumero && Elem.Tipo == ConstantesUSQL.TipoNumero) 
                            {
                                //obtener el ultimo valor numerico introducido
                                int num=0, aux=0;
                                for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {
                                    aux = Integer.parseInt(((Registro)Tab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(Camp.Nombre_Campo)).ValorCampo_Registro);
                                    if(num == 0) num = aux;
                                    if(num<aux) num = aux;
                                }
                                System.out.println("el numeor mayor es " +num);
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, num+1+""));
                                exito = true;
                                
                                
                            }else if(llave_primaria && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo)) 
                            {
                                exito = true;
                                //obtener el ultimo valor numerico introducido
                                for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {                                                                        
                                    if(Elem.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(Camp.Nombre_Campo)).ValorCampo_Registro)) exito = false;
                                }
                                
                                if (exito) tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                else ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" es llave primaria y no se puede repetir.", 0, 0);
                                
                            }else if(nulo && Elem.Tipo == ConstantesUSQL.TipoNull)
                            {
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, ""));
                                exito = true;
                                
                            }else if(nulo && !Elem.Valor.equals("") && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo))
                            {
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                exito = true;
                                
                            }else if(unico && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo)) 
                            {
                                exito = true;
                                //obtener el ultimo valor numerico introducido
                                for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {                                                                        
                                    if(Elem.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(Camp.Nombre_Campo)).ValorCampo_Registro)) exito = false;
                                }
                                
                                if (exito) tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));                                                                
                                else ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" es del tipo unico y no se puede repetir.", 0, 0);
                                
                            }else if(no_nulo && !Elem.Valor.equals("") && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo))
                            {
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                exito = true;
                                
                            }else if(Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo))
                            {
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                exito = true;
                                
                            }else
                            {
                                ConstantesXML.AddError("Base Datos", "El elemento No. "+i+" no es del mismo tipo para el campo "+nodoCampo.name, 0, 0);
                                exito = false;
                                break;
                            }
                        }else
                        {
                            ConstantesXML.AddError("Base Datos", "El campo "+nodoCampo.name+" no pertenece a la tabla "+nombreTabla, 0, 0);
                            exito = false;
                            break;
                        }                        
                    }
                    if(exito){
                        Tab.LstTuplas_Tabla.add(tup);
                        
                        ConstantesUSQL.CadenaMsj += " Insert -> Se realizo una insercion correctamente. "+ConstantesUSQL.Separador;
                        ConstantesUSQL.ultimoResultado = " Se realizo una insercion correctamente. ";
                    }
                    
                }else
                {
                    //error 
                    ConstantesXML.AddError("Base Datos", "El numero de campos a insertar no coinside con el numero de valores a insertar o requeridos por la tabla. ", 0, 0);
                }
                
            }else{
                //tomar cada campo en la hashCampos y extraer campo para insertar en tupla
//                if(Tab.HashCampos_Tabla.size()== nodoValoresInsertar.children.length)
                if(Tab.LstCampos_Tabla.size()== nodoValoresInsertar.children.length)
                {
                    //buscar cada campo en la hash de campos de la tabla
//                    Vector v = new Vector(Tab.HashCampos_Tabla.keySet());
//                    Collections.sort(v);
//                    Iterator it = v.iterator();
//                    
//                    int i =0; 
//                    exito = true;
//                    
//                    while (it.hasNext()) {  
//                        //obtener llaves de hash
//                        String element =  (String)it.next();
//                    int iV =0;
                    for (int i =0; i <Tab.LstCampos_Tabla.size(); i++) {
                                            
                        //obtengo el campo
//                        Camp = (Campo) Tab.HashCampos_Tabla.get(element);
                        Camp = (Campo) Tab.LstCampos_Tabla.get(i);

                        if(Camp!=null && exito)
                        {
                            analizador.SimpleNode nodoValor = (analizador.SimpleNode)nodoValoresInsertar.children[i];

                            //obtener el valor a insertar
                            ejecutarUSQL.ejecucion RecExp = new ejecucion();
                            ElementoExp Elem = RecExp.RecorrerEXP(nodoValor, ambito);

                            //obtenemos todas las propiedades del campo
                            boolean nulo= false, no_nulo= false, autoincrementable= false, llave_primaria= false, llave_foranea= false, unico = false;
                            String foranea="";

                            for (int j = 0; j < Camp.Propiedades.size(); j++) {

                                if(Camp.Propiedades.get(j).toUpperCase().equals("NULO")) nulo = true;
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("NO NULO")) no_nulo = true;
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("AUTOINCREMENTABLE")) autoincrementable = true;
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("LLAVE_PRIMARIA")) llave_primaria = true;
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("LLAVE_FORANEA")){ llave_foranea = true; foranea = Camp.Propiedades.get(j+1);}
                                 if(Camp.Propiedades.get(j).toUpperCase().equals("UNICO")) unico = true;                               
                            }

                            if(llave_foranea){

                                Campo campF = VerificarForanea(foranea);

                                if(campF != null)
                                {

                                    if((llave_primaria || unico) && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo)) 
                                    {
                                        exito = true;
                                        //obtener el ultimo valor numerico introducido
                                        for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {                                                                        
                                            if(Elem.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(Camp.Nombre_Campo)).ValorCampo_Registro)) exito = false;
                                        }

                                        if(exito){

                                            if(campF.Tipo_campo.equals(Camp.Tipo_campo))
                                            {
                                                boolean existeDato= false;
                                                for (int j = 0; j < ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.size(); j++) {
                                                    if(((Registro)ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(campF.Nombre_Campo)).ValorCampo_Registro.equals(Elem.Valor))
                                                    {
                                                        existeDato = true;
                                                    }                                            
                                                }
                                                if(existeDato)
                                                {
                                                    tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                                    exito = true;
                                                }else
                                                {
                                                    exito = false;
                                                    ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es una llave foranea valida.", 0, 0);
                                                }
                                            }else
                                            {
                                                exito = false;
                                                ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es del mismo tipo que en el campo "+campF.Nombre_Campo+" de la tabla"+ConstantesUSQL.UltimaTablaForanea.Nombre_Tabla, 0, 0);
                                            }
                                        }else
                                        {
                                            exito = false;
                                            ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" es primaria o unica por lo tanto no puede repetirse.", 0, 0);
                                        }

                                    }else if(Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo))
                                    {
                                        if(campF.Tipo_campo.equals(Camp.Tipo_campo))
                                        {
                                            boolean existeDato= false;
                                            for (int j = 0; j < ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.size(); j++) {
                                                if(((Registro)ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(campF.Nombre_Campo)).ValorCampo_Registro.equals(Elem.Valor))
                                                {
                                                    existeDato = true;
                                                }                                            
                                            }
                                            if(existeDato)
                                            {
                                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                                exito = true;
                                            }else
                                            {
                                                exito = false;
                                                ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es una llave foranea valida.", 0, 0);
                                            }
                                        }else
                                        {
                                            exito = false;
                                            ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es del mismo tipo que en el campo "+campF.Nombre_Campo+" de la tabla"+ConstantesUSQL.UltimaTablaForanea.Nombre_Tabla, 0, 0);
                                        }
                                    }else
                                    {
                                        exito = false;
                                        ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" para el campo "+Camp.Nombre_Campo+" de la tabla "+nombreTabla+" no es del mismo tipo que en el campo "+Camp.Nombre_Campo, 0, 0);
                                    }
                                }

                            }else if(autoincrementable && ObtenerTipoInt(Camp.Tipo_campo) == ConstantesUSQL.TipoNumero && Elem.Tipo == ConstantesUSQL.TipoNumero) 
                            {
                                //obtener el ultimo valor numerico introducido
                                int num=0, aux=0;
                                for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {
                                    aux = Integer.parseInt(((Registro)Tab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(Camp.Nombre_Campo)).ValorCampo_Registro);
                                    if(num == 0) num = aux;
                                    if(num<aux) num = aux;
                                }
                                System.out.println("el numeor mayor es " +num);
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, num+1+""));
                                exito = true;


                            }else if(llave_primaria && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo)) 
                            {
                                exito = true;
                                //obtener el ultimo valor numerico introducido
                                for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {                                                                        
                                    if(Elem.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(Camp.Nombre_Campo)).ValorCampo_Registro)) exito = false;
                                }

                                if (exito) tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                else ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" es llave primaria y no se puede repetir.", 0, 0);

                            }else if(nulo && Elem.Tipo == ConstantesUSQL.TipoNull)
                            {
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, ""));
                                exito = true;

                            }else if(nulo && !Elem.Valor.equals("") && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo))
                            {
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                exito = true;

                            }else if(unico && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo)) 
                            {
                                exito = true;
                                //obtener el ultimo valor numerico introducido
                                for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {                                                                        
                                    if(Elem.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.get(Camp.Nombre_Campo)).ValorCampo_Registro)) exito = false;
                                }

                                if (exito) tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));                                                                
                                else ConstantesXML.AddError("Base Datos", "El valor "+Elem.Valor+" es del tipo unico y no se puede repetir.", 0, 0);

                            }else if(no_nulo && !Elem.Valor.equals("") && Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo))
                            {
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                exito = true;

                            }else if(Elem.Tipo == ObtenerTipoInt(Camp.Tipo_campo))
                            {
                                tup.HashRegistros_Tabla.put(Camp.Nombre_Campo, new Registro(Camp.Nombre_Campo, Elem.Valor));
                                exito = true;

                            }else
                            {
                                ConstantesXML.AddError("Base Datos", "El elemento No. "+i+" no es del mismo tipo para el campo "+Camp.Nombre_Campo, 0, 0);
                                exito = false;
                                break;
                            }
                        }else
                        {
                            ConstantesXML.AddError("Base Datos", "El campo "+Camp.Nombre_Campo+" no pertenece a la tabla "+nombreTabla, 0, 0);
                            exito = false;
                            break;
                        }


//                        iV++;
                    }                                                                                                                    
                    if(exito) 
                    {
                        Tab.LstTuplas_Tabla.add(tup);
                        
                        ConstantesUSQL.CadenaMsj += " Insert -> Se realizo una insercion correctamente. "+ConstantesUSQL.Separador;
                        ConstantesUSQL.ultimoResultado = " Se realizo una insercion correctamente. ";
                    }
                    
                }else
                {
                    //error 
                    ConstantesXML.AddError("Base Datos", "El numero de campos a insertar no coinside con el numero de valores a insertar o requeridos por la tabla. ", 0, 0);
                }
                
            }
            //quitar y reemplazar la tabla en la BD actual
            ConstantesUSQL.BDActual.HashTablas.remove(Tab.Nombre_Tabla);
            ConstantesUSQL.BDActual.HashTablas.put(Tab.Nombre_Tabla,Tab);
            
            //quitar y reemplazar BD actual en la BD general
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.remove(ConstantesUSQL.BDActual.Nombre_BD);
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(ConstantesUSQL.BDActual.Nombre_BD,ConstantesUSQL.BDActual);
        }else
        {
            //error tabla no existe en BD actual
            ConstantesXML.AddError("Base Datos", "La tabla "+ nombreTabla+ " no existe en base de datos "+ConstantesUSQL.BDActual.Nombre_BD, 0, 0);
        }
    }
    
    public Campo VerificarForanea(String tablaForanea){
        if(!ConstantesUSQL.BDActual.HashTablas.containsKey(tablaForanea)) return  null;
        
        Tabla table = (Tabla)ConstantesUSQL.BDActual.HashTablas.get(tablaForanea);
        
        //remiendo para ver que exista el dato en la tabla a la que pertence el ultimo campo encontrado
        ConstantesUSQL.UltimaTablaForanea = table;
        
//        Set<String> keys = table.HashCampos_Tabla.keySet();            
//        Iterator<String> itr = keys.iterator();
//
//            String str = "";
            Campo camp = null;                 
//            
//            while (itr.hasNext()) { 
//                //obtener llaves de lista tablas
//                str = itr.next();     
            
                for (int k = 0; k < table.LstCampos_Tabla.size(); k++) {
                    
//                camp = (Campo) table.HashCampos_Tabla.get(str);
                camp = (Campo) table.LstCampos_Tabla.get(k);
                
                boolean bPrim=false,bFora=false;
                String StrForanea="";
                for (int i = 0; i < camp.Propiedades.size(); i++) {
                    if(camp.Propiedades.get(i).toUpperCase().equals("LLAVE_PRIMARIA")) bPrim = true;                  
                    if(camp.Propiedades.get(i).toUpperCase().equals("LLAVE_FORANEA")){ bFora = true; StrForanea = camp.Propiedades.get(i+1);}
                }
                
                if(bPrim){
                    if(bFora){
                        return VerificarForanea(StrForanea);
                    }
                    break;
                }
            }
            return camp;
    }
    
    public void CrearArchivoRegistro(String path, String BD)
    {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element BASE_DATOS = doc.createElement("BASE_DATOS");
            doc.appendChild(BASE_DATOS);
            
            Element Procedures = doc.createElement("Procedures");
            BASE_DATOS.appendChild(Procedures);
            
            Element Procedure = doc.createElement("Procedure");
            Procedures.appendChild(Procedure);

            String pathProcedures=ConstantesXML.pathArchivos+"metodos_"+BD+".usac";
            CrearArchivoMetodos(pathProcedures,BD);
            
            Element path_procedure = doc.createElement("path_procedure");
            path_procedure.appendChild(doc.createCDATASection(pathProcedures));
            Procedure.appendChild(path_procedure);
            
            Element Objects = doc.createElement("Objects");
            BASE_DATOS.appendChild(Objects);
            
            Element Object = doc.createElement("Object");
            Objects.appendChild(Object);

            String pathObjects=ConstantesXML.pathArchivos+"objetos_"+BD+".usac";
            CrearArchivoObjeto(pathObjects,BD);
            
            Element path_objeto = doc.createElement("path_objeto");
            path_objeto.appendChild(doc.createCDATASection(pathObjects));
            Object.appendChild(path_objeto);
            
            Element Tablas = doc.createElement("Tablas");
            Tablas.appendChild(doc.createTextNode(" "));
            BASE_DATOS.appendChild(Tablas);
            
            
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(path));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML Registro", "No se puede crear el archivo registro para BD: "+BD +" "+e.toString(), 0, 0);
        }
    }
    
    //metodo que crea archivo Procedures
    public void CrearArchivoMetodos(String path, String BD)
    {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element METODOS_FUNCIONES = doc.createElement("METODOS_FUNCIONES");
            METODOS_FUNCIONES.appendChild(doc.createTextNode(" "));
            doc.appendChild(METODOS_FUNCIONES);                                         
            
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(path));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML Metodos", "No se puede crear el archivo metodos para BD: "+BD +" "+e.toString(), 0, 0);
        }
    }
    
    //metodo que crea archivo objetos
    public void CrearArchivoObjeto(String path, String BD)
    {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element OBJETOS_BD = doc.createElement("OBJETOS_BD");
            OBJETOS_BD.appendChild(doc.createTextNode(" "));
            doc.appendChild(OBJETOS_BD);                                         
            
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(path));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML Metodos", "No se puede crear el archivo metodos para BD: "+BD +" "+e.toString(), 0, 0);
        }
    }
    
    public void CrearArchivoTabla(String path, String tsbls)
    {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element DATOS_TABLA = doc.createElement("DATOS_TABLA");
            DATOS_TABLA.appendChild(doc.createTextNode(" "));
            doc.appendChild(DATOS_TABLA);                                         
            
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(path));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML Tabla", "No se puede crear el archivo Tabla: "+tsbls +" "+e.toString(), 0, 0);
        }
    }
    
    public void SobreEscrituraMasiva()
    {
        Set<String> keys = ConstantesXML.BaseDatosGeneral.HashBasesSistema.keySet();
        //Obtaining iterator over set entries
        Iterator<String> itr = keys.iterator();
        
        String str = "";
        Base_Datos base;                 
        
        //crear archivo Maestro Nuevo
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element Maestro = doc.createElement("Maestro");
            doc.appendChild(Maestro);
            
            while (itr.hasNext()) { 
            // Getting Key
            str = itr.next();      
            //obtebgo la db
            base = (Base_Datos) ConstantesXML.BaseDatosGeneral.HashBasesSistema.get(str);
            
            //creo nuea etiqueta DB
            Element DB = doc.createElement("DB");
            Maestro.appendChild(DB);
            
            //agrego el nombre de la db
            Element nombre_base = doc.createElement("nombre_base");
            nombre_base.appendChild(doc.createTextNode(base.Nombre_BD));
            DB.appendChild(nombre_base);
            
            Element path_base = doc.createElement("path_base");
            path_base.appendChild(doc.createCDATASection(base.Path_BD));
            DB.appendChild(path_base);
            
            //sebreescribimos el archivo registro para la actual base de datos
            sobreEscribirRegistro(base.Path_BD,base.HashMetodos_Fun,base.HashObjetos, base.HashTablas,base.Nombre_BD);
            
            Element usuarios = doc.createElement("usuarios_autorizados");
            
            for (int i = 0; i < base.Usuarios_BD.size(); i++) {
                Element usuario = doc.createElement("usuario_autorizado");
                usuario.appendChild(doc.createTextNode(base.Usuarios_BD.get(i)));
                usuarios.appendChild(usuario);
            }
            
            DB.appendChild(usuarios);
            
            
            System.out.println("Key: "+str+" & Value: "+ConstantesXML.BaseDatosGeneral.HashBasesSistema.get(str));
            
            
            }                                             
            
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(ConstantesXML.pathArchivos+"Maestro.usac"));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML Maestro", "No se puede crear el maestro registro:  "+e.toString(), 0, 0);
        }
        
    }
    
    public void sobreEscribirRegistro(String Patharchivo,Hashtable HashMetodos_Fun,Hashtable HashObjetos, Hashtable HashTablas, String BD)
    {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element BASE_DATOS = doc.createElement("BASE_DATOS");
            doc.appendChild(BASE_DATOS);
            
            Element Procedures = doc.createElement("Procedures");
            BASE_DATOS.appendChild(Procedures);
            
            Element Procedure = doc.createElement("Procedure");
            Procedures.appendChild(Procedure);

            String pathProcedures=ConstantesXML.pathArchivos+"metodos_"+BD+".usac";
            
            //sobreescribir archivo Metodos
            sobreEscribirProcedures(pathProcedures,HashMetodos_Fun,BD);
            
            Element path_procedure = doc.createElement("path_procedure");
            path_procedure.appendChild(doc.createCDATASection(pathProcedures));
            Procedure.appendChild(path_procedure);
            
            Element Objects = doc.createElement("Objects");
            BASE_DATOS.appendChild(Objects);
            
            Element Object = doc.createElement("Object");
            Objects.appendChild(Object);

            String pathObjects=ConstantesXML.pathArchivos+"objetos_"+BD+".usac";
            
            //sobre escribimos el archivo para aobjetos
            sobreEscribirObjetos(pathObjects,HashObjetos,BD);
            
            Element path_objeto = doc.createElement("path_objeto");
            path_objeto.appendChild(doc.createCDATASection(pathObjects));
            Object.appendChild(path_objeto);
            
            Element Tablas = doc.createElement("Tablas");
            
            //HashTablas
            Set<String> keys = HashTablas.keySet();            
            Iterator<String> itr = keys.iterator();

            String str = "";
            Tabla Tab;                 
            
            while (itr.hasNext()) { 
                //obtener llaves de lista tablas
                str = itr.next();      
                //obtebgo la tabla
                Tab = (Tabla) HashTablas.get(str);

                //creo nuea etiqueta Tabla
                Element Tabla = doc.createElement("Tabla");
                
                Element nombre_tabla = doc.createElement("nombre_tabla");
                nombre_tabla.appendChild(doc.createTextNode(Tab.Nombre_Tabla));
                Tabla.appendChild(nombre_tabla);
                
                Element path_tabla = doc.createElement("path_tabla");
                path_tabla.appendChild(doc.createCDATASection(Tab.Path_Tabla));
                Tabla.appendChild(path_tabla);
                
                //sobreescribir archivo tabla
                //Tab.LstTuplas_Tabla
                sobreEscribirTuplas(Tab.Path_Tabla,Tab.LstTuplas_Tabla,BD);
                
                Element campos = doc.createElement("campos");
                

                //agregar campo por cada elemento en lista                                
//                Set<String> keysCampos = Tab.HashCampos_Tabla.keySet();            
//                Iterator<String> itrCampos = keysCampos.iterator();
//
//                String strCampos = "";
                Campo Camp;                 
//
//                while (itrCampos.hasNext()) { 
//                    //obtener llaves de lista Campos
//                    strCampos = itrCampos.next();    
                for (int k = 0; k < Tab.LstCampos_Tabla.size(); k++) {
                                    
                    //obtebgo la Campo
//                    Camp = (Campo) Tab.HashCampos_Tabla.get(strCampos);
                    Camp = (Campo) Tab.LstCampos_Tabla.get(k);

                    //creo nuea etiqueta Campo
                    Element campo = doc.createElement("campo");
                    
                    Element tipo_campo = doc.createElement("tipo_campo");
                    tipo_campo.appendChild(doc.createTextNode(Camp.Tipo_campo.toUpperCase()));
                    campo.appendChild(tipo_campo);
                    
                    Element nombre_campo = doc.createElement("nombre_campo");
                    nombre_campo.appendChild(doc.createTextNode(Camp.Nombre_Campo));
                    campo.appendChild(nombre_campo);
                    
                    Element propiedades = doc.createElement("propiedades");
                    //por cada propiedad en el arraylist se le agrega un propiedad_campo a propiedades
                    
                    for (int i = 0; i < Camp.Propiedades.size(); i++) {
                        Element propiedad_campo = doc.createElement("propiedad_campo");
                        
                        if(Camp.Propiedades.get(i).toUpperCase().equals("LLAVE_FORANEA"))
                        {
                            propiedad_campo.appendChild(doc.createTextNode(Camp.Propiedades.get(i)+ " "+ Camp.Propiedades.get(i+1)));
                            i++;
                        }else
                        {
                            propiedad_campo.appendChild(doc.createTextNode(Camp.Propiedades.get(i)));
                        }
                        
                        propiedades.appendChild(propiedad_campo);
                    }
                    propiedades.appendChild(doc.createTextNode(" "));
                    campo.appendChild(propiedades);
                    
                    campos.appendChild(campo);                                        
                }
                
                campos.appendChild(doc.createTextNode(" "));
                
                Tabla.appendChild(campos);                
                
                Tablas.appendChild(Tabla);                                           
            }
            
            Tablas.appendChild(doc.createTextNode(" "));
            
            BASE_DATOS.appendChild(Tablas);            
            
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(Patharchivo));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML Registro", "No se puede crear el archivo registro para BD: "+BD +" "+e.toString(), 0, 0);
        }
    }
    
    public void sobreEscribirProcedures(String pathArchivo,Hashtable HashPF,String BD){
        
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element METODOS_FUNCIONES = doc.createElement("METODOS_FUNCIONES");
            doc.appendChild(METODOS_FUNCIONES);
                        
            //por cada procedimiento o funcion en la hash parametro
            //agregamos etiqueta pro_fun
            Set<String> keys = HashPF.keySet();            
            Iterator<String> itr = keys.iterator();

            String str = "";
            Metodo_Fun MF;                 
            
            while (itr.hasNext()) { 
                //obtener llaves de lista tablas
                str = itr.next();      
                //obtebgo la tabla
                MF = (Metodo_Fun) HashPF.get(str);

                //creo nuea etiqueta Tabla
                Element pro_fun = doc.createElement("pro_fun");
                
                Element nombre_pro_fun = doc.createElement("nombre_pro_fun");
                nombre_pro_fun.appendChild(doc.createTextNode(MF.Nombre_MetodoFun));
                pro_fun.appendChild(nombre_pro_fun);                                
                
                Element parammetros_pro_fun = doc.createElement("parammetros_pro_fun");
                

                //agregar campo por cada elemento en lista                                
//                Set<String> keysParametro = MF.HashParametros_MetodoFun.keySet();            
//                Iterator<String> itrParametro = keysParametro.iterator();
//
//                String strParametro = "";
                Parametro Par;                 
//
//                while (itrParametro.hasNext()) { 
//                    //obtener llaves de lista Campos
//                    strParametro = itrParametro.next();    
                for (int i = 0; i < MF.LstParametros_MetodoFun.size(); i++) {
                                    
                    //obtebgo la Campo
//                    Par = (Parametro) MF.HashParametros_MetodoFun.get(strParametro);
                    Par = (Parametro) MF.LstParametros_MetodoFun.get(i);

                    Element parametro_pro_fun = doc.createElement("parametro_pro_fun");
                    
                    Element tipo_param_mf = doc.createElement("tipo_param_mf");
                    tipo_param_mf.appendChild(doc.createTextNode(Par.TIPO.toUpperCase()));
                    parametro_pro_fun.appendChild(tipo_param_mf);
                    
                    Element nombre_param_mf = doc.createElement("nombre_param_mf");
                    nombre_param_mf.appendChild(doc.createTextNode(Par.Nombre_Parametro));
                    parametro_pro_fun.appendChild(nombre_param_mf);
                    
                    parammetros_pro_fun.appendChild(parametro_pro_fun);
                                                          
                }
                
                parammetros_pro_fun.appendChild(doc.createTextNode(" "));
                
                pro_fun.appendChild(parammetros_pro_fun);   
                
                Element codigo_pf = doc.createElement("codigo_pf");
                codigo_pf.appendChild(doc.createCDATASection(MF.Codigo_MetodoFun));
                pro_fun.appendChild(codigo_pf);
                
                Element tipo_retorno = doc.createElement("tipo_retorno");
                if(MF.Retorno_MetodoFun!=null)
                {
                    tipo_retorno.appendChild(doc.createTextNode(MF.Retorno_MetodoFun));
                }else
                {
                    tipo_retorno.appendChild(doc.createTextNode(" "));
                }
                tipo_retorno.appendChild(doc.createTextNode(" "));
                pro_fun.appendChild(tipo_retorno);
                
                METODOS_FUNCIONES.appendChild(pro_fun);                                           
            }                        
            METODOS_FUNCIONES.appendChild(doc.createTextNode(" "));
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(pathArchivo));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML metodos", "No se puede crear el archivo meyodos para BD: "+BD +" "+e.toString(), 0, 0);
        }
    }
    
    public void sobreEscribirObjetos(String pathArchivo,Hashtable HashObjs,String BD){
        
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element OBJETOS_BD = doc.createElement("OBJETOS_BD");
            doc.appendChild(OBJETOS_BD);
                        
            //por cada procedimiento o funcion en la hash parametro
            //agregamos etiqueta pro_fun
            Set<String> keys = HashObjs.keySet();            
            Iterator<String> itr = keys.iterator();

            String str = "";
            Objeto Obj;                 
            
            while (itr.hasNext()) { 
                //obtener llaves de lista tablas
                str = itr.next();      
                //obtebgo la tabla
                Obj = (Objeto) HashObjs.get(str);

                //creo nuea etiqueta obj
                Element obj = doc.createElement("obj");
                
                Element nombre_obj = doc.createElement("nombre_obj");
                nombre_obj.appendChild(doc.createTextNode(Obj.Nombre_Objero));
                obj.appendChild(nombre_obj);                                
                
                Element parammetros_obj = doc.createElement("parammetros_obj");
                

                //agregar campo por cada elemento en lista                                
//                Set<String> keysParametro = Obj.HashParametros_Objeto.keySet();            
//                Iterator<String> itrParametro = keysParametro.iterator();
//
//                String strParametro = "";
                Parametro Par;                 
//
//                while (itrParametro.hasNext()) { 
//                    //obtener llaves de lista Campos
//                    strParametro = itrParametro.next();  
                for (int k = 0; k < Obj.LstParametros_Objeto.size(); k++) {
                                    
                    //obtebgo la Campo
//                    Par = (Parametro) Obj.HashParametros_Objeto.get(strParametro);
                    Par = (Parametro) Obj.LstParametros_Objeto.get(k);

                    Element parametro_obj = doc.createElement("parametro_obj");
                    
                    Element tipo_param_obj = doc.createElement("tipo_param_obj");
                    tipo_param_obj.appendChild(doc.createTextNode(Par.TIPO.toUpperCase()));
                    parametro_obj.appendChild(tipo_param_obj);
                    
                    Element nombre_param_obj = doc.createElement("nombre_param_obj");
                    nombre_param_obj.appendChild(doc.createTextNode(Par.Nombre_Parametro));
                    parametro_obj.appendChild(nombre_param_obj);
                    
                    parammetros_obj.appendChild(parametro_obj);
                                                          
                }
                
                parammetros_obj.appendChild(doc.createTextNode(" "));
                
                obj.appendChild(parammetros_obj);   
                
                OBJETOS_BD.appendChild(obj);                                           
            }                        
            OBJETOS_BD.appendChild(doc.createTextNode(" "));
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(pathArchivo));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML metodos", "No se puede crear el archivo meyodos para BD: "+BD +" "+e.toString(), 0, 0);
        }
    }
    
    public void sobreEscribirTuplas(String pathArchivo,ArrayList<Tupla> lstTuplas,String BD)
    {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();                        
            
            Element DATOS_TABLA = doc.createElement("DATOS_TABLA");
            doc.appendChild(DATOS_TABLA);
                        
            //por cada tupla agregamos etiqueta registro
            for(int i = 0; i < lstTuplas.size(); i++) {
                                     
                Tupla Tp = lstTuplas.get(i);

                //creo nuea etiqueta registro
                Element registro = doc.createElement("registro");
                
                
                //por cada elemenot en la hash guardamos un registro                            
                Set<String> keysRegistros = Tp.HashRegistros_Tabla.keySet();            
                Iterator<String> itrRegistros = keysRegistros.iterator();

                String strRegistros = "";
                Registro Reg;                 

                while (itrRegistros.hasNext()) { 
                    
                    strRegistros = itrRegistros.next();      
                    
                    Reg = (Registro) Tp.HashRegistros_Tabla.get(strRegistros);
                    
                    Element r = doc.createElement("r");
                    
                    Element nombreRegistro = doc.createElement("nombreRegistro");
                    nombreRegistro.appendChild(doc.createTextNode(Reg.NombreCampo_Registro));
                    r.appendChild(nombreRegistro); 
                    
                    Element valRegistro = doc.createElement("valRegistro");
                    if(Reg.ValorCampo_Registro != ""){
                        valRegistro.appendChild(doc.createTextNode(Reg.ValorCampo_Registro));
                    }else
                    {
                        valRegistro.appendChild(doc.createTextNode(" "));
                    }
                    r.appendChild(valRegistro); 
                    r.appendChild(doc.createTextNode(" "));
                    registro.appendChild(r);                                                                                                                                          
                }
                
                registro.appendChild(doc.createTextNode(" "));               
                
                DATOS_TABLA.appendChild(registro);                                           
            }                        
            DATOS_TABLA.appendChild(doc.createTextNode(" "));
            //escribir documento
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(pathArchivo));
            Source input = new DOMSource(doc);
            transformer.transform(input, output);
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
            //ERROR Ccrear Documento
            ConstantesXML.AddError("XML metodos", "No se puede crear el archivo meyodos para BD: "+BD +" "+e.toString(), 0, 0);
        }
    }
    
    public int ObtenerTipoInt(String tipo){
        switch(tipo.toUpperCase()){
            case "TEXT":
                return ConstantesUSQL.TipoCadena;
            case "INTEGER":
                return ConstantesUSQL.TipoNumero;
            case "DOUBLE":
                return ConstantesUSQL.TipoDouble;
            case "BOOL":
                return ConstantesUSQL.TipoBooleano;
            case "DATE":
                return ConstantesUSQL.TipoFecha;
            case "DATETIME":
                return ConstantesUSQL.TipoFechaHora;
            case "NULO":
                return ConstantesUSQL.TipoNull;
            default:
                return 0;
        }
    }
    
    public void ActualizarTabla(analizador.SimpleNode nodoActualizar, String ambito) {
        if(ConstantesUSQL.BDActual.HashTablas.containsKey(((analizador.SimpleNode)nodoActualizar.children[0]).name))
        {
            Tabla Tab = (Tabla) ConstantesUSQL.BDActual.HashTablas.get(((analizador.SimpleNode)nodoActualizar.children[0]).name);

            analizador.SimpleNode nodoListaID = (analizador.SimpleNode)nodoActualizar.children[1];
            analizador.SimpleNode nodoValores = (analizador.SimpleNode)nodoActualizar.children[2];

            switch(nodoActualizar.children.length)
            {
                case 4:
                    //identificador2() LISTA_ID() VALORES_PARAMETROS() CONDICIONES()
                    analizador.SimpleNode nodoCondiciones = (analizador.SimpleNode)nodoActualizar.children[3];
                    
                    Tabla tAux = new Tabla("tabAux","", Tab.LstCampos_Tabla, Tab.LstTuplas_Tabla);
                    int anterior = 0;
                    for (int i = 0; i < nodoCondiciones.children.length; i++) {
                        analizador.SimpleNode Nodocondicion = (analizador.SimpleNode) nodoCondiciones.children[i];
                        if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("CONDICION"))
                        {
                            if(anterior ==0)
                            {
                                tAux = FiltrarPorCondicion(Nodocondicion,tAux,ambito);
                            }else
                            {
                                tAux = convinarTuplas(tAux,FiltrarPorCondicion(Nodocondicion,new Tabla("tabAux2","", Tab.LstCampos_Tabla, Tab.LstTuplas_Tabla),ambito));
                            }
                            
                        }else if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("tk_and"))
                        {
                            anterior=0;
                        }else
                        {
                            //tk_or
                            anterior=1;
                        }                        
                    }
                    
                    //tengo las tuplas a actualizar en tAux
                    
                    if(nodoListaID.children.length == nodoValores.children.length)
                    {
                        //por cada parametro 
                        for (int i = 0; i < nodoListaID.children.length; i++) {                        
                            analizador.SimpleNode Ncampo = (analizador.SimpleNode)nodoListaID.children[i];
                            analizador.SimpleNode Nvalor = (analizador.SimpleNode)nodoValores.children[i];
                            
                            ejecutarUSQL.ejecucion RecExp = new ejecucion();
                            ElementoExp valor = RecExp.RecorrerEXP(Nvalor, ambito);
                            
                            boolean exiteCumpleCampo = false;
                            boolean AutoIncrementableCumpleCampo = false;
                            boolean UnicoCumpleCampo = false;
                            boolean PrimariaCumpleCampo = false;
                            
                            for (int j = 0; j < Tab.LstCampos_Tabla.size(); j++) {
                                Campo camp = ((Campo)Tab.LstCampos_Tabla.get(j));
                                if( camp.Nombre_Campo.equals(Ncampo.name) )
                                {
                                    //obtenemos todas las propiedades del campo
                                    boolean nulo= false, no_nulo= false, autoincrementable= false, llave_primaria= false, llave_foranea= false, unico = false;
                                    String foranea="";
                            
                                    for (int jj = 0; jj < camp.Propiedades.size(); jj++) {

                                        if(camp.Propiedades.get(jj).toUpperCase().equals("NULO")) nulo = true;
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("NO NULO")) no_nulo = true;
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("AUTOINCREMENTABLE")) autoincrementable = true;
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("LLAVE_PRIMARIA")) llave_primaria = true;
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("LLAVE_FORANEA")){ llave_foranea = true; foranea = camp.Propiedades.get(j+1);}
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("UNICO")) unico = true;                               
                                    }
                            
                                    if(llave_foranea){
                                
                                        Campo campF =  VerificarForanea(foranea);
                                
                                        if(campF != null)
                                        {
                                    
                                            if((llave_primaria || unico) && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo)) 
                                            {
                                                exiteCumpleCampo = true;
                                                //obtener el ultimo valor numerico introducido
                                                for (int j2 = 0; j2 < Tab.LstTuplas_Tabla.size(); j2++) {                                                                        
                                                    if(valor.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j2).HashRegistros_Tabla.get(camp.Nombre_Campo)).ValorCampo_Registro)) exiteCumpleCampo = false;
                                                }

                                                if(exiteCumpleCampo){

                                                    if(campF.Tipo_campo.equals(camp.Tipo_campo))
                                                    {
                                                        boolean existeDato= false;
                                                        for (int j3 = 0; j3 < ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.size(); j3++) {
                                                            if(((Registro)ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.get(j3).HashRegistros_Tabla.get(campF.Nombre_Campo)).ValorCampo_Registro.equals(valor.Valor))
                                                            {
                                                                existeDato = true;
                                                            }                                            
                                                        }
                                                        if(existeDato)
                                                        {
                                                            exiteCumpleCampo = true;
                                                            PrimariaCumpleCampo = true;
                                                            UnicoCumpleCampo = true;
                                                        }else
                                                        {
                                                            exiteCumpleCampo = false;
                                                            ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es una llave foranea valida.", 0, 0);
                                                        }
                                                    }else
                                                    {
                                                        exiteCumpleCampo = false;
                                                        ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es del mismo tipo que en el campo "+campF.Nombre_Campo+" de la tabla"+ConstantesUSQL.UltimaTablaForanea.Nombre_Tabla, 0, 0);
                                                    }
                                                }else
                                                {
                                                    exiteCumpleCampo = false;
                                                    ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" es primaria o unica po rlo tanto no puede repetirse.", 0, 0);
                                                }

                                            }else if(valor.Tipo == ObtenerTipoInt(camp.Tipo_campo))
                                            {
                                                if(campF.Tipo_campo.equals(camp.Tipo_campo))
                                                {
                                                boolean existeDato= false;
                                                for (int j4 = 0; j4 < ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.size(); j4++) {
                                                    if(((Registro)ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.get(j4).HashRegistros_Tabla.get(campF.Nombre_Campo)).ValorCampo_Registro.equals(valor.Valor))
                                                    {
                                                        existeDato = true;
                                                    }                                            
                                                }
                                                if(existeDato)
                                                {
                                                    exiteCumpleCampo = true;
                                                    
                                                }else
                                                {
                                                    exiteCumpleCampo = false;
                                                    ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es una llave foranea valida.", 0, 0);
                                                }
                                            }else
                                            {
                                                exiteCumpleCampo = false;
                                                ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es del mismo tipo que en el campo "+campF.Nombre_Campo+" de la tabla"+ConstantesUSQL.UltimaTablaForanea.Nombre_Tabla, 0, 0);
                                            }
                                        }else
                                        {
                                            exiteCumpleCampo = false;
                                            ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es del mismo tipo que en el campo "+camp.Nombre_Campo, 0, 0);
                                        }
                                    }
                                
                                    }else if(autoincrementable && ObtenerTipoInt(camp.Tipo_campo) == ConstantesUSQL.TipoNumero && valor.Tipo == ConstantesUSQL.TipoNumero) 
                                    {
                                        //obtener el ultimo valor numerico introducido
                                        int num=0, aux=0;
                                        for (int j5 = 0; j5 < Tab.LstTuplas_Tabla.size(); j5++) {
                                            aux = Integer.parseInt(((Registro)Tab.LstTuplas_Tabla.get(j5).HashRegistros_Tabla.get(camp.Nombre_Campo)).ValorCampo_Registro);
                                            if(num == 0) num = aux;
                                            if(num<aux) num = aux;
                                        }
                                        valor.Valor =String.valueOf(num+1);
                                        exiteCumpleCampo = true;
                                        AutoIncrementableCumpleCampo = true;


                                    }else if(llave_primaria && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo)) 
                                    {
                                        exiteCumpleCampo = true;
                                        //obtener el ultimo valor numerico introducido
                                        for (int j6 = 0; j6 < Tab.LstTuplas_Tabla.size(); j6++) {                                                                        
                                            if(valor.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j6).HashRegistros_Tabla.get(camp.Nombre_Campo)).ValorCampo_Registro)) exiteCumpleCampo = false;
                                        }

                                        if (exiteCumpleCampo){ exiteCumpleCampo=true; PrimariaCumpleCampo=true;}
                                        else{ ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" es llave primaria y no se puede repetir.", 0, 0); exiteCumpleCampo=false;}

                                    }else if(nulo && valor.Tipo == ConstantesUSQL.TipoNull)
                                    {
                                        valor.Valor=" ";
                                        exiteCumpleCampo=true;

                                    }else if(nulo && !valor.Valor.equals("") && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo))
                                    {
                                        exiteCumpleCampo = true;

                                    }else if(unico && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo)) 
                                    {
                                        exiteCumpleCampo = true;
                                        //obtener el ultimo valor numerico introducido
                                        for (int j7 = 0; j7 < Tab.LstTuplas_Tabla.size(); j7++) {                                                                        
                                            if(valor.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j7).HashRegistros_Tabla.get(camp.Nombre_Campo)).ValorCampo_Registro)) exiteCumpleCampo = false;
                                        }

                                        if (exiteCumpleCampo){ exiteCumpleCampo=true;UnicoCumpleCampo=true;}
                                        else{ ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" es del tipo unico y no se puede repetir.", 0, 0);exiteCumpleCampo=false;}

                                    }else if(no_nulo && !valor.Valor.equals("") && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo))
                                    {
                                        exiteCumpleCampo = true;

                                    }else if(valor.Tipo == ObtenerTipoInt(camp.Tipo_campo))
                                    {
                                        exiteCumpleCampo = true;

                                    }else
                                    {
                                        ConstantesXML.AddError("Base Datos", "El elemento No. "+i+" no es del mismo tipo para el campo "+Ncampo.name, 0, 0);
                                        exiteCumpleCampo = false;
                                        break;
                                    }  
                                    break;
                                }
                            }

                            if(exiteCumpleCampo)
                            {
                               //recorrer cada una de las tuplas y reemplazar por el valor que corresponde
                                for (int k = 0; k < tAux.LstTuplas_Tabla.size(); k++) {
                                    Tupla T=null;
                                    
                                    for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {
                                        if(((Tupla)Tab.LstTuplas_Tabla.get(j)) == ((Tupla)tAux.LstTuplas_Tabla.get(k)) )
                                        {
                                            T = Tab.LstTuplas_Tabla.get(j);
                                            break;
                                        }
                                    }
                                    

                                    if(T.HashRegistros_Tabla.containsKey(Ncampo.name))
                                    {
                                        if(UnicoCumpleCampo || PrimariaCumpleCampo){ConstantesXML.AddError("Base Datos", "No puedes actualizar una llave primaria o campo unico.", Ncampo.linea, Ncampo.columna);break;}
                                        if(AutoIncrementableCumpleCampo)
                                        {
                                            ((Registro)T.HashRegistros_Tabla.get(Ncampo.name)).ValorCampo_Registro = valor.Valor;
                                            valor.Valor = String.valueOf(Integer.valueOf(valor.Valor) + 1);
                                        }else
                                        {
                                            ((Registro)T.HashRegistros_Tabla.get(Ncampo.name)).ValorCampo_Registro = valor.Valor;
                                        }
                                        
                                    }                                    
                                } 
                            }                        
                        }
                    }
                    
                    
                    break;

                case 3:
                    //identificador2() LISTA_ID() VALORES_PARAMETROS() 
                    if(nodoListaID.children.length == nodoValores.children.length)
                    {
                        //por cada parametro 
                        for (int i = 0; i < nodoListaID.children.length; i++) {                        
                            analizador.SimpleNode Ncampo = (analizador.SimpleNode)nodoListaID.children[i];
                            analizador.SimpleNode Nvalor = (analizador.SimpleNode)nodoValores.children[i];
                            
                            ejecutarUSQL.ejecucion RecExp = new ejecucion();
                            ElementoExp valor = RecExp.RecorrerEXP(Nvalor, ambito);
                            
                            boolean exiteCumpleCampo = false;
                            boolean AutoIncrementableCumpleCampo = false;
                            boolean UnicoCumpleCampo = false;
                            boolean PrimariaCumpleCampo = false;
                            
                            for (int j = 0; j < Tab.LstCampos_Tabla.size(); j++) {
                                Campo camp = ((Campo)Tab.LstCampos_Tabla.get(j));
                                if( camp.Nombre_Campo.equals(Ncampo.name) )
                                {
                                    //obtenemos todas las propiedades del campo
                                    boolean nulo= false, no_nulo= false, autoincrementable= false, llave_primaria= false, llave_foranea= false, unico = false;
                                    String foranea="";
                            
                                    for (int jj = 0; jj < camp.Propiedades.size(); jj++) {

                                        if(camp.Propiedades.get(jj).toUpperCase().equals("NULO")) nulo = true;
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("NO NULO")) no_nulo = true;
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("AUTOINCREMENTABLE")) autoincrementable = true;
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("LLAVE_PRIMARIA")) llave_primaria = true;
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("LLAVE_FORANEA")){ llave_foranea = true; foranea = camp.Propiedades.get(j+1);}
                                        if(camp.Propiedades.get(jj).toUpperCase().equals("UNICO")) unico = true;                               
                                    }
                            
                                    if(llave_foranea){
                                
                                        Campo campF =  VerificarForanea(foranea);
                                
                                        if(campF != null)
                                        {
                                    
                                            if((llave_primaria || unico) && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo)) 
                                            {
                                                exiteCumpleCampo = true;
                                                //obtener el ultimo valor numerico introducido
                                                for (int j2 = 0; j2 < Tab.LstTuplas_Tabla.size(); j2++) {                                                                        
                                                    if(valor.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j2).HashRegistros_Tabla.get(camp.Nombre_Campo)).ValorCampo_Registro)) exiteCumpleCampo = false;
                                                }

                                                if(exiteCumpleCampo){

                                                    if(campF.Tipo_campo.equals(camp.Tipo_campo))
                                                    {
                                                        boolean existeDato= false;
                                                        for (int j3 = 0; j3 < ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.size(); j3++) {
                                                            if(((Registro)ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.get(j3).HashRegistros_Tabla.get(campF.Nombre_Campo)).ValorCampo_Registro.equals(valor.Valor))
                                                            {
                                                                existeDato = true;
                                                            }                                            
                                                        }
                                                        if(existeDato)
                                                        {
                                                            exiteCumpleCampo = true;
                                                            PrimariaCumpleCampo = true;
                                                            UnicoCumpleCampo = true;
                                                        }else
                                                        {
                                                            exiteCumpleCampo = false;
                                                            ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es una llave foranea valida.", 0, 0);
                                                        }
                                                    }else
                                                    {
                                                        exiteCumpleCampo = false;
                                                        ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es del mismo tipo que en el campo "+campF.Nombre_Campo+" de la tabla"+ConstantesUSQL.UltimaTablaForanea.Nombre_Tabla, 0, 0);
                                                    }
                                                }else
                                                {
                                                    exiteCumpleCampo = false;
                                                    ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" es primaria o unica po rlo tanto no puede repetirse.", 0, 0);
                                                }

                                            }else if(valor.Tipo == ObtenerTipoInt(camp.Tipo_campo))
                                            {
                                                if(campF.Tipo_campo.equals(camp.Tipo_campo))
                                                {
                                                boolean existeDato= false;
                                                for (int j4 = 0; j4 < ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.size(); j4++) {
                                                    if(((Registro)ConstantesUSQL.UltimaTablaForanea.LstTuplas_Tabla.get(j4).HashRegistros_Tabla.get(campF.Nombre_Campo)).ValorCampo_Registro.equals(valor.Valor))
                                                    {
                                                        existeDato = true;
                                                    }                                            
                                                }
                                                if(existeDato)
                                                {
                                                    exiteCumpleCampo = true;
                                                    
                                                }else
                                                {
                                                    exiteCumpleCampo = false;
                                                    ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es una llave foranea valida.", 0, 0);
                                                }
                                            }else
                                            {
                                                exiteCumpleCampo = false;
                                                ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es del mismo tipo que en el campo "+campF.Nombre_Campo+" de la tabla"+ConstantesUSQL.UltimaTablaForanea.Nombre_Tabla, 0, 0);
                                            }
                                        }else
                                        {
                                            exiteCumpleCampo = false;
                                            ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" para el campo "+camp.Nombre_Campo+" de la tabla "+(((analizador.SimpleNode)nodoActualizar.children[0]).name)+" no es del mismo tipo que en el campo "+camp.Nombre_Campo, 0, 0);
                                        }
                                    }
                                
                                    }else if(autoincrementable && ObtenerTipoInt(camp.Tipo_campo) == ConstantesUSQL.TipoNumero && valor.Tipo == ConstantesUSQL.TipoNumero) 
                                    {
                                        //obtener el ultimo valor numerico introducido
                                        int num=0, aux=0;
                                        for (int j5 = 0; j5 < Tab.LstTuplas_Tabla.size(); j5++) {
                                            aux = Integer.parseInt(((Registro)Tab.LstTuplas_Tabla.get(j5).HashRegistros_Tabla.get(camp.Nombre_Campo)).ValorCampo_Registro);
                                            if(num == 0) num = aux;
                                            if(num<aux) num = aux;
                                        }
                                        valor.Valor =String.valueOf(num+1);
                                        exiteCumpleCampo = true;
                                        AutoIncrementableCumpleCampo = true;


                                    }else if(llave_primaria && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo)) 
                                    {
                                        exiteCumpleCampo = true;
                                        //obtener el ultimo valor numerico introducido
                                        for (int j6 = 0; j6 < Tab.LstTuplas_Tabla.size(); j6++) {                                                                        
                                            if(valor.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j6).HashRegistros_Tabla.get(camp.Nombre_Campo)).ValorCampo_Registro)) exiteCumpleCampo = false;
                                        }

                                        if (exiteCumpleCampo){ exiteCumpleCampo=true; PrimariaCumpleCampo=true;}
                                        else{ ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" es llave primaria y no se puede repetir.", 0, 0); exiteCumpleCampo=false;}

                                    }else if(nulo && valor.Tipo == ConstantesUSQL.TipoNull)
                                    {
                                        valor.Valor=" ";
                                        exiteCumpleCampo=true;

                                    }else if(nulo && !valor.Valor.equals("") && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo))
                                    {
                                        exiteCumpleCampo = true;

                                    }else if(unico && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo)) 
                                    {
                                        exiteCumpleCampo = true;
                                        //obtener el ultimo valor numerico introducido
                                        for (int j7 = 0; j7 < Tab.LstTuplas_Tabla.size(); j7++) {                                                                        
                                            if(valor.Valor.equals(((Registro)Tab.LstTuplas_Tabla.get(j7).HashRegistros_Tabla.get(camp.Nombre_Campo)).ValorCampo_Registro)) exiteCumpleCampo = false;
                                        }

                                        if (exiteCumpleCampo){ exiteCumpleCampo=true;UnicoCumpleCampo=true;}
                                        else{ ConstantesXML.AddError("Base Datos", "El valor "+valor.Valor+" es del tipo unico y no se puede repetir.", 0, 0);exiteCumpleCampo=false;}

                                    }else if(no_nulo && !valor.Valor.equals("") && valor.Tipo == ObtenerTipoInt(camp.Tipo_campo))
                                    {
                                        exiteCumpleCampo = true;

                                    }else if(valor.Tipo == ObtenerTipoInt(camp.Tipo_campo))
                                    {
                                        exiteCumpleCampo = true;

                                    }else
                                    {
                                        ConstantesXML.AddError("Base Datos", "El elemento No. "+i+" no es del mismo tipo para el campo "+Ncampo.name, 0, 0);
                                        exiteCumpleCampo = false;
                                        break;
                                    }  
                                    break;
                                }
                            }

                            if(exiteCumpleCampo)
                            {
                               //recorrer cada una de las tuplas y reemplazar por el valor que corresponde
                                for (int j = 0; j < Tab.LstTuplas_Tabla.size(); j++) {
                                    Tupla T = Tab.LstTuplas_Tabla.get(j);

                                    if(T.HashRegistros_Tabla.containsKey(Ncampo.name))
                                    {
                                        if(UnicoCumpleCampo || PrimariaCumpleCampo){ConstantesXML.AddError("Base Datos", "No puedes actualizar una llave primaria o campo unico.", Ncampo.linea, Ncampo.columna);break;}
                                        if(AutoIncrementableCumpleCampo)
                                        {
                                            ((Registro)T.HashRegistros_Tabla.get(Ncampo.name)).ValorCampo_Registro = valor.Valor;
                                            valor.Valor = String.valueOf(Integer.valueOf(valor.Valor) + 1);
                                        }else
                                        {
                                            ((Registro)T.HashRegistros_Tabla.get(Ncampo.name)).ValorCampo_Registro = valor.Valor;
                                        }
                                        
                                    }                                    
                                } 
                            }                        
                        }
                    }                                        
                    break;
            }
            //quitar y reemplazar tabla en BD actual
            ConstantesUSQL.BDActual.HashTablas.remove(Tab.Nombre_Tabla);
            ConstantesUSQL.BDActual.HashTablas.put(Tab.Nombre_Tabla,Tab);
            //quitar y reemplazar BD general
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.remove(ConstantesUSQL.BDActual.Nombre_BD);
            ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(ConstantesUSQL.BDActual.Nombre_BD,ConstantesUSQL.BDActual);
            
            ConstantesUSQL.CadenaMsj += " Actualizar -> Actualizacion de registros. "+ConstantesUSQL.Separador;
            ConstantesUSQL.ultimoResultado = " Actualizacion de registros. ";
        }else
        {
            //error tabla no existe en BD actual
            ConstantesXML.AddError("Base Datos", "La tabla "+ ((analizador.SimpleNode)nodoActualizar.children[0]).name+ " no existe en base de datos "+ConstantesUSQL.BDActual.Nombre_BD, ((analizador.SimpleNode)nodoActualizar.children[0]).linea, ((analizador.SimpleNode)nodoActualizar.children[0]).columna);
        }    
    }
    
    public Tabla FiltrarPorCondicion(analizador.SimpleNode Condicion,Tabla tab,String Ambito)
    {
        Tabla tablaReturn=new Tabla("TabAux", "", tab.LstCampos_Tabla,new ArrayList<Tupla>());
        
        if(tab!=null)
        {
            analizador.SimpleNode Relacional = (analizador.SimpleNode) Condicion.children[0];
            //ARIT() simbolo ARIT()
            if(Relacional.children.length==3)
            {
                ejecutarUSQL.ejecucion RecExp = new ejecucion();
                
                ConstantesXML.EsCondicion =true;                
                ElementoExp NombreCampoComparar = RecExp.RecorrerEXP((analizador.SimpleNode)Relacional.children[0], Ambito);
                ConstantesXML.EsCondicion =false;
                ConstantesXML.EsCondicion =true;
                ElementoExp ValorComparar = RecExp.RecorrerEXP((analizador.SimpleNode)Relacional.children[2], Ambito);
                ConstantesXML.EsCondicion =false;
                boolean existeCampo= false;
                boolean existeCampo2= false;
                Campo camp=null;
                Campo camp2=null;
                //recorrer los campos de la tabla y verificar que el NombreCampoComparar exista
                for (int i = 0; i < tab.LstCampos_Tabla.size(); i++) {
                    
                    if(((Campo)tab.LstCampos_Tabla.get(i)).Nombre_Campo.equals(NombreCampoComparar.Valor))
                    {
                        existeCampo = true;
                        camp = tab.LstCampos_Tabla.get(i);
                        break;
                    }
                }
                
                for (int i = 0; i < tab.LstCampos_Tabla.size(); i++) {
                    
                    if(((Campo)tab.LstCampos_Tabla.get(i)).Nombre_Campo.equals(ValorComparar.Valor))
                    {
                        existeCampo2 = true;
                        camp2 = tab.LstCampos_Tabla.get(i);
                        break;
                    }
                }
                
                if(existeCampo && !existeCampo2)
                {
                    ValorComparar = RecExp.RecorrerEXP((analizador.SimpleNode)Relacional.children[2], Ambito);
                    if(ValorComparar != null){
                    if(ObtenerTipoInt(camp.Tipo_campo) == ValorComparar.Tipo)
                    {
                        for (int i = 0; i < tab.LstTuplas_Tabla.size(); i++) {
                            Tupla tup =  tab.LstTuplas_Tabla.get(i);
                            
                            Registro Reg = (Registro) tup.HashRegistros_Tabla.get(camp.Nombre_Campo);
                            
                            switch(((analizador.SimpleNode)Relacional.children[1]).name)
                            {
                                case "==":
                                    if(Reg.ValorCampo_Registro.equals(ValorComparar.Valor)) 
                                    {                                        
                                        tablaReturn.LstTuplas_Tabla.add(tup);
                                    }
                                    break;
                                case "!=":
                                    if(!Reg.ValorCampo_Registro.equals(ValorComparar.Valor)) 
                                    {
                                        tablaReturn.LstTuplas_Tabla.add(tup);
                                    }
                                    break;
                                case "<":
                                    if(ValorComparar.Tipo == ConstantesUSQL.TipoNumero || ValorComparar.Tipo == ConstantesUSQL.TipoDouble)
                                    {
                                        if(Integer.valueOf(Reg.ValorCampo_Registro) < Integer.valueOf(ValorComparar.Valor)) 
                                        {
                                            tablaReturn.LstTuplas_Tabla.add(tup);
                                        }
                                    }else
                                    {
                                        ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no es del tipo numerico para comparar <.", Relacional.linea, Relacional.columna);
                                    }
                                    break;
                                case ">":
                                    if(ValorComparar.Tipo == ConstantesUSQL.TipoNumero || ValorComparar.Tipo == ConstantesUSQL.TipoDouble)
                                    {
                                        if(Integer.valueOf(Reg.ValorCampo_Registro) > Integer.valueOf(ValorComparar.Valor)) 
                                        {
                                            tablaReturn.LstTuplas_Tabla.add(tup);
                                        }
                                    }else
                                    {
                                        ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no es del tipo numerico para comparar <.", Relacional.linea, Relacional.columna);
                                    }
                                    break;
                                case "<=":
                                    if(ValorComparar.Tipo == ConstantesUSQL.TipoNumero || ValorComparar.Tipo == ConstantesUSQL.TipoDouble)
                                    {
                                        if(Integer.valueOf(Reg.ValorCampo_Registro) <= Integer.valueOf(ValorComparar.Valor)) 
                                        {
                                            tablaReturn.LstTuplas_Tabla.add(tup);
                                        }
                                    }else
                                    {
                                        ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no es del tipo numerico para comparar <.", Relacional.linea, Relacional.columna);
                                    }
                                    break;
                                case ">=":
                                    if(ValorComparar.Tipo == ConstantesUSQL.TipoNumero || ValorComparar.Tipo == ConstantesUSQL.TipoDouble)
                                    {
                                        if(Integer.valueOf(Reg.ValorCampo_Registro) >= Integer.valueOf(ValorComparar.Valor)) 
                                        {
                                            tablaReturn.LstTuplas_Tabla.add(tup);
                                        }
                                    }else
                                    {
                                        ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no es del tipo numerico para comparar <.", Relacional.linea, Relacional.columna);
                                    }
                                    break;
                            }
                            
                        }
                            
                    }else
                    {
                        ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no coincide con el existente en la tabla.", Relacional.linea, Relacional.columna);
                    }
                    }
                }else if(existeCampo && existeCampo2)
                {
                    
                    for (int i = 0; i < tab.LstTuplas_Tabla.size(); i++) {
                        Tupla tup =  tab.LstTuplas_Tabla.get(i);
                            
                        Registro Reg = (Registro) tup.HashRegistros_Tabla.get(camp.Nombre_Campo);
                        Registro Reg2 = (Registro) tup.HashRegistros_Tabla.get(camp2.Nombre_Campo);
                                
                        switch(((analizador.SimpleNode)Relacional.children[1]).name)
                        {
                            case "==":
                                if(Reg.ValorCampo_Registro.equals(Reg2.ValorCampo_Registro)) 
                                {                                        
                                    tablaReturn.LstTuplas_Tabla.add(tup);
                                }
                                break;
                            case "!=":
                                if(!Reg.ValorCampo_Registro.equals(Reg2.ValorCampo_Registro)) 
                                {
                                    tablaReturn.LstTuplas_Tabla.add(tup);
                                }
                                break;
                            case "<":
                                try
                                {
                                    if(Integer.valueOf(Reg.ValorCampo_Registro) < Integer.valueOf(Reg2.ValorCampo_Registro)) 
                                    {
                                        tablaReturn.LstTuplas_Tabla.add(tup);
                                    }
                                }catch(Exception e)
                                {
                                    ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no es del tipo numerico para comparar <.", Relacional.linea, Relacional.columna);
                                }
                                break;
                            case ">":
                                try
                                {
                                    if(Integer.valueOf(Reg.ValorCampo_Registro) > Integer.valueOf(Reg2.ValorCampo_Registro)) 
                                    {
                                        tablaReturn.LstTuplas_Tabla.add(tup);
                                    }
                                }catch(Exception e)
                                {
                                    ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no es del tipo numerico para comparar >.", Relacional.linea, Relacional.columna);
                                }
                                break;
                            case "<=":
                                try
                                {
                                    if(Integer.valueOf(Reg.ValorCampo_Registro) <= Integer.valueOf(Reg2.ValorCampo_Registro)) 
                                    {
                                        tablaReturn.LstTuplas_Tabla.add(tup);
                                    }
                                }catch(Exception e)
                                {
                                    ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no es del tipo numerico para comparar <=.", Relacional.linea, Relacional.columna);
                                }
                                break;
                            case ">=":
                                try
                                {
                                    if(Integer.valueOf(Reg.ValorCampo_Registro) >= Integer.valueOf(Reg2.ValorCampo_Registro)) 
                                    {
                                        tablaReturn.LstTuplas_Tabla.add(tup);
                                    }
                                }catch(Exception e)
                                {
                                    ConstantesXML.AddError("Base Datos", "El tipo del campo en la condicion no es del tipo numerico para comparar >=.", Relacional.linea, Relacional.columna);
                                }
                                break;
                        }             
                    }

                }else
                {
                    ConstantesXML.AddError("Base Datos", "El campo en la condicion no existe en la tabla.", Relacional.linea, Relacional.columna);
                }
                
            }else
            {
                ConstantesXML.AddError("Base Datos", "la condicion no cumple con una exprecion relacional.", Relacional.linea, Relacional.columna);
            }           
        }
        
        return tablaReturn;
    }
    
    public Tabla convinarTuplas(Tabla tablaAnd, Tabla tablaOR){
        
        for (int i = 0; i < tablaAnd.LstTuplas_Tabla.size(); i++) {
            
            for (int j = 0; j < tablaOR.LstTuplas_Tabla.size(); j++) {
                if(((Tupla)tablaAnd.LstTuplas_Tabla.get(i)) == ((Tupla)tablaOR.LstTuplas_Tabla.get(j)) )
                {
                    tablaOR.LstTuplas_Tabla.remove(j);
                }
            }                        
        }
        for (int i = 0; i < tablaOR.LstTuplas_Tabla.size(); i++) {
            tablaAnd.LstTuplas_Tabla.add(((Tupla)tablaOR.LstTuplas_Tabla.get(i)));
        }
        return tablaAnd;
    }
    
    public Tabla SeleccionarTabla(analizador.SimpleNode nodo, String Ambito){
        Tabla tabSelec = new Tabla("TablaSeleccionada", "", new ArrayList<Campo>(), new ArrayList<Tupla>());
        
        //(LISTA_ID_SELECT() | <TK_POR>) LISTA_ID() CONDICIONES()? (identificador2()(tk_asc()|tk_desc()) )?
        analizador.SimpleNode nodoCondiciones;
        analizador.SimpleNode NodoListaTablas;
        
        switch(nodo.children.length)
        {
            case 1:
                //<TK_POR> LISTA_ID()
                NodoListaTablas = (analizador.SimpleNode)nodo.children[0];
                tabSelec = FormarTablota(NodoListaTablas);
                //GENERAR PAQUETE USQL
                
                break;
            case 2:
                if(AnalizadorTreeConstants.jjtNodeName[((analizador.SimpleNode)nodo.children[0]).id].equals("LISTA_ID_SELECT"))
                {
                    //LISTA_ID_SELECT() LISTA_ID()
                    NodoListaTablas = (analizador.SimpleNode)nodo.children[1];
                    tabSelec = FormarTablota(NodoListaTablas);
                    //filtrar por campos
                    tabSelec = SeleccionarCampos(tabSelec,(analizador.SimpleNode)nodo.children[0]);
                    //GENERAR PAQUETE USQL
                    
                }else
                {
                    //<TK_POR> LISTA_ID() CONDICIONES()
                    NodoListaTablas = (analizador.SimpleNode)nodo.children[0];
                    tabSelec = FormarTablota(NodoListaTablas);
                    
                    //validar condiciones
                    nodoCondiciones = (analizador.SimpleNode)nodo.children[1];
                    
                    Tabla tAux = new Tabla("tabAux","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla);
                    
                    int anterior = 0;
                    for (int i = 0; i < nodoCondiciones.children.length; i++) {
                        analizador.SimpleNode Nodocondicion = (analizador.SimpleNode) nodoCondiciones.children[i];
                        if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("CONDICION"))
                        {
                            if(anterior ==0)
                            {
                                tAux = FiltrarPorCondicion(Nodocondicion,tAux,Ambito);
                            }else
                            {
                                tAux = convinarTuplas(tAux,FiltrarPorCondicion(Nodocondicion,new Tabla("tabAux2","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla),Ambito));
                            }
                            
                        }else if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("tk_and"))
                        {
                            anterior=0;
                        }else
                        {
                            //tk_or
                            anterior=1;
                        }                        
                    }
                    tabSelec = tAux;
                    //GENERAR PAQUETE USQL
                }
                System.out.println("Finalizo Select.");
                break;
            case 3:
                if(AnalizadorTreeConstants.jjtNodeName[((analizador.SimpleNode)nodo.children[0]).id].equals("LISTA_ID_SELECT"))
                {
                    //LISTA_ID_SELECT() LISTA_ID() CONDICIONES()
                    NodoListaTablas = (analizador.SimpleNode)nodo.children[1];
                    tabSelec = FormarTablota(NodoListaTablas);

                    //validar condiciones
                    nodoCondiciones = (analizador.SimpleNode)nodo.children[2];
                    
                    Tabla tAux = new Tabla("tabAux","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla);
                    
                    int anterior = 0;
                    for (int i = 0; i < nodoCondiciones.children.length; i++) {
                        analizador.SimpleNode Nodocondicion = (analizador.SimpleNode) nodoCondiciones.children[i];
                        if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("CONDICION"))
                        {
                            if(anterior ==0)
                            {
                                tAux = FiltrarPorCondicion(Nodocondicion,tAux,Ambito);
                            }else
                            {
                                tAux = convinarTuplas(tAux,FiltrarPorCondicion(Nodocondicion,new Tabla("tabAux2","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla),Ambito));
                            }
                            
                        }else if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("tk_and"))
                        {
                            anterior=0;
                        }else
                        {
                            //tk_or
                            anterior=1;
                        }                        
                    }
                    
                    //filtrar por campos
                    tabSelec = SeleccionarCampos(tAux,(analizador.SimpleNode)nodo.children[0]);
                    //GENERAR PAQUETE USQL
                }else
                {
                    //<TK_POR> LISTA_ID() identificador2()(tk_asc()|tk_desc())
                    NodoListaTablas = (analizador.SimpleNode)nodo.children[0];
                    tabSelec = FormarTablota(NodoListaTablas);
                    
                    //ORDENAR
                    try {
                        analizador.SimpleNode nodoAtr = (analizador.SimpleNode)nodo.children[1];
                        analizador.SimpleNode nodoOrden = (analizador.SimpleNode)nodo.children[2];

                        Collections.sort(tabSelec.LstTuplas_Tabla, new Comparator<Tupla>() {
                            public int compare(Tupla obj1, Tupla obj2) {
                               return( (Registro)obj1.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro.compareTo(( (Registro)obj2.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro);
                            }
                        });

                        if (nodoOrden.name.toUpperCase().equals("DESC")) {
                            Collections.reverse(tabSelec.LstTuplas_Tabla);
                        }
                        //GENERAR PAQUETE USQL
                        
                    } catch (Exception e) {
                        ConstantesXML.AddError("Base Datos", "El campo para ordenar no es valido.", 0,0);
                    }
                        
                                        
           
                }
                
                break;
            case 4:
                if(AnalizadorTreeConstants.jjtNodeName[((analizador.SimpleNode)nodo.children[0]).id].equals("LISTA_ID_SELECT"))
                {
                    //LISTA_ID_SELECT() LISTA_ID() (identificador2()(tk_asc()|tk_desc()) )
                    NodoListaTablas = (analizador.SimpleNode)nodo.children[1];
                    tabSelec = FormarTablota(NodoListaTablas);
                    
                    //filtrar por campos
                    tabSelec = SeleccionarCampos(tabSelec,(analizador.SimpleNode)nodo.children[0]);
                    
                    //ORDENAR
                    try {
                        analizador.SimpleNode nodoAtr = (analizador.SimpleNode)nodo.children[2];
                        analizador.SimpleNode nodoOrden = (analizador.SimpleNode)nodo.children[3];

                        Collections.sort(tabSelec.LstTuplas_Tabla, new Comparator<Tupla>() {
                            public int compare(Tupla obj1, Tupla obj2) {
                               return( (Registro)obj1.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro.compareTo(( (Registro)obj2.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro);
                            }
                        });

                        if (nodoOrden.name.toUpperCase().equals("DESC")) {
                            Collections.reverse(tabSelec.LstTuplas_Tabla);
                        }
                    } catch (Exception e) {
                        ConstantesXML.AddError("Base Datos", "El campo para ordenar no es valido.", 0,0);
                    }
                    
                    
                    //GENERAR PAQUETE USQL                       
                    
                }else
                {
                    if(AnalizadorTreeConstants.jjtNodeName[((analizador.SimpleNode)nodo.children[1]).id].equals("CONDICIONES"))
                    {
                        //<TK_POR> LISTA_ID() CONDICIONES() identificador2()(tk_asc()|tk_desc())                        
                        
                        NodoListaTablas = (analizador.SimpleNode)nodo.children[0];
                        tabSelec = FormarTablota(NodoListaTablas);

                        //validar condiciones
                        nodoCondiciones = (analizador.SimpleNode)nodo.children[1];
                    
                        Tabla tAux = new Tabla("tabAux","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla);
                    
                        int anterior = 0;
                        for (int i = 0; i < nodoCondiciones.children.length; i++) {
                            analizador.SimpleNode Nodocondicion = (analizador.SimpleNode) nodoCondiciones.children[i];
                            if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("CONDICION"))
                            {
                                if(anterior ==0)
                                {
                                    tAux = FiltrarPorCondicion(Nodocondicion,tAux,Ambito);
                                }else
                                {
                                    tAux = convinarTuplas(tAux,FiltrarPorCondicion(Nodocondicion,new Tabla("tabAux2","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla),Ambito));
                                }

                            }else if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("tk_and"))
                            {
                                anterior=0;
                            }else
                            {
                                //tk_or
                                anterior=1;
                            }                        
                        }
                        tabSelec = tAux;
                        //ORDENAR
                        try {
                            analizador.SimpleNode nodoAtr = (analizador.SimpleNode)nodo.children[2];
                            analizador.SimpleNode nodoOrden = (analizador.SimpleNode)nodo.children[3];

                            Collections.sort(tabSelec.LstTuplas_Tabla, new Comparator<Tupla>() {
                                public int compare(Tupla obj1, Tupla obj2) {
                                   return( (Registro)obj1.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro.compareTo(( (Registro)obj2.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro);
                                }
                            });

                            if (nodoOrden.name.toUpperCase().equals("DESC")) {
                                Collections.reverse(tabSelec.LstTuplas_Tabla);
                            }
                            
                        } catch (Exception e) {
                        ConstantesXML.AddError("Base Datos", "El campo para ordenar no es valido.", 0,0);
                        }
                            

                        //GENERAR PAQUETE USQL      
                        
                    }else
                    {
                        //<TK_POR> LISTA_ID() identificador2() identificador2() (tk_asc()|tk_desc())
                        NodoListaTablas = (analizador.SimpleNode)nodo.children[0];
                        tabSelec = FormarTablota(NodoListaTablas);

                        //ORDENAR
                        try {
                            analizador.SimpleNode nodoAtr = (analizador.SimpleNode)nodo.children[1];
                            analizador.SimpleNode nodoAtr2 = (analizador.SimpleNode)nodo.children[2];
                            analizador.SimpleNode nodoOrden = (analizador.SimpleNode)nodo.children[3];

                            Collections.sort(tabSelec.LstTuplas_Tabla, new Comparator<Tupla>() {
                                public int compare(Tupla obj1, Tupla obj2) {
                                   return( (Registro)obj1.HashRegistros_Tabla.get(nodoAtr.name+"."+nodoAtr2.name)).ValorCampo_Registro.compareTo(( (Registro)obj2.HashRegistros_Tabla.get(nodoAtr.name+"."+nodoAtr2.name)).ValorCampo_Registro);
                                }
                            });

                            if (nodoOrden.name.toUpperCase().equals("DESC")) {
                                Collections.reverse(tabSelec.LstTuplas_Tabla);
                            }

                            //GENERAR PAQUETE USQL
                        } catch (Exception e) {
                            ConstantesXML.AddError("Base Datos", "El campo para ordenar no es valido.", 0,0);
                        }
                        

                    }                                  
                }                
                
                break;
            case 5:
                if(AnalizadorTreeConstants.jjtNodeName[((analizador.SimpleNode)nodo.children[0]).id].equals("LISTA_ID_SELECT"))
                {
                    
                    if(AnalizadorTreeConstants.jjtNodeName[((analizador.SimpleNode)nodo.children[2]).id].equals("CONDICIONES"))
                    {
                        //LISTA_ID_SELECT() LISTA_ID() CONDICIONES() identificador2()(tk_asc()|tk_desc()) 
                        NodoListaTablas = (analizador.SimpleNode)nodo.children[1];
                        tabSelec = FormarTablota(NodoListaTablas);
                        
                        //validar condiciones
                        nodoCondiciones = (analizador.SimpleNode)nodo.children[2];

                        Tabla tAux = new Tabla("tabAux","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla);

                        int anterior = 0;
                        for (int i = 0; i < nodoCondiciones.children.length; i++) {
                            analizador.SimpleNode Nodocondicion = (analizador.SimpleNode) nodoCondiciones.children[i];
                            if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("CONDICION"))
                            {
                                if(anterior ==0)
                                {
                                    tAux = FiltrarPorCondicion(Nodocondicion,tAux,Ambito);
                                }else
                                {
                                    tAux = convinarTuplas(tAux,FiltrarPorCondicion(Nodocondicion,new Tabla("tabAux2","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla),Ambito));
                                }

                            }else if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("tk_and"))
                            {
                                anterior=0;
                            }else
                            {
                                //tk_or
                                anterior=1;
                            }                        
                        }
                        
                        //filtrar por campos
                        tabSelec = SeleccionarCampos(tAux,(analizador.SimpleNode)nodo.children[0]);
                        
                        
                        //ORDENAR
                        try {
                            analizador.SimpleNode nodoAtr = (analizador.SimpleNode)nodo.children[2];
                            analizador.SimpleNode nodoOrden = (analizador.SimpleNode)nodo.children[3];

                            Collections.sort(tabSelec.LstTuplas_Tabla, new Comparator<Tupla>() {
                                public int compare(Tupla obj1, Tupla obj2) {
                                   return( (Registro)obj1.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro.compareTo(( (Registro)obj2.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro);
                                }
                            });

                            if (nodoOrden.name.toUpperCase().equals("DESC")) {
                                Collections.reverse(tabSelec.LstTuplas_Tabla);
                            }    
                            //GENERAR PAQUETE USQL 
                        } catch (Exception e) {
                            ConstantesXML.AddError("Base Datos", "El campo para ordenar no es valido.", 0,0);
                        }
                             
                        
                    }else
                    {
                        //LISTA_ID_SELECT() LISTA_ID() (identificador2()(tk_asc()|tk_desc()) )
                        NodoListaTablas = (analizador.SimpleNode)nodo.children[1];
                        tabSelec = FormarTablota(NodoListaTablas);

                        //filtrar por campos
                        tabSelec = SeleccionarCampos(tabSelec,(analizador.SimpleNode)nodo.children[0]);

                        //ORDENAR
                        try {
                            analizador.SimpleNode nodoAtr = (analizador.SimpleNode)nodo.children[2];
                            analizador.SimpleNode nodoOrden = (analizador.SimpleNode)nodo.children[3];

                            Collections.sort(tabSelec.LstTuplas_Tabla, new Comparator<Tupla>() {
                                public int compare(Tupla obj1, Tupla obj2) {
                                   return( (Registro)obj1.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro.compareTo(( (Registro)obj2.HashRegistros_Tabla.get(nodoAtr.name)).ValorCampo_Registro);
                                }
                            });

                            if (nodoOrden.name.toUpperCase().equals("DESC")) {
                                Collections.reverse(tabSelec.LstTuplas_Tabla);
                            }

                            //GENERAR PAQUETE USQL  
                        } catch (Exception e) {
                            ConstantesXML.AddError("Base Datos", "El campo para ordenar no es valido.", 0,0);
                        }
                            
                    }
           
                }else
                {
                    //<TK_POR> LISTA_ID() CONDICIONES() identificador2() identificador2()(tk_asc()|tk_desc())                        
                        
                    NodoListaTablas = (analizador.SimpleNode)nodo.children[0];
                    tabSelec = FormarTablota(NodoListaTablas);

                    //validar condiciones
                    nodoCondiciones = (analizador.SimpleNode)nodo.children[1];

                    Tabla tAux = new Tabla("tabAux","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla);

                    int anterior = 0;
                    for (int i = 0; i < nodoCondiciones.children.length; i++) {
                        analizador.SimpleNode Nodocondicion = (analizador.SimpleNode) nodoCondiciones.children[i];
                        if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("CONDICION"))
                        {
                            if(anterior ==0)
                            {
                                tAux = FiltrarPorCondicion(Nodocondicion,tAux,Ambito);
                            }else
                            {
                                tAux = convinarTuplas(tAux,FiltrarPorCondicion(Nodocondicion,new Tabla("tabAux2","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla),Ambito));
                            }

                        }else if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("tk_and"))
                        {
                            anterior=0;
                        }else
                        {
                            //tk_or
                            anterior=1;
                        }                        
                    }
                    tabSelec= tAux;
                    //ORDENAR
                    try {
                        analizador.SimpleNode nodoAtr = (analizador.SimpleNode)nodo.children[2];
                        analizador.SimpleNode nodoAtr2 = (analizador.SimpleNode)nodo.children[3];
                        analizador.SimpleNode nodoOrden = (analizador.SimpleNode)nodo.children[4];

                        Collections.sort(tabSelec.LstTuplas_Tabla, new Comparator<Tupla>() {
                            public int compare(Tupla obj1, Tupla obj2) {
                               return( (Registro)obj1.HashRegistros_Tabla.get(nodoAtr.name+"."+nodoAtr2.name)).ValorCampo_Registro.compareTo(( (Registro)obj2.HashRegistros_Tabla.get(nodoAtr.name+"."+nodoAtr2.name)).ValorCampo_Registro);
                            }
                        });

                        if (nodoOrden.name.toUpperCase().equals("DESC")) {
                            Collections.reverse(tabSelec.LstTuplas_Tabla);
                        }

                        //GENERAR PAQUETE USQL 
                    } catch (Exception e) {
                        ConstantesXML.AddError("Base Datos", "El campo para ordenar no es valido.", 0,0);
                    }
                        
                }           
                    
                break;
            case 6:
                //LISTA_ID_SELECT() LISTA_ID() CONDICIONES() identificador2() identificador2() (tk_asc()|tk_desc()) 
                
                
                NodoListaTablas = (analizador.SimpleNode)nodo.children[1];
                tabSelec = FormarTablota(NodoListaTablas);

                //validar condiciones
                nodoCondiciones = (analizador.SimpleNode)nodo.children[2];

                Tabla tAux = new Tabla("tabAux","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla);

                int anterior = 0;
                for (int i = 0; i < nodoCondiciones.children.length; i++) {
                    analizador.SimpleNode Nodocondicion = (analizador.SimpleNode) nodoCondiciones.children[i];
                    if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("CONDICION"))
                    {
                        if(anterior ==0)
                        {
                            tAux = FiltrarPorCondicion(Nodocondicion,tAux,Ambito);
                        }else
                        {
                            tAux = convinarTuplas(tAux,FiltrarPorCondicion(Nodocondicion,new Tabla("tabAux2","", tabSelec.LstCampos_Tabla, tabSelec.LstTuplas_Tabla),Ambito));
                        }

                    }else if(AnalizadorTreeConstants.jjtNodeName[Nodocondicion.id].equals("tk_and"))
                    {
                        anterior=0;
                    }else
                    {
                        //tk_or
                        anterior=1;
                    }                        
                }

                //filtrar por campos
                tabSelec = SeleccionarCampos(tAux,(analizador.SimpleNode)nodo.children[0]);


                //ORDENAR
                try {
                    analizador.SimpleNode nodoAtr = (analizador.SimpleNode)nodo.children[3];
                    analizador.SimpleNode nodoAtr2 = (analizador.SimpleNode)nodo.children[4];
                    analizador.SimpleNode nodoOrden = (analizador.SimpleNode)nodo.children[5];

                    Collections.sort(tabSelec.LstTuplas_Tabla, new Comparator<Tupla>() {
                        public int compare(Tupla obj1, Tupla obj2) {
                           return( (Registro)obj1.HashRegistros_Tabla.get(nodoAtr.name+"."+nodoAtr2.name)).ValorCampo_Registro.compareTo(( (Registro)obj2.HashRegistros_Tabla.get(nodoAtr.name+"."+nodoAtr2.name)).ValorCampo_Registro);
                        }
                    });

                    if (nodoOrden.name.toUpperCase().equals("DESC")) {
                        Collections.reverse(tabSelec.LstTuplas_Tabla);
                    }    
                    //GENERAR PAQUETE USQL
                } catch (Exception e) {
                    ConstantesXML.AddError("Base Datos", "El campo para ordenar no es valido.", 0,0);
                }
                  
                
                
                break;
        }
        return tabSelec;
    }
    
    public Tabla FormarTablota(analizador.SimpleNode ListaIDTablas)
    {
        Tabla tb = new Tabla("tablota", "",new ArrayList<Campo>(), new ArrayList<Tupla>());
        
        if(ListaIDTablas.children.length==1){
            analizador.SimpleNode nombreTab = (analizador.SimpleNode) ListaIDTablas.children[0];
                if(ConstantesUSQL.BDActual.HashTablas.containsKey(nombreTab.name))
                {
                     Tabla tbAux = (Tabla)ConstantesUSQL.BDActual.HashTablas.get(nombreTab.name);
                     tb.LstCampos_Tabla = tbAux.LstCampos_Tabla;
                     tb.LstTuplas_Tabla = tbAux.LstTuplas_Tabla;
                     
                }else
                {
                    tb = new Tabla("tablota", "",new ArrayList<Campo>(), new ArrayList<Tupla>());
                    ConstantesXML.AddError("Base Datos", "La tabla a seleccionar no pertenece a la Base de datos actual.", nombreTab.linea, nombreTab.columna);
                }
        }else
        {
            //ArrayList<Tupla> TPTabla1 = new ArrayList<Tupla>();            
            Tabla tab1 = null;
            
             for (int i = 0; i < ListaIDTablas.children.length; i++) {
                analizador.SimpleNode nombreTab = (analizador.SimpleNode) ListaIDTablas.children[i];
                if(ConstantesUSQL.BDActual.HashTablas.containsKey(nombreTab.name))
                {
                    Tabla tbAux = (Tabla)ConstantesUSQL.BDActual.HashTablas.get(nombreTab.name);
                    //agregar parametros a tablota
                    for (int j = 0; j < tbAux.LstCampos_Tabla.size(); j++) {
                        Campo cp = tbAux.LstCampos_Tabla.get(j);
                        // se le cambia el nombre al parametro poniendo prefijo nombre de tabla
//                        cp.Nombre_Campo = nombreTab.name + "."+cp.Nombre_Campo;
                        //ese campo se agrega a campos de tablota
                        tb.LstCampos_Tabla.add(new Campo(cp.Tipo_campo, nombreTab.name + "."+cp.Nombre_Campo, cp.Propiedades));
                    }
                    
                    if(tab1!=null){
                        //mas de una tabla
                        //ArrayList<Tupla> TPTabla2 = new ArrayList<Tupla>();
                        Tabla tab2 = new Tabla(nombreTab.name, null, null, tbAux.LstTuplas_Tabla);
                        
                        //TPTabla2 = tbAux.LstTuplas_Tabla;
                        
                        for (int j = 0; j < tab1.LstTuplas_Tabla.size(); j++) {
                            Tupla tuplaTabla1 = tab1.LstTuplas_Tabla.get(j);
                            
                            for (int k = 0; k < tab2.LstTuplas_Tabla.size(); k++) {
                                Tupla tuplaTabla2 = tab2.LstTuplas_Tabla.get(k);
                                Hashtable nuevos = new Hashtable();
                                
                                Vector v = new Vector(tuplaTabla1.HashRegistros_Tabla.keySet());
                                Collections.sort(v);
                                Iterator it = v.iterator();

                                Registro reg;                                                    

                                while (it.hasNext()) {  
                                    //obtener llaves de hash
                                    String element =  (String)it.next();               
                                    //obtenr el registro
                                    reg = new Registro(tab1.Nombre_Tabla + "."+((Registro)tuplaTabla1.HashRegistros_Tabla.get(element)).NombreCampo_Registro, ((Registro)tuplaTabla1.HashRegistros_Tabla.get(element)).ValorCampo_Registro);
                                    //(Registro)tuplaTabla1.HashRegistros_Tabla.get(element);
//                                    reg.NombreCampo_Registro = tab1.Nombre_Tabla + "."+reg.NombreCampo_Registro;
                                    
//                                    nuevos.put(tab1.Nombre_Tabla + "."+reg.NombreCampo_Registro, reg);
                                    nuevos.put(reg.NombreCampo_Registro, reg);
                                }
                                
                                Vector v2 = new Vector(tuplaTabla2.HashRegistros_Tabla.keySet());
                                Collections.sort(v2);
                                Iterator it2 = v2.iterator();

                                Registro reg2;                                                    

                                while (it2.hasNext()) {  
                                    //obtener llaves de hash
                                    String element =  (String)it2.next();               
                                    //obtenr el registro
                                    reg2 = new Registro(tab2.Nombre_Tabla + "."+((Registro) tuplaTabla2.HashRegistros_Tabla.get(element)).NombreCampo_Registro, ((Registro) tuplaTabla2.HashRegistros_Tabla.get(element)).ValorCampo_Registro);
                                    //reg2 = (Registro) tuplaTabla2.HashRegistros_Tabla.get(element);
//                                    reg2.NombreCampo_Registro = tab2.Nombre_Tabla + "."+reg2.NombreCampo_Registro;
                                    
//                                    nuevos.put(tab2.Nombre_Tabla + "."+reg2.NombreCampo_Registro, reg2);
                                    nuevos.put(reg2.NombreCampo_Registro, reg2);
                                }
                                
                                tb.LstTuplas_Tabla.add(new Tupla(nuevos));
                                
                            }                          
                        }
                        tab1 = new Tabla(nombreTab.name, null, null, new ArrayList<Tupla>());                        
                        for (int j = 0; j < tb.LstTuplas_Tabla.size(); j++) {
                            tab1.LstTuplas_Tabla.add(tb.LstTuplas_Tabla.get(j));
                        }
                    }else
                    {
                        tab1 = new Tabla(nombreTab.name, null, null, new ArrayList<Tupla>());
                        for (int j = 0; j < tbAux.LstTuplas_Tabla.size(); j++) {
                            tab1.LstTuplas_Tabla.add(tbAux.LstTuplas_Tabla.get(j));
                        }
                        
                    } 
                    
                    
//                    ArrayList<Tupla> arr = new ArrayList<Tupla>();
//                    
//                    for (int j = 0; j < tbAux.LstTuplas_Tabla.size(); j++) {
//                        Tupla tpAux = tbAux.LstTuplas_Tabla.get(j);
//                        //recorrer cada uno de los registros 
//
//                        Vector v = new Vector(tpAux.HashRegistros_Tabla.keySet());
//                        Collections.sort(v);
//                        Iterator it = v.iterator();
//
//                        Registro reg;                    
//                        Tupla primeraTp = new Tupla(new Hashtable());                    
//
//                        while (it.hasNext()) {  
//                            //obtener llaves de hash
//                            String element =  (String)it.next();               
//                            //obtenr el registro
//                            reg = (Registro) tpAux.HashRegistros_Tabla.get(element);
//                            reg.NombreCampo_Registro = nombreTab.name + "."+reg.NombreCampo_Registro;
//
//                            
//                            if(tb.LstTuplas_Tabla.size()>0){
//                                //ingresar este registro a cada una de las hash registros en las tuplas de tablota
//                                for (int k = 0; k < tb.LstTuplas_Tabla.size(); k++) {
//                                    tb.LstTuplas_Tabla.get(k).HashRegistros_Tabla.put(reg.NombreCampo_Registro, reg);
//
//                                }
//                            }else
//                            {
//                                primeraTp.HashRegistros_Tabla.put(reg.NombreCampo_Registro, reg);
//                            }                
//                        }
//                        if(tb.LstTuplas_Tabla.size()==0){
//                            arr.add(primeraTp);
//                        }                    
//                    }
//                    if(tb.LstTuplas_Tabla.size()==0){
//                        tb.LstTuplas_Tabla=arr;
//                    }
                }else
                {
                    tb = new Tabla("tablota", "",new ArrayList<Campo>(), new ArrayList<Tupla>());
                    ConstantesXML.AddError("Base Datos", "La tabla a seleccionar no pertenece a la Base de datos actual.", nombreTab.linea, nombreTab.columna);
                    break;
                }
            }
        }
        
            
        return tb;
    }

    public Tabla SeleccionarCampos(Tabla tabSelec, analizador.SimpleNode nodoLst) {
        
        Tabla nTab = new Tabla("ntab", null, new ArrayList<Campo>(), new ArrayList<Tupla>());
        for (int i = 0; i < nodoLst.children.length; i++) {
            analizador.SimpleNode IDSelect = (analizador.SimpleNode)nodoLst.children[i];
            String nombreBuscar = "";
            if(IDSelect.children.length==1)
            {
                nombreBuscar = ((analizador.SimpleNode)IDSelect.children[0]).name;
            }else
            {
                nombreBuscar = ((analizador.SimpleNode)IDSelect.children[0]).name+"."+((analizador.SimpleNode)IDSelect.children[1]).name;
            }
            
            Campo camp = null;
            
            for (int j = 0; j < tabSelec.LstCampos_Tabla.size(); j++) {
                if(((Campo)tabSelec.LstCampos_Tabla.get(j)).Nombre_Campo.equals(nombreBuscar) )
                {
                    camp =((Campo)tabSelec.LstCampos_Tabla.get(j));
                    break;
                }                
            }
            
            ArrayList<Tupla> Arr = new ArrayList<Tupla>();
            Tupla tup ;
            
            if(camp!= null){
                //agregar campo
                nTab.LstCampos_Tabla.add(new Campo(camp.Tipo_campo, camp.Nombre_Campo, camp.Propiedades));
                
                //recorer cada tupla
                for (int j = 0; j < tabSelec.LstTuplas_Tabla.size(); j++) {
                    Tupla tp = tabSelec.LstTuplas_Tabla.get(j);
                    Registro r = (Registro) tp.HashRegistros_Tabla.get(camp.Nombre_Campo);
                    
                    if(nTab.LstTuplas_Tabla.size()==0)
                    {
                        tup = new Tupla(new Hashtable());
                        tup.HashRegistros_Tabla.put(r.NombreCampo_Registro, r);
                        Arr.add(tup);
                    }else
                    {
                        nTab.LstTuplas_Tabla.get(j).HashRegistros_Tabla.put(r.NombreCampo_Registro, r);
                    }                                        
                }
                if(nTab.LstTuplas_Tabla.size()==0)
                {
                    nTab.LstTuplas_Tabla = Arr;
//                    for (int j = 0; j < Arr.size(); j++) {
//                        nTab.LstTuplas_Tabla.add(Arr.get(j));
//                    }
                }
                
            }else
            {
                ConstantesXML.AddError("Base Datos", "La tabla a seleccionar no contiene el campo "+nombreBuscar+".", 0, 0);
                break;
            }
            
            
        }
        
        return nTab;
    }
}
