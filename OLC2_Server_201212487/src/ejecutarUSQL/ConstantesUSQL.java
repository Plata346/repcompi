/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecutarUSQL;

/**
 *
 * @author Plata
 */
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Stack;
import olc2_server_201212487.Server_Interface;
import recorridoXML.*;

public class ConstantesUSQL {
    
    public static ArrayList<error> LstErrores = new ArrayList<error>();
            
    public static final int TipoNumero = 1;
    public static final int TipoDouble = 2;
    public static final int TipoCadena = 3;
    public static final int TipoFecha = 4;
    public static final int TipoFechaHora = 5;
    public static final int TipoBooleano = 6;
    public static final int TipoNull = 7;
    public static final int TipoObjeto = 8;
    
    public static  ElementoExp variableRetorno = null;
    public static String verdadero = "verdadero";
    public static String falso = "falso";
    
   public static Base_Datos BDActual;
   public static String UsuarioActual ="admin";
   public static Tabla UltimaTablaForanea;
   
   public static Hashtable TablaSimbolos = new Hashtable();
   public static String Ambito = "Global";
   public static Stack<elementoStack> PilaEjecucion = new Stack<elementoStack>();
    
   public static boolean EjecutarAdentro= false;
   
   public static boolean PaqueteLoguin= false;
   public static String RespuestaLoguin= "";
   public static String CadenaTablas= "";
   public static String CadenaMsj= "";
   public static String ultimoResultado= "";
   public static char Separador= '%';
    
    public static void AddError(String Tipo, String descripcion, int linea, int columna)
    {
        LstErrores.add(new error(Tipo,descripcion,linea,columna));
        
        Server_Interface.EscribirBitacora("Error "+Tipo, descripcion+" linea:"+linea+" columna: "+columna);
        ConstantesUSQL.CadenaMsj += "Error -> "+Tipo+" Descripcion -> "+descripcion+" Linea: "+linea+" Col: "+columna+" "+ConstantesUSQL.Separador; 
        ConstantesUSQL.ultimoResultado += "Error -> "+Tipo+" Descripcion -> "+descripcion; 
    }
}
