/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecutarUSQL;
import analizador.Analizador;
import olc2_server_201212487.*;
import analizador.SimpleNode;
import analizador.AnalizadorTreeConstants;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import interpreteXML.AnalizadorXML;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.InputStream;
import javax.swing.JOptionPane;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import recorridoXML.Base_Datos;
import recorridoXML.*;

/**
 *
 * @author Plata
 */
public class ejecucion {
    public ejecucion(){
        
    }
    
    public void ejecutar(SimpleNode nodo,String ambito){
        
        recorridoXML rXML = new recorridoXML();
        
        String  nombreNodo = AnalizadorTreeConstants.jjtNodeName[nodo.id];
        
        switch (nombreNodo){
            case "INICIO":
                ejecutar((SimpleNode)nodo.children[0], ambito);
                break;
            case "SENTENCIAS":
                for(int i = 0; i<nodo.children.length;i++)
                {
                    SimpleNode sentencia = (SimpleNode)nodo.children[i];
                    ejecutar((SimpleNode)sentencia.children[0],ambito);
                    
                    String bitacora = ObtenerTextoSentencias((SimpleNode)sentencia.children[0]);
                    System.out.println(bitacora+"\n");
                    EscribirBitacora(bitacora);
                    EscribirHistorial(Calendar.getInstance().getTime()+" - " +bitacora+" -> " +ConstantesUSQL.ultimoResultado);
                }
                break;
            case "CREAR":
                ejecutar((SimpleNode)nodo.children[0], ambito);
                break;
            
            case "CREAR_BD":
                //identificador2()
                SimpleNode NombreBD =(SimpleNode)nodo.children[0];
                rXML.AgregarBD(NombreBD.name);
                rXML.SobreEscrituraMasiva();
                break;
                
            case "CREAR_TABLA":
                //identificador2()  PARAMETROS_TABLA()
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionadu una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    Server_Interface.EscribirBitacora("Crear Tabla", "No se ha seleccionadu una base de datos");
                }else
                {
                    SimpleNode NombreTab =(SimpleNode)nodo.children[0];
                    SimpleNode Parametros =(SimpleNode)nodo.children[1];
                    rXML.AgregarTabla(NombreTab.name,ConstantesUSQL.BDActual.Nombre_BD,Parametros);
                    rXML.SobreEscrituraMasiva();
                }
                
                break;
            
            case "CREAR_OBJETO":
                //identificador2()  PARAMETROS_OBJETO()
                if(ConstantesUSQL.BDActual == null)
                {
////                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    Server_Interface.EscribirBitacora("Crear Objeto", "No se ha seleccionadu una base de datos");
                }else
                {
                    SimpleNode NombreObj =(SimpleNode)nodo.children[0];
                    SimpleNode ParametrosObj =(SimpleNode)nodo.children[1];
                    rXML.AgregarObjeto(NombreObj.name,ConstantesUSQL.BDActual.Nombre_BD,ParametrosObj);
                    rXML.SobreEscrituraMasiva();
                }
                
                break;
                
            case "CREAR_PROCEDIMINENTO":   
                if(ConstantesUSQL.EjecutarAdentro)
                {
                    ConstantesUSQL.EjecutarAdentro = false;
                    ejecutar((SimpleNode)nodo.children[1], ambito);
                    break;
                }
                else
                {
                    if(ConstantesUSQL.BDActual == null)
                    {
//                        JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                        Server_Interface.EscribirBitacora("Crear Procedimineto", "No se ha seleccionadu una base de datos");
                    }else
                    {
                        SimpleNode NombrePro =(SimpleNode)nodo.children[0];
                        SimpleNode NodoCentro =(SimpleNode)nodo.children[1];

                        String TextoSentenicas="";

                        switch (nodo.children.length)
                        {
                            case 3:
                                //identificador2()  (PARAMETROS_OBJETO())?  SENTENCIAS_INTERNAS()
                                SimpleNode NodoSentencias =(SimpleNode)nodo.children[2];

                                TextoSentenicas = ObtenerTextoSentencias(NodoSentencias);

                                rXML.AgregarProcedimiento(NombrePro.name,ConstantesUSQL.BDActual.Nombre_BD,NodoCentro,TextoSentenicas);
                                break;
                            case 2:
                                //identificador2()  SENTENCIAS_INTERNAS()

                                TextoSentenicas = ObtenerTextoSentencias(NodoCentro);

                                rXML.AgregarProcedimiento(NombrePro.name,ConstantesUSQL.BDActual.Nombre_BD,null,TextoSentenicas);
                                break;
                        }                                       
                        rXML.SobreEscrituraMasiva();
                    }
                }
                break;
                
            case "CREAR_FUNCION":
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    Server_Interface.EscribirBitacora("Crear Funcion", "No se ha seleccionadu una base de datos");
                }else
                {
                    SimpleNode NombrePro =(SimpleNode)nodo.children[0];
                    SimpleNode NodoCentro =(SimpleNode)nodo.children[1];
                    
                    String TextoSentenicas="";
                    
                    switch (nodo.children.length)
                    {
                        case 4:
                            //identificador2() (PARAMETROS_OBJETO())? TIPO_DATO() SENTENCIAS_INTERNAS()
                            SimpleNode NodoTipoDato =(SimpleNode)nodo.children[2];
                            SimpleNode NodoSentencias =(SimpleNode)nodo.children[3];
                            
                            TextoSentenicas = ObtenerTextoSentencias(NodoSentencias);
                            
                            rXML.AgregarFuncion(NombrePro.name,ConstantesUSQL.BDActual.Nombre_BD,NodoCentro,NodoTipoDato,TextoSentenicas);
                            break;
                        case 3:
                            //identificador2() TIPO_DATO() SENTENCIAS_INTERNAS() 
                            SimpleNode NodoSentencias2 =(SimpleNode)nodo.children[2];
                            
                            TextoSentenicas = ObtenerTextoSentencias(NodoSentencias2);
                            
                            rXML.AgregarFuncion(NombrePro.name,ConstantesUSQL.BDActual.Nombre_BD,null,NodoCentro,TextoSentenicas);
                            break;
                    }                                       
                    rXML.SobreEscrituraMasiva();
                }
                break;
                
            case "CREAR_USUARIO":
                
                if(ConstantesUSQL.UsuarioActual.equals("admin"))
                {
                    //identificador2() cadena() 
                    Base_Datos b = (Base_Datos) ConstantesXML.BaseDatosGeneral.HashBasesSistema.get("Administracion");
                    
                    SimpleNode usr = (SimpleNode)nodo.children[0];
                    SimpleNode pass = (SimpleNode)nodo.children[1];                    
                    
                    Hashtable hash = new Hashtable();
                    hash.put("nombre", new Registro("nombre", "\""+usr.name+"\""));
                    hash.put("clave", new Registro("clave", pass.name));
                    Tupla tp = new Tupla(hash);
                    ((Tabla)b.HashTablas.get("Usuarios")).LstTuplas_Tabla.add(tp);
                    
                    ConstantesXML.BaseDatosGeneral.HashBasesSistema.remove(b.Nombre_BD);
                    ConstantesXML.BaseDatosGeneral.HashBasesSistema.put(b.Nombre_BD,b);
                    
                    ConstantesUSQL.CadenaMsj += " Crear -> Usuario creado correctamente. "+ConstantesUSQL.Separador;
                    ConstantesUSQL.ultimoResultado = " Usuario creado correctamente. ";
                }else
                {
                    ConstantesUSQL.AddError("Semantico", "Solamente el Usuario \"admin\" puede agregar usuarios.", 0, 0);
                }
                
                rXML.SobreEscrituraMasiva();
                
                
                break;
                
            case "INSERTAR":
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    Server_Interface.EscribirBitacora("Insertar", "No se ha seleccionadu una base de datos");
                }else
                {
                    ejecutar((SimpleNode)nodo.children[0], ambito);
                    rXML.SobreEscrituraMasiva();
                }
                
                break;
                
            case "INSERTAR_TABLA":
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);                    
                    Server_Interface.EscribirBitacora("Insertar", "No se ha seleccionadu una base de datos");
                }else
                {
                    // identificador2() VALORES_PARAMETROS() 
                    rXML.InsertarTabla(((SimpleNode)nodo.children[0]).name,ambito,null,(SimpleNode)nodo.children[1]);
                }
                
                break;
            
            case "INSERTAR_TABLA_ESPECIAL":
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    Server_Interface.EscribirBitacora("Insertar", "No se ha seleccionadu una base de datos");
                }else
                {
                    // identificador2() LISTA_ID() VALORES_PARAMETROS() 
                    rXML.InsertarTabla(((SimpleNode)nodo.children[0]).name,ambito,(SimpleNode)nodo.children[1],(SimpleNode)nodo.children[2]);
                }
                
                break;
                
            case "DECLARAR":
                //DECLARAR_VARIABLE()
                //DECLARAR_INSTANCIA_OBJETO()
                ejecutar((SimpleNode)nodo.children[0], ambito);
                //rXML.SobreEscrituraMasiva();
                break;
            
            case "DECLARAR_VARIABLE":
                
                switch(nodo.children.length)
                {
                    case 3:
                        //LISTA_VAR() TIPO_DATO() VALOR()
                        SimpleNode nodotipo = (SimpleNode)nodo.children[1];
                        SimpleNode Tipo = (SimpleNode)nodotipo.children[0];
                        SimpleNode nodoValores = (SimpleNode)nodo.children[2];
                        ElementoExp val = RecorrerEXP((SimpleNode)nodoValores.children[0], ambito);
                        
                        if(val!=null)
                        {
                            if(rXML.ObtenerTipoInt(Tipo.name) == val.Tipo)
                            {
                                DeclararVariables((SimpleNode)nodo.children[0],val.Tipo,val.Valor,ambito);
                            }else
                            {
                                ConstantesUSQL.AddError("Semantico", "Declaracion de variables con distinto tipo.", Tipo.linea, Tipo.columna);
                            }
                        }else
                        {
                            ConstantesUSQL.AddError("Semantico", "El valor a asignar no es valido", Tipo.linea, Tipo.columna);
                        }
                        
                        
                        
                        break;
                    case 2:
                        //LISTA_VAR() TIPO_DATO()
                        SimpleNode tipoS = (SimpleNode)nodo.children[1];                       
                        
                        DeclararVariables((SimpleNode)nodo.children[0],rXML.ObtenerTipoInt(((SimpleNode)tipoS.children[0]).name),"$",ambito);
                        
                        break;
                }                                                
                break;
                
            case "DECLARAR_INSTANCIA_OBJETO":
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    Server_Interface.EscribirBitacora("Instanciar Objeto", "No se ha seleccionadu una base de datos");
                }else
                {
                    //identificador() identificador2() 
                    SimpleNode nombre =(SimpleNode) nodo.children[0];
                    SimpleNode Obj =(SimpleNode) nodo.children[1];
                    if(ConstantesUSQL.BDActual.HashObjetos.containsKey(Obj.name))
                    {
                        Objeto OB = (Objeto)ConstantesUSQL.BDActual.HashObjetos.get(Obj.name);
                        
                        InstanciarObjeto(nombre.name,OB,ambito);
                    }
                }
                        
                break;
            case "LLAMADA_METODOS_FUNCIONES":
                //identificador2() VALORES_PARAMETROS()
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    Server_Interface.EscribirBitacora("Llamada Procedimineto/Funcion", "No se ha seleccionadu una base de datos");
                }else
                {
                    // identificador2() LISTA_ID() VALORES_PARAMETROS() 
                    LlamadaFuncion(nodo,ambito);
                }
                
//                SimpleNode nodoNombre =(SimpleNode)nodo.children[0];
//                String nombreFuncion = nodoNombre.name;
//                //aqui hay que buscar la funcion en el archivo XML de la base de datos 
//                SimpleNode nodo_valores = (SimpleNode)nodo.children[1];
                
//                //esto es para probar la exprecion
//                ElementoExp valor_param_1 = RecorrerEXP((SimpleNode)nodo_valores.children[0],ambito);
//                if(valor_param_1 != null){
//                JOptionPane.showMessageDialog(null, "Se encontro -> " + valor_param_1.Valor +"\nTipo -> "+valor_param_1.Tipo, "EXITO", JOptionPane.INFORMATION_MESSAGE);
//                }else JOptionPane.showMessageDialog(null, "ERROES DE LA VIDA", "EXITO", JOptionPane.INFORMATION_MESSAGE);
//                    
                break;
                
            case "ACTUALIZAR":
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    Server_Interface.EscribirBitacora("Actualizar", "No se ha seleccionadu una base de datos");
                }else
                {
                    rXML.ActualizarTabla(nodo,ambito);
                }  
                rXML.SobreEscrituraMasiva();
                break;
               
            case "SELECCIONAR":
                
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);                    
                    Server_Interface.EscribirBitacora("Seleccionar", "No se ha seleccionadu una base de datos");
                }else
                {
                   Tabla tb = rXML.SeleccionarTabla(nodo,ambito);
                   if(ConstantesUSQL.PaqueteLoguin)
                   {
                       if(tb.LstTuplas_Tabla.size()==0)
                       {
                           ConstantesUSQL.RespuestaLoguin = "FAIL";
                       }else
                       {
                           ConstantesUSQL.RespuestaLoguin = "OK";
                       }
                   }else
                   {
                       //convertir tabla a HTML
                       //solo imprime para ver la tabla q va a mostrar al usuario
                            String tablaHTML = "<table class=\"table table-striped table-hover \"><tr>";
                            for (int i = 0; i < tb.LstCampos_Tabla.size(); i++) {
                                tablaHTML = tablaHTML + "<th>" + tb.LstCampos_Tabla.get(i).Nombre_Campo+"</th>";
                            }
                            tablaHTML = tablaHTML + "</tr>";
                            for (int i = 0; i < tb.LstTuplas_Tabla.size(); i++) {
                                String columnas = "";
                                //por cada tupla
                                for (int k = 0; k < tb.LstCampos_Tabla.size(); k++) {
                                    Campo cp =  (Campo)tb.LstCampos_Tabla.get(k);
                                    //por cada campo
                                    columnas = columnas + "<td>" + ((Registro)tb.LstTuplas_Tabla.get(i).HashRegistros_Tabla.get(cp.Nombre_Campo)).ValorCampo_Registro+"</td>";
                                }
                                
                                tablaHTML = tablaHTML +"<tr>"+columnas+"</tr>";
                                //System.out.println("" + registro);
                            }
                            tablaHTML = tablaHTML + "</table>";
                            
                            ConstantesUSQL.CadenaTablas += tablaHTML+ConstantesUSQL.Separador;
                   }
                    ConstantesUSQL.CadenaMsj += " Seleccionar -> Se realizo una peticion de seleccion. "+ConstantesUSQL.Separador;
                    ConstantesUSQL.ultimoResultado = " Se realizo una peticion de seleccion. ";
                }  
//                rXML.SobreEscrituraMasiva();
                break;
            case "USAR_BD":  
                //se carga toda las las bases de datos en una estructura
                
                rXML.CargarBD_Completa();
                //VerificarExistenciaBD(nodoNombreBD.name);
                // se selecciona la base de datos a utilizar
                SimpleNode nodoNombreBD =(SimpleNode)nodo.children[0];
                ConstantesUSQL.BDActual = UsarBD(nodoNombreBD.name);
                
                if(ConstantesUSQL.BDActual == null)
                {
//                    JOptionPane.showMessageDialog(null, "No se encontro la base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                    ConstantesUSQL.AddError("Semantico", "No se encontro la base de datos "+nodoNombreBD.name, nodoNombreBD.linea, nodoNombreBD.columna);                    
                }else
                {
                    ConstantesUSQL.CadenaMsj += " Usar -> Base de datos seleccionada. "+ConstantesUSQL.Separador;
                    ConstantesUSQL.ultimoResultado = " Base de datos seleccionada. ";
                }
                break;
                
            case "SENTENCIAS_INTERNAS":
                for(int i = 0; i<nodo.children.length;i++)
                {
                    SimpleNode sentencia = (SimpleNode)nodo.children[i];
                    ejecutar((SimpleNode)sentencia.children[0],ambito);  
                    
                    String bitacora = ObtenerTextoSentencias((SimpleNode)sentencia.children[0]);
                    EscribirBitacora(bitacora);
                    EscribirHistorial(Calendar.getInstance().getTime()+" - " +bitacora+" -> " +ConstantesUSQL.ultimoResultado);
                }
                break;
                
            case "ASIGNAR":
                if(nodo.children.length == 2)
                {
                    //identificador() VALOR() 
                    SimpleNode var = (SimpleNode)nodo.children[0];
                    ElementoExp valor = RecorrerEXP((SimpleNode)nodo.children[1], ambito);
                    if(valor != null)
                    {
                        AsignarValorVariable(var.name,valor.Valor,valor.Tipo,ambito);
                    }else
                    {
                        ConstantesUSQL.AddError("Semantico", "El valor a asignar no es valido para "+var.name, var.linea, var.columna);                    
                    }
                        
                    
                }else
                {                                        
                    if(ConstantesUSQL.BDActual == null)
                    {
    //                    JOptionPane.showMessageDialog(null, "No se ha seleccionado una base de datos", "ERROR", JOptionPane.ERROR_MESSAGE);
                        Server_Interface.EscribirBitacora("Instanciar Objeto", "No se ha seleccionadu una base de datos");
                    }else
                    {
                        //identificador() (<TK_PUNTO> identificador2())? VALOR() 
                        SimpleNode InstObj = (SimpleNode)nodo.children[0];
                        SimpleNode atr = (SimpleNode)nodo.children[1];
                        ElementoExp valor = RecorrerEXP((SimpleNode)nodo.children[2], ambito);
                        
                        if(valor != null)
                        {                            
                            AsignarValorObjeto(InstObj.name,atr.name,valor.Valor,valor.Tipo,ambito);                          
                            
                        }else
                        {
                            ConstantesUSQL.AddError("Semantico", "El valor a asignar no es valido para el atributo "+atr.name, atr.linea, atr.columna);
                        }

                    }                                                            
                }
                
                break;
                
            case "SI":
                recorrerIf(nodo,ambito);
                break;
            
            case"MIENTRAS":
                RecorrerMientras(nodo,ambito);
                break;
            
            case"SELEC":
                RecorrerSelecciona(nodo,ambito);
                break;
            
            case"PARA":
                RecorrerPara(nodo,ambito);
                break;
                
            case "RETORNO":
                //VALOR()
                SimpleNode retorno = (SimpleNode) nodo.children[0];
                
                ConstantesUSQL.variableRetorno = RecorrerEXP((SimpleNode)retorno, ambito);
                break;
                
            case"IMPRIMIR":
                // VALOR()
                ElementoExp ele = RecorrerEXP((SimpleNode)nodo.children[0], ambito);
                if(ele.Tipo == ConstantesUSQL.TipoCadena)
                {
                    //mandar paquete salida
                    
                    ConstantesUSQL.CadenaMsj += ele.Valor +ConstantesUSQL.Separador;                
                    ConstantesUSQL.CadenaMsj += " Imprimir -> Ejecucion imprimir. "+ConstantesUSQL.Separador;                
                    ConstantesUSQL.ultimoResultado = "jecucion imprimir. ";
                    
                }else
                {
                    ConstantesUSQL.AddError("Semantico", "El valor en IMPRIMIR no es valido.", ((SimpleNode)nodo.children[0]).linea, ((SimpleNode)nodo.children[0]).columna);
                }
                
                
                break;
        }
//        SimpleNode E = (SimpleNode)nodo.children[0].jjtGetChild(0);
//        int resultado = E(E);
//        System.out.println((String.valueOf(resultado)));
    }
    
//region Recorrer EXP  
    public ElementoExp RecorrerEXP(SimpleNode exp, String ambito)
    {        
        try {
            ElementoExp Izquierda, Derecha, Resultado = null;
            
            String nombreExp = AnalizadorTreeConstants.jjtNodeName[exp.id];
            switch (nombreExp)
            {
                case "VALOR":
                    Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                    break;
                case "BOOLEANO":
                    
                    switch (exp.children.length)
                    {
                        case 3:
                        //J() (tk_or() BOOLEANO())?
                            Izquierda = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            Derecha  = RecorrerEXP((SimpleNode)exp.children[2],ambito);
                            
                            SimpleNode term = (SimpleNode)exp.children[1];                            
//                            
                            if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                            {
                                if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) || (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                {
                                    Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                    return Resultado;
                                }else
                                {
                                    Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                    return Resultado;
                                }
                            }else
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Expresion logica Invalida para \"OR\" Solo puedes operar entre boleanos.", term.linea, term.columna);
                                break;                                       
                            }
                        case 1:
                        //J()
                            Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            break;
                    }
                    break;
                case "J":
                    switch (exp.children.length)
                    {
                        case 3:
                        //K() (tk_and() J())?
                            Izquierda = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            Derecha  = RecorrerEXP((SimpleNode)exp.children[2],ambito);
                            
                            SimpleNode term = (SimpleNode)exp.children[1];                            
                            
                            if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                            {
                                if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) && (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                {
                                    Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                    return Resultado;
                                }else
                                {
                                    Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                    return Resultado;
                                }
                            }else
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Expresion logica Invalida para \"AND\" Solo puedes operar entre boleanos.", term.linea, term.columna);
                                break;                                       
                            }
                            
                        case 1:
                        //K()
                            Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            break;
                    }
                    break;
                case "K":
                    switch (exp.children.length)
                    {
                        case 2:
                        //tk_not() BOOLEANO()
                            Izquierda = RecorrerEXP((SimpleNode)exp.children[1],ambito);
                            
                            SimpleNode term = (SimpleNode)exp.children[0];                            
                            
                            if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano)
                            {
                                if(Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero))
                                {
                                    Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                    return Resultado;
                                }else
                                {
                                    Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                    return Resultado;
                                }
                            }else
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Expresion logica Invalida para \"NOT\" Solo puedes negar un boleano.", term.linea, term.columna);
                                break;                                       
                            }
                            
                        case 1:
                        //RELACIONAL()
                            Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            break;
                    }
                    break;
                case "RELACIONAL":
                    
                    switch (exp.children.length)
                    {
                        case 3:
                        //ARIT() ((tk_igual_igual()|tk_no_igual()|tk_mayor_igual()|tk_menor_igual()|tk_mayor()|tk_menor()) ARIT())?
                            Izquierda = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            SimpleNode operador = (SimpleNode) exp.children[1];
                            //signo
                            String operacion = operador.name;
                            //nombre establecido en la gramatica
                            String op = AnalizadorTreeConstants.jjtNodeName[operador.id];
                            Derecha  = RecorrerEXP((SimpleNode)exp.children[2],ambito);
                            
                            SimpleNode term = (SimpleNode)exp.children[1];    
                            
                            if(Izquierda.Tipo == Derecha.Tipo)
                            {
                                
                                switch (operacion)
                                {
                                    //casos de operaciones < > == .....
                                    case "==":
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano){
                                            if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) && (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else if((Izquierda.Valor.equals("0") || Izquierda.Valor.equals(ConstantesUSQL.falso)) && (Derecha.Valor.equals("0") || Derecha.Valor.equals(ConstantesUSQL.falso)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else
                                        {
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoBooleano)
                                            {
                                                Izquierda.Valor = String.valueOf(Double.parseDouble(Izquierda.Valor));
                                                Derecha.Valor = String.valueOf(Double.parseDouble(Derecha.Valor));
                                            }
                                            
                                            if(Izquierda.Valor.equals(Derecha.Valor))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        } 
                                    case "!=":
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano){
                                            if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) && (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else if((Izquierda.Valor.equals("0") || Izquierda.Valor.equals(ConstantesUSQL.falso)) && (Derecha.Valor.equals("0") || Derecha.Valor.equals(ConstantesUSQL.falso)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else
                                        {
                                            if(Izquierda.Valor.equals(Derecha.Valor))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }                                       
                                    case "<":
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano){
                                            if((Izquierda.Valor.equals("0") || Izquierda.Valor.equals(ConstantesUSQL.falso)) && (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena)
                                        {
                                            if(Izquierda.Valor.length() < Derecha.Valor.length())
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                        {
                                            if(Double.parseDouble(Izquierda.Valor) < Double.parseDouble(Derecha.Valor))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)
                                        {
                                            if(Date.parse(Izquierda.Valor.replace("-", "/")) < Date.parse(Derecha.Valor.replace("-", "/")))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else
                                        {
                                            Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                        }
                                    case ">":
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano){
                                            if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) && (Derecha.Valor.equals("0") || Derecha.Valor.equals(ConstantesUSQL.falso)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena)
                                        {
                                            if(Izquierda.Valor.length() > Derecha.Valor.length())
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                        {
                                            if(Double.parseDouble(Izquierda.Valor) > Double.parseDouble(Derecha.Valor))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)
                                        {
                                            if(Date.parse(Izquierda.Valor.replace("-", "/")) > Date.parse(Derecha.Valor.replace("-", "/")))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else
                                        {
                                            Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                        } 
                                    case "<=":
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano){
                                            if((Izquierda.Valor.equals("0") || Izquierda.Valor.equals(ConstantesUSQL.falso)) && (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) && (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else if((Izquierda.Valor.equals("0") || Izquierda.Valor.equals(ConstantesUSQL.falso)) && (Derecha.Valor.equals("0") || Derecha.Valor.equals(ConstantesUSQL.falso)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena)
                                        {
                                            if(Izquierda.Valor.length() <= Derecha.Valor.length())
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                        {
                                            if(Double.parseDouble(Izquierda.Valor) <= Double.parseDouble(Derecha.Valor))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)
                                        {
                                            if(Date.parse(Izquierda.Valor.replace("-", "/")) <= Date.parse(Derecha.Valor.replace("-", "/")))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else
                                        {
                                            Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                        }
                                    case ">=":
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano){
                                            if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) && (Derecha.Valor.equals("0") || Derecha.Valor.equals(ConstantesUSQL.falso)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) && (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else if((Izquierda.Valor.equals("0") || Izquierda.Valor.equals(ConstantesUSQL.falso)) && (Derecha.Valor.equals("0") || Derecha.Valor.equals(ConstantesUSQL.falso)))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena)
                                        {
                                            if(Izquierda.Valor.length() >= Derecha.Valor.length())
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else{
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                        {
                                            if(Double.parseDouble(Izquierda.Valor) >= Double.parseDouble(Derecha.Valor))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else if(Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)
                                        {
                                            if(Date.parse(Izquierda.Valor.replace("-", "/")) >= Date.parse(Derecha.Valor.replace("-", "/")))
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }else
                                            {
                                                Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                            }
                                        }else
                                        {
                                            Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                                return Resultado;
                                        }
                                }
                            }else
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Relacional Invalida para \""+operacion+"\" No puedes comparar dos tipos distintos.", term.linea, term.columna);
                                break;                                       
                            }                                                      
                            break;
                        case 1:
                        //ARIT()
                            Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            break;
                    }
                    break;
                case "ARIT":                    
                    switch (exp.children.length)
                    {
                        case 3:
                        //T() ((mas()|menos()) ARIT())?
                            Izquierda = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            
                            SimpleNode operador = (SimpleNode) exp.children[1];
                            //signo
                            String operacion = operador.name;
                            //nombre establecido en la gramatica
                            String op = AnalizadorTreeConstants.jjtNodeName[operador.id];
                            
                            Derecha  = RecorrerEXP((SimpleNode)exp.children[2],ambito);
                            
                            SimpleNode term = (SimpleNode)exp.children[1];                            
                            
                            switch (operacion)
                            {
                                //casos de operaciones + o -
                                case "+":
                                    if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                                    {
                                        if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) || (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                        {
                                            Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                            return Resultado;
                                        }else{
                                            Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                            return Resultado;
                                        }
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoCadena )
                                    {
                                        String izq,der="";
                                        izq = Izquierda.Valor.replace("\"", "");
                                        der = Derecha.Valor.replace("\"", "");
                                        Resultado = new ElementoExp(izq+der, ConstantesUSQL.TipoCadena);
                                        return Resultado;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && (Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"+\" No puedes sumar Boleano y fecha o FechaHora", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && (Derecha.Tipo == ConstantesUSQL.TipoNumero || Derecha.Tipo == ConstantesUSQL.TipoDouble))
                                    {
                                        if(Izquierda.Valor.equals("1")){                                            
                                            double izq, der, res;
                                            izq = 1;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq + der;
                                            
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals("0")){

                                            double izq, der, res;
                                            izq = 0;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq + der;
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals(ConstantesUSQL.verdadero)){

                                            double izq, der, res;
                                            izq = 1;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq + der;
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals(ConstantesUSQL.falso)){

                                            double izq, der, res;
                                            izq = 0;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq + der;
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else{
                                            break;
                                        } 
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                                    {
                                        if(Derecha.Valor.equals("1")){                                            
                                            double izq, der, res;
                                            der = 1;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq + der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Derecha.Valor.equals("0")){
                                            
                                            double izq, der, res;
                                            der = 0;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq + der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;
                                            
                                        }else if(Derecha.Valor.equals(ConstantesUSQL.verdadero)){

                                            double izq, der, res;
                                            der = 1;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq + der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Derecha.Valor.equals(ConstantesUSQL.falso)){

                                            double izq, der, res;
                                            der = 0;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq + der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else{
                                            break;
                                        } 
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoNumero|| Derecha.Tipo == ConstantesUSQL.TipoDouble))
                                    {                                                                                   
                                        double izq, der, res;
                                        izq = Double.parseDouble(Izquierda.Valor);
                                        der = Double.parseDouble(Derecha.Valor);
                                        res = izq + der;
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoDouble || Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                        {
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                        }else
                                        {
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                        }
                                        
                                        return Resultado;
                                        
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoFecha|| Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                                    {                                                                                   
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"+\" No puedes sumar con fecha o fechaHora.", term.linea, term.columna);
                                        break;                                       
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && Derecha.Tipo == ConstantesUSQL.TipoCadena)
                                    {                                                                                   
                                        String izq,der="";
                                        izq = Izquierda.Valor.replace("\"", "");
                                        der = Derecha.Valor.replace("\"", "");
                                        Resultado = new ElementoExp(izq+der, ConstantesUSQL.TipoCadena);
                                        return Resultado;                                      
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena && (Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora) )
                                    {                                                                                   
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"+\" No puedes sumar una cadena con una fecha o FechaHora.", term.linea, term.columna);
                                        break;                                       
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena && (Derecha.Tipo != ConstantesUSQL.TipoFecha || Derecha.Tipo != ConstantesUSQL.TipoFechaHora) )
                                    {                                                                                   
                                        String izq,der="";
                                        izq = Izquierda.Valor.replace("\"", "");
                                        der = Derecha.Valor.replace("\"", "");
                                        Resultado = new ElementoExp(izq+der, ConstantesUSQL.TipoCadena);
                                        return Resultado;                                     
                                    }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)  && (Derecha.Tipo == ConstantesUSQL.TipoBooleano || Derecha.Tipo == ConstantesUSQL.TipoNumero ||Derecha.Tipo == ConstantesUSQL.TipoDouble ||Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"+\" No puedes sumar fecha o fechaHora con ningun tipo de dato que no sea cadena.", term.linea, term.columna);
                                        break;

                                    }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora) && Derecha.Tipo == ConstantesUSQL.TipoCadena)
                                    {
                                        String izq,der="";
                                        izq = Izquierda.Valor.replace("\"", "");
                                        der = Derecha.Valor.replace("\"", "");
                                        Resultado = new ElementoExp(izq+der, ConstantesUSQL.TipoCadena);
                                        return Resultado;

                                    }else ConstantesUSQL.AddError("Semantico", "ERROR DE LA VIDA CASO NO TOMADO EN CUENTA EN SUMA.", term.linea, term.columna);
                                        break;
                                case "-":
                                    if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" No puedes restar Boleano y Boleano", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoCadena )
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" No puedes restar Boleano y cadena", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && (Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" No puedes restar Boleano y fecha o FechaHora", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && (Derecha.Tipo == ConstantesUSQL.TipoNumero || Derecha.Tipo == ConstantesUSQL.TipoDouble))
                                    {
                                        if(Izquierda.Valor.equals("1")){                                            
                                            double izq, der, res;
                                            izq = 1;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq - der;
                                            
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals("0")){

                                            double izq, der, res;
                                            izq = 0;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq - der;
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals(ConstantesUSQL.verdadero)){

                                            double izq, der, res;
                                            izq = 1;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq - der;
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals(ConstantesUSQL.falso)){

                                            double izq, der, res;
                                            izq = 0;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq - der;
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else{
                                            break;
                                        } 
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                                    {
                                        if(Derecha.Valor.equals("1")){                                            
                                            double izq, der, res;
                                            der = 1;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq - der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Derecha.Valor.equals("0")){
                                            
                                            double izq, der, res;
                                            der = 0;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq - der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;
                                            
                                        }else if(Derecha.Valor.equals(ConstantesUSQL.verdadero)){

                                            double izq, der, res;
                                            der = 1;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq - der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Derecha.Valor.equals(ConstantesUSQL.falso)){

                                            double izq, der, res;
                                            der = 0;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq - der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else{
                                            break;
                                        } 
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoNumero|| Derecha.Tipo == ConstantesUSQL.TipoDouble))
                                    {                                                                                   
                                        double izq, der, res;
                                        izq = Double.parseDouble(Izquierda.Valor);
                                        der = Double.parseDouble(Derecha.Valor);
                                        res = izq - der;
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoDouble || Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                        {
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                        }else
                                        {
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                        }
                                        
                                        return Resultado;
                                        
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoFecha|| Derecha.Tipo == ConstantesUSQL.TipoFechaHora ||  Derecha.Tipo == ConstantesUSQL.TipoCadena))
                                    {                                                                                   
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" No puedes restar con fecha, fechaHora o cadena.", term.linea, term.columna);
                                        break;                                       
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena)
                                    {                                                                                   
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" No puedes restar una cadena.", term.linea, term.columna);
                                        break;                                       
                                    }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)  && (Derecha.Tipo == ConstantesUSQL.TipoBooleano || Derecha.Tipo == ConstantesUSQL.TipoNumero ||Derecha.Tipo == ConstantesUSQL.TipoDouble ||Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora || Derecha.Tipo == ConstantesUSQL.TipoCadena))
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" No puedes restar fecha o fechaHora con ningun tipo de dato.", term.linea, term.columna);
                                        break;

                                    }else ConstantesUSQL.AddError("Semantico", "ERROR DE LA VIDA CASO NO TOMADO EN CUENTA EN RESTA.", term.linea, term.columna);
                                        break;
                            }
                            
                            break;
                        case 1:
                        //T()
                            Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            break;
                    }
                    break;
                case "T":                    
                    switch (exp.children.length)
                    {
                        case 3:
                        //F() ((por()|div())  T())?
                            Izquierda = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            
                            SimpleNode operador = (SimpleNode) exp.children[1];
                            //signo
                            String operacion = operador.name;
                            //nombre establecido en la gramatica
                            String op = AnalizadorTreeConstants.jjtNodeName[operador.id];
                            
                            Derecha  = RecorrerEXP((SimpleNode)exp.children[2],ambito);
                            
                            SimpleNode term = (SimpleNode)exp.children[1];                            
                            
                            switch (operacion)
                            {
                                //casos de operaciones * o /
                                case "*":
                                    if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                                    {
                                        if((Izquierda.Valor.equals("1") || Izquierda.Valor.equals(ConstantesUSQL.verdadero)) && (Derecha.Valor.equals("1") || Derecha.Valor.equals(ConstantesUSQL.verdadero)))
                                        {
                                            Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);
                                            return Resultado;
                                        }else{
                                            Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);
                                            return Resultado;
                                        }
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoCadena )
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"*\" No puedes multiplicar Boleano por cadena", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && (Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"*\" No puedes multiplicar Boleano por fecha o FechaHora", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && (Derecha.Tipo == ConstantesUSQL.TipoNumero || Derecha.Tipo == ConstantesUSQL.TipoDouble))
                                    {
                                        if(Izquierda.Valor.equals("1")){                                            
                                            double izq, der, res;
                                            izq = 1;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq * der;
                                            
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals("0")){

                                            double izq, der, res;
                                            izq = 0;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq * der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals(ConstantesUSQL.verdadero)){

                                            double izq, der, res;
                                            izq = 1;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq * der;
                                            if(Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals(ConstantesUSQL.falso)){

                                            double izq, der, res;
                                            izq = 0;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq * der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            return Resultado;

                                        }else{
                                            break;
                                        } 
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                                    {
                                        if(Derecha.Valor.equals("1")){                                            
                                            double izq, der, res;
                                            der = 1;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq * der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Derecha.Valor.equals("0")){
                                            
                                            double izq, der, res;
                                            der = 0;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq * der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            return Resultado;
                                            
                                        }else if(Derecha.Valor.equals(ConstantesUSQL.verdadero)){

                                            double izq, der, res;
                                            der = 1;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq * der;
                                            if(Izquierda.Tipo == ConstantesUSQL.TipoDouble)
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            }else
                                            {
                                                Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            }
                                            return Resultado;

                                        }else if(Derecha.Valor.equals(ConstantesUSQL.falso)){

                                            double izq, der, res;
                                            der = 0;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq * der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                            return Resultado;

                                        }else{
                                            break;
                                        } 
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoNumero|| Derecha.Tipo == ConstantesUSQL.TipoDouble))
                                    {                                                                                   
                                        double izq, der, res;
                                        izq = Double.parseDouble(Izquierda.Valor);
                                        der = Double.parseDouble(Derecha.Valor);
                                        res = izq * der;
                                        if(Izquierda.Tipo == ConstantesUSQL.TipoDouble || Derecha.Tipo == ConstantesUSQL.TipoDouble)
                                        {
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                        }else
                                        {
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoNumero);
                                        }
                                        
                                        return Resultado;
                                        
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoFecha|| Derecha.Tipo == ConstantesUSQL.TipoFechaHora ||  Derecha.Tipo == ConstantesUSQL.TipoCadena))
                                    {                                                                                   
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"*\" No puedes multiplicar entre fecha, fechaHora o cadena.", term.linea, term.columna);
                                        break;                                       
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena)
                                    {                                                                                   
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"*\" No puedes multiplicar una cadena.", term.linea, term.columna);
                                        break;                                       
                                    }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora) && Derecha.Tipo == ConstantesUSQL.TipoCadena)
                                    {
                                        String izq,der="";
                                        izq = Izquierda.Valor.replace("\"", "");
                                        der = Derecha.Valor.replace("\"", "");
                                        Resultado = new ElementoExp(izq+der, ConstantesUSQL.TipoCadena);
                                        return Resultado;

                                    }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)  && (Derecha.Tipo == ConstantesUSQL.TipoBooleano || Derecha.Tipo == ConstantesUSQL.TipoNumero ||Derecha.Tipo == ConstantesUSQL.TipoDouble ||Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"*\" No puedes multiplicar fecha o fechaHora con ningun tipo de dato que no sea cadena ", term.linea, term.columna);
                                        break;

                                    }else ConstantesUSQL.AddError("Semantico", "ERROR DE LA VIDA CASO NO TOMADO EN CUENTA EN MULTIPLICACION.", term.linea, term.columna);
                                        break;
                                case "/":
                                    if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"÷\" No puedes dividr dos Boleanos", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoCadena )
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"÷\" No puedes dividr Boleano entre cadena", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && (Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"÷\" No puedes dividr Boleano entre fecha o FechaHora", term.linea, term.columna);
                                        break;
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && (Derecha.Tipo == ConstantesUSQL.TipoNumero || Derecha.Tipo == ConstantesUSQL.TipoDouble))
                                    {
                                        if(Izquierda.Valor.equals("1")){                                            
                                            double izq, der, res;
                                            izq = 1;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq / der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals("0")){

                                            double izq, der, res;
                                            izq = 0;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq / der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals(ConstantesUSQL.verdadero)){

                                            double izq, der, res;
                                            izq = 1;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq / der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            return Resultado;

                                        }else if(Izquierda.Valor.equals(ConstantesUSQL.falso)){

                                            double izq, der, res;
                                            izq = 0;
                                            der = Double.parseDouble(Derecha.Valor);
                                            res = izq / der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            return Resultado;

                                        }else{
                                            break;
                                        } 
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                                    {
                                        if(Derecha.Valor.equals("1")){                                            
                                            double izq, der, res;
                                            der = 1;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq / der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            return Resultado;

                                        }else if(Derecha.Valor.equals("0")){
                                            //ERROR
                                            ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"÷\" No puedes dividr entre bool 0", term.linea, term.columna);
                                            break;
                                            
                                        }else if(Derecha.Valor.equals(ConstantesUSQL.verdadero)){

                                            double izq, der, res;
                                            der = 1;
                                            izq = Double.parseDouble(Izquierda.Valor);
                                            res = izq / der;
                                            Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                            return Resultado;

                                        }else if(Derecha.Valor.equals(ConstantesUSQL.falso)){

                                            //ERROR
                                            ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"÷\" No puedes dividr entre bool falso", term.linea, term.columna);
                                            break;

                                        }else{
                                            break;
                                        } 
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoNumero|| Derecha.Tipo == ConstantesUSQL.TipoDouble))
                                    {                                                                                   
                                        double izq, der, res;
                                        izq = Double.parseDouble(Izquierda.Valor);
                                        der = Double.parseDouble(Derecha.Valor);
                                        res = izq / der;
                                        Resultado = new ElementoExp(String.valueOf(res), ConstantesUSQL.TipoDouble);
                                        return Resultado;
                                        
                                    }else if((Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoFecha|| Derecha.Tipo == ConstantesUSQL.TipoFechaHora ||  Derecha.Tipo == ConstantesUSQL.TipoCadena))
                                    {                                                                                   
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"÷\" No puedes dividr entre fecha, fechaHora o cadena.", term.linea, term.columna);
                                        break;                                       
                                    }else if(Izquierda.Tipo == ConstantesUSQL.TipoCadena)
                                    {                                                                                   
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"÷\" No puedes dividr una cadena.", term.linea, term.columna);
                                        break;                                       
                                    }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora) && Derecha.Tipo == ConstantesUSQL.TipoCadena)
                                    {
                                        String izq,der="";
                                        izq = Izquierda.Valor.replace("\"", "");
                                        der = Derecha.Valor.replace("\"", "");
                                        Resultado = new ElementoExp(izq+der, ConstantesUSQL.TipoCadena);
                                        return Resultado;

                                    }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)  && (Derecha.Tipo == ConstantesUSQL.TipoBooleano || Derecha.Tipo == ConstantesUSQL.TipoNumero ||Derecha.Tipo == ConstantesUSQL.TipoDouble ||Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                                    {
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"÷\" No puedes dividir fecha o fechaHora con ningun tipo de dato que no sea cadena ", term.linea, term.columna);
                                        break;

                                    }else ConstantesUSQL.AddError("Semantico", "ERROR DE LA VIDA CASO NO TOMADO EN CUENTA EN DIVISION.", term.linea, term.columna);
                                        break;
                                }
                            
                            break;
                        case 1:
                        //F()
                            Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            break;
                    }
                    break;
                case "F":
                    switch (exp.children.length)
                    {
                        case 3:
                        //G() (potencia() F())?
                            Izquierda = RecorrerEXP((SimpleNode)exp.children[0],ambito);                                                        
                            //^
                            Derecha  = RecorrerEXP((SimpleNode)exp.children[2],ambito);
                            
                            SimpleNode term = (SimpleNode)exp.children[1];                            
                            
                            if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"^\" No puedes elevar dos Boleanos", term.linea, term.columna);
                                break;
                            }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoNumero)
                            {
                                if(Izquierda.Valor.equals("1")){                                            
                                                                                
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(1, Integer.parseInt(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                    
                                }else if(Izquierda.Valor.equals("0")){
                                        
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(0, Integer.parseInt(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                        
                                }else if(Izquierda.Valor.equals(ConstantesUSQL.verdadero)){
                                    
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(1, Integer.parseInt(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                    
                                }else if(Izquierda.Valor.equals(ConstantesUSQL.falso)){
                                                                                    
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(0, Integer.parseInt(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                     
                                }else{
                                    break;
                                }                               
                            }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoDouble)
                            {
                                if(Izquierda.Valor.equals("1")){                                            
                                                                                
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(1, Double.parseDouble(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                    
                                }else if(Izquierda.Valor.equals("0")){
                                        
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(0, Double.parseDouble(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                        
                                }else if(Izquierda.Valor.equals(ConstantesUSQL.verdadero)){
                                    
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(1, Double.parseDouble(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                    
                                }else if(Izquierda.Valor.equals(ConstantesUSQL.falso)){
                                                                                    
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(0, Double.parseDouble(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                     
                                }else{
                                    break;
                                }                               
                            }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoCadena)
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"^\" No puedes elevar boleano y cadena", term.linea, term.columna);
                                break;
                            }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoFecha)
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"^\" No puedes elevar boleano y fecha", term.linea, term.columna);
                                break;
                            }else if(Izquierda.Tipo == ConstantesUSQL.TipoBooleano && Derecha.Tipo == ConstantesUSQL.TipoFechaHora)
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"^\" No puedes elevar boleano y fechaHora", term.linea, term.columna);
                                break;
                            }else if(Izquierda.Tipo == ConstantesUSQL.TipoNumero && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                            {
                                if(Derecha.Valor.equals("1")){                                            
                                                                                
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(Double.parseDouble(Izquierda.Valor) , 1)), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                    
                                }else if(Derecha.Valor.equals("0")){
                                        
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(Double.parseDouble(Izquierda.Valor) , 0)), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                        
                                }else if(Derecha.Valor.equals(ConstantesUSQL.verdadero)){
                                    
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(Double.parseDouble(Izquierda.Valor) , 1)), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                    
                                }else if(Derecha.Valor.equals(ConstantesUSQL.falso)){
                                                                                    
                                    Resultado = new ElementoExp(String.valueOf((int) Math.pow(Double.parseDouble(Izquierda.Valor) , 0)), ConstantesUSQL.TipoNumero);
                                    return Resultado;
                                     
                                }else{
                                    break;
                                }
                                
                            }else if( Izquierda.Tipo == ConstantesUSQL.TipoDouble && Derecha.Tipo == ConstantesUSQL.TipoBooleano)
                            {
                                if(Derecha.Valor.equals("1")){                                            
                                                                                
                                    Resultado = new ElementoExp(String.valueOf((double) Math.pow(Double.parseDouble(Izquierda.Valor) , 1)), ConstantesUSQL.TipoDouble);
                                    return Resultado;
                                    
                                }else if(Derecha.Valor.equals("0")){
                                        
                                    Resultado = new ElementoExp(String.valueOf((double) Math.pow(Double.parseDouble(Izquierda.Valor) , 0)), ConstantesUSQL.TipoDouble);
                                    return Resultado;
                                        
                                }else if(Derecha.Valor.equals(ConstantesUSQL.verdadero)){
                                    
                                    Resultado = new ElementoExp(String.valueOf((double) Math.pow(Double.parseDouble(Izquierda.Valor) , 1)), ConstantesUSQL.TipoDouble);
                                    return Resultado;
                                    
                                }else if(Derecha.Valor.equals(ConstantesUSQL.falso)){
                                                                                    
                                    Resultado = new ElementoExp(String.valueOf((double) Math.pow(Double.parseDouble(Izquierda.Valor) , 0)), ConstantesUSQL.TipoDouble);
                                    return Resultado;
                                     
                                }else{
                                    break;
                                }
                                
                            }else if( Izquierda.Tipo == ConstantesUSQL.TipoNumero && Derecha.Tipo == ConstantesUSQL.TipoNumero)
                            {
                                Resultado = new ElementoExp(String.valueOf((int) Math.pow(Double.parseDouble(Izquierda.Valor) , Double.parseDouble(Derecha.Valor))), ConstantesUSQL.TipoNumero);
                                return Resultado;
                                
                            }else if( Izquierda.Tipo == ConstantesUSQL.TipoNumero && Derecha.Tipo == ConstantesUSQL.TipoDouble)
                            {
                                Resultado = new ElementoExp(String.valueOf((double) Math.pow(Double.parseDouble(Izquierda.Valor) , Double.parseDouble(Derecha.Valor))), ConstantesUSQL.TipoDouble);
                                return Resultado;
                                
                            }else if( Izquierda.Tipo == ConstantesUSQL.TipoDouble && Derecha.Tipo == ConstantesUSQL.TipoDouble)
                            {
                                Resultado = new ElementoExp(String.valueOf((double) Math.pow(Double.parseDouble(Izquierda.Valor) , Double.parseDouble(Derecha.Valor))), ConstantesUSQL.TipoDouble);
                                return Resultado;
                                
                            }else if( Izquierda.Tipo == ConstantesUSQL.TipoDouble && Derecha.Tipo == ConstantesUSQL.TipoNumero)
                            {
                                Resultado = new ElementoExp(String.valueOf((double) Math.pow(Double.parseDouble(Izquierda.Valor) , Double.parseDouble(Derecha.Valor))), ConstantesUSQL.TipoDouble);
                                return Resultado;
                                
                            }else if( (Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && Derecha.Tipo == ConstantesUSQL.TipoCadena)
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"^\" No puedes elevar numero y cadena", term.linea, term.columna);
                                break;
                                
                            }else if( (Izquierda.Tipo == ConstantesUSQL.TipoNumero || Izquierda.Tipo == ConstantesUSQL.TipoDouble) && (Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"^\" No puedes elevar numero y fecha o fechaHora", term.linea, term.columna);
                                break;
                                
                            }else if( Izquierda.Tipo == ConstantesUSQL.TipoCadena  && (Derecha.Tipo == ConstantesUSQL.TipoBooleano ||Derecha.Tipo == ConstantesUSQL.TipoNumero ||Derecha.Tipo == ConstantesUSQL.TipoDouble ||Derecha.Tipo == ConstantesUSQL.TipoCadena ||Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"^\" No puedes elevar cadena a ningun tipo de dato", term.linea, term.columna);
                                break;
                                
                            }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora)  && (Derecha.Tipo == ConstantesUSQL.TipoBooleano || Derecha.Tipo == ConstantesUSQL.TipoNumero ||Derecha.Tipo == ConstantesUSQL.TipoDouble ||Derecha.Tipo == ConstantesUSQL.TipoFecha || Derecha.Tipo == ConstantesUSQL.TipoFechaHora))
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"^\" No puedes elevar fecha o fechaHora a ningun tipo de dato que no sea cadena", term.linea, term.columna);
                                break;
                                
                            }else if( (Izquierda.Tipo == ConstantesUSQL.TipoFecha || Izquierda.Tipo == ConstantesUSQL.TipoFechaHora) && Derecha.Tipo == ConstantesUSQL.TipoCadena)
                            {
                                String izq,der="";
                                izq = Izquierda.Valor.replace("\"", "");
                                der = Derecha.Valor.replace("\"", "");
                                Resultado = new ElementoExp(izq+der, ConstantesUSQL.TipoCadena);
                                return Resultado;
                                
                            }else ConstantesUSQL.AddError("Semantico", "ERROR DE LA VIDA CASO NO TOMADO EN CUENTA EN POTENCIA.", term.linea, term.columna);
                                break;
                        case 1:
                        //G()
                            Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                            break;
                    }
                    break;
                case "G":
                    switch (exp.children.length)
                    {
                        case 2:
                        SimpleNode NodoIzquierda = (SimpleNode) exp.children[0];
                        SimpleNode NodoDrecha = (SimpleNode) exp.children[1];
                        String caso = AnalizadorTreeConstants.jjtNodeName[NodoIzquierda.id];
                        switch(caso)
                        {
                            case "identificador":
                                //| identificador() (<TK_PUNTO> identificador2())?
                                //VERIFICAR CUAL DE LOS DOS CASOS SERIA EL DE OBJETO.VARIABLE
                                if(ConstantesXML.EsCondicion)
                                {
                                    return new ElementoExp(NodoIzquierda.name+"."+NodoDrecha.name, 0);
                                }else
                                {
                                    variable var = ObtenerValorVarAtr(NodoIzquierda.name,NodoDrecha.name,ambito);
                                    if(var != null)
                                    {
                                        
                                        return new ElementoExp(var.valor, var.tipo);
                                       
                                    }
                                    else
                                    {
                                        //error semantico
                                        ConstantesUSQL.AddError("Semantico", "Variable \"" + NodoIzquierda.name + "\" No declarada.", NodoIzquierda.linea, NodoIzquierda.columna);
                                    }
                                }
                                break;
                            case "identificador2":
                                //| identificador2() (<TK_PUNTO> identificador2())?   
                                //VERIFICAR CUAL DE LOS DOS CASOS SERIA EL DE OBJETO.VARIABLE
                                if(ConstantesXML.EsCondicion)
                                {
                                    return new ElementoExp(NodoIzquierda.name+"."+NodoDrecha.name, 0);
                                }else
                                {
                                    variable var = ObtenerValorVarAtr(NodoIzquierda.name,NodoDrecha.name,ambito);
                                    if(var != null)
                                    {
                                        
                                        return new ElementoExp(var.valor, var.tipo);
                                       
                                    }
                                    else
                                    {
                                        //error semantico
                                        ConstantesUSQL.AddError("Semantico", "Variable \"" + NodoIzquierda.name + "\" No declarada.", NodoIzquierda.linea, NodoIzquierda.columna);
                                    }
                                }
                                break;
                            case "menos":
                                //| menos() G()
                                Derecha = RecorrerEXP(NodoDrecha, ambito);
                                
                                SimpleNode term = (SimpleNode)exp.children[0];                            
                                
                                int newVal;
                                switch(Derecha.Tipo)
                                {
                                    case ConstantesUSQL.TipoNumero:
                                        newVal = Integer.parseInt(Derecha.Valor)*-1;
                                        Derecha.Valor = String.valueOf(newVal);
                                        return Derecha;
                                        
                                    case ConstantesUSQL.TipoDouble:
                                        double Val = Double.parseDouble(Derecha.Valor)*-1;
                                        Derecha.Valor = String.valueOf(Val);
                                        return Derecha;
                                    case ConstantesUSQL.TipoBooleano:
                                        
                                        if(Derecha.Valor.equals("1")){                                            
                                            Derecha.Valor = "-1";
                                            Derecha.Tipo = ConstantesUSQL.TipoNumero;
                                            return Derecha;
                                        }else if(Derecha.Valor.equals("0")){
                                            Derecha.Valor = "0";
                                            Derecha.Tipo = ConstantesUSQL.TipoNumero;
                                            return Derecha;
                                        }else if(Derecha.Valor.equals(ConstantesUSQL.verdadero)){
                                            Derecha.Valor = "-1";
                                            Derecha.Tipo = ConstantesUSQL.TipoNumero;
                                            return Derecha;
                                        }else if(Derecha.Valor.equals(ConstantesUSQL.falso)){
                                            Derecha.Valor = "0";
                                            Derecha.Tipo = ConstantesUSQL.TipoNumero;
                                            return Derecha;
                                        }else{
                                            break;
                                        }
                                    case ConstantesUSQL.TipoCadena:
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" con Cadena", term.linea, term.columna);
                                        break;
                                    case ConstantesUSQL.TipoFecha:
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" con Fecha", term.linea, term.columna);
                                        break;
                                    case ConstantesUSQL.TipoFechaHora:
                                        //ERROR
                                        ConstantesUSQL.AddError("Semantico", "Operacion Invalida para \"-\" con FechaHora", term.linea, term.columna);
                                        break;
                                }
                                break;
                        }                            
                            
                            break;
                        case 1:
                            SimpleNode nombreNodo = (SimpleNode)exp.children[0];
                            String casoTerminal = AnalizadorTreeConstants.jjtNodeName[nombreNodo.id];
                            
                            switch(casoTerminal){
                                case "CONTAR":
                                    //FUNCION PROPIA CONTAR
                                    //SELECCIONAR()
                                    SimpleNode sele = (SimpleNode)nombreNodo.children[0];
                                    recorridoXML rXMLE = new recorridoXML();
                                    Tabla tb = rXMLE.SeleccionarTabla(sele,ambito);
                                    
                                    return new ElementoExp(String.valueOf(tb.LstTuplas_Tabla.size()), ConstantesUSQL.TipoNumero);
                                    
                                case "FUN_FECHA":
                                    //FUNCION PROPIA FECHA
                                    return new ElementoExp(obtenerFormatoFecha(), ConstantesUSQL.TipoFecha);
                                    
                                case "FUN_FECHA_HORA":
                                    //FUNCION PROPIA FECHA_HORA
                                    return new ElementoExp(obtenerFormatoFechaHora(), ConstantesUSQL.TipoFechaHora);
                                case "LLAMADA_METODOS_FUNCIONES":
                                    //FUNCION PROPIA LLAMADA
                                    LlamadaFuncion(nombreNodo,ambito);
                                    
                                    ElementoExp e = ConstantesUSQL.variableRetorno;
                                    ConstantesUSQL.variableRetorno = null;
                                    return e;
                                    
                                case "identificador":
                                    //identificador
                                    //buscar el identificador de variable en la tabla de simbolos con el ultimo ambito
                                    if(ConstantesXML.EsCondicion)
                                    {
                                        return new ElementoExp(nombreNodo.name, 0);
                                    }else
                                    {
                                        variable var = ObtenerValorVar(nombreNodo.name,ambito);
                                        if(var != null)
                                        {
                                            if (!var.valor.equals("$"))
                                            {
                                                return new ElementoExp(var.valor, var.tipo);
                                            }
                                            else
                                            {
                                                //error semantico
                                                ConstantesUSQL.AddError("Semantico", "Variable \"" + nombreNodo.name + "\" Declarada pero SIN VALOR ASIGNADO", nombreNodo.linea, nombreNodo.columna);
                                            }
                                        }
                                        else
                                        {
                                            //error semantico
                                            ConstantesUSQL.AddError("Semantico", "Variable \"" + nombreNodo.name + "\" No declarada.", nombreNodo.linea, nombreNodo.columna);
                                        }
                                    }
                                    break;
                                case "identificador2":
                                    //Buscar el nombre del objeto o hay q ver en q casos aplica
                                    if(ConstantesXML.EsCondicion)
                                    {
                                        return new ElementoExp(nombreNodo.name, 0);
                                    }else
                                    {
                                        variable var = ObtenerValorVar(nombreNodo.name,ambito);
                                        if(var != null)
                                        {
                                            if (!var.valor.equals("$"))
                                            {
                                                return new ElementoExp(var.valor, var.tipo);
                                            }
                                            else
                                            {
                                                //error semantico
                                                ConstantesUSQL.AddError("Semantico", "Variable \"" + nombreNodo.name + "\" Declarada pero SIN VALOR ASIGNADO", nombreNodo.linea, nombreNodo.columna);
                                            }
                                        }
                                        else
                                        {
                                            //error semantico
                                            ConstantesUSQL.AddError("Semantico", "Variable \"" + nombreNodo.name + "\" No declarada.", nombreNodo.linea, nombreNodo.columna);
                                        }
                                        
                                    }
                                    break;
                                case "entero":
                                    Resultado = new ElementoExp(nombreNodo.name, ConstantesUSQL.TipoNumero);
                                    return Resultado;                                    
                                case "decimal":
                                    Resultado = new ElementoExp(nombreNodo.name, ConstantesUSQL.TipoDouble);
                                    return Resultado;                                    
                                case "tk_verdadero":
                                    Resultado = new ElementoExp(ConstantesUSQL.verdadero, ConstantesUSQL.TipoBooleano);                                         
                                    return Resultado;
                                case "tk_falso":
                                    Resultado = new ElementoExp(ConstantesUSQL.falso, ConstantesUSQL.TipoBooleano);                                         
                                    return Resultado;
                                case "cadena":
                                    Resultado = new ElementoExp(nombreNodo.name, ConstantesUSQL.TipoCadena);                                         
                                    return Resultado;
                                case "fecha":
                                    Resultado = new ElementoExp(nombreNodo.name, ConstantesUSQL.TipoFecha);                                         
                                    return Resultado;
                                case "fecha_hora":
                                    Resultado = new ElementoExp(nombreNodo.name, ConstantesUSQL.TipoFechaHora);                                         
                                    return Resultado;
                                case "nulo":
                                    Resultado = new ElementoExp("NULO", ConstantesUSQL.TipoNull);                                         
                                    return Resultado;
                                case "BOOLEANO":
                                    Resultado  = RecorrerEXP((SimpleNode)exp.children[0],ambito);
                                    break;
                            }
                            
                            break;
                    }
                    break;
                
                case "":
                    break;
            }
            
            return Resultado;
        
        } catch (Exception e) {
            return null; 
        }        
    }
//endregion
    
    //metodo para llamada de funciones
    public void LlamadaFuncion(SimpleNode nodoLLmada, String ambito)
    {
        recorridoXML rXML1 = new recorridoXML();
        
        switch(nodoLLmada.children.length)
        {
            case 1:
                //identificador2() 
                SimpleNode nombreF = (SimpleNode)nodoLLmada.children[0];                
                
                if (ConstantesUSQL.BDActual.HashMetodos_Fun.containsKey(nombreF.name))
                {
                    Metodo_Fun Fn = (Metodo_Fun) ConstantesUSQL.BDActual.HashMetodos_Fun.get(nombreF.name);                    
                            
                    //colocar nuevo ambito
                    ConstantesUSQL.PilaEjecucion.push(new elementoStack(Fn.Nombre_MetodoFun));

                    //mandar a recorrer el cuerpo del metodo 
                        
                    String texto = "CREAR PROCEDIMIENTO funcionPaja(){"+Fn.Codigo_MetodoFun + "}";
                    InputStream is = new ByteArrayInputStream(texto.getBytes());
                    Analizador analisis = new Analizador(is);
                    try {
                        SimpleNode raiz = analisis.INICIO();

                        //cargar todas las BD
                        recorridoXML rXML = new recorridoXML();
                        rXML.CargarBD_Completa();

//                       //HACER METODO SENTENCIAS INTERNAS
                        ConstantesUSQL.EjecutarAdentro = true;
                        ejecucion ejec = new ejecucion();
                        ejec.ejecutar(raiz,Fn.Nombre_MetodoFun);
                            
//                       JOptionPane.showMessageDialog(null, "Se ejecuto INTERNO", "EXITO", JOptionPane.INFORMATION_MESSAGE);

                    } catch (Exception e) {
                        System.err.println(e.toString());
                        JOptionPane.showMessageDialog(null, e.toString(), "ERROR", JOptionPane.INFORMATION_MESSAGE);
                        Server_Interface.EscribirBitacora("Error Fatal", "El server esta Sad :'v");
                    }
                        
                    //QUitamos el ultimo ambito
                    ConstantesUSQL.PilaEjecucion.pop();
                    ConstantesUSQL.CadenaMsj += " Llamada -> Procedimineto/Funcion "+nombreF.name+" ejecutada. "+ConstantesUSQL.Separador;
                    ConstantesUSQL.ultimoResultado = " Procedimineto/Funcion "+nombreF.name+" ejecutada. ";
                    
                    if ( Fn.Retorno_MetodoFun!= null &&(ConstantesUSQL.variableRetorno == null || (rXML1.ObtenerTipoInt(Fn.Retorno_MetodoFun) != ConstantesUSQL.variableRetorno.Tipo)))
                    {
                        if(!Fn.Retorno_MetodoFun.equals(""))
                        {
                            if (ConstantesUSQL.variableRetorno == null)
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Procedimineto/Funcion " + Fn.Nombre_MetodoFun + " requiere un Retorno", nombreF.linea, nombreF.columna);                                
                            }else
                            {
                                ConstantesUSQL.AddError("Semantico", "El valor del Procedimineto/Funcion " + Fn.Nombre_MetodoFun + " es distinto a "+Fn.Retorno_MetodoFun, nombreF.linea, nombreF.columna);
                            }
                        }
                    }
                    
                }else
                {
                    //error semantico
                    ConstantesUSQL.AddError("Semantico", "El procedimineto o funcion no existen el la base de datos actual.", nombreF.linea, nombreF.columna);
                }
                break;
            case 2:
                
                //identificador2() VALORES_PARAMETROS()
                //si la funcion o metoo existen
                
                SimpleNode nombre = (SimpleNode)nodoLLmada.children[0];
                SimpleNode nodoValores = (SimpleNode)nodoLLmada.children[1];
                
                if (ConstantesUSQL.BDActual.HashMetodos_Fun.containsKey(nombre.name))
                {
                    Metodo_Fun Fn = (Metodo_Fun) ConstantesUSQL.BDActual.HashMetodos_Fun.get(nombre.name);                    
                    
//                    if(Fn.HashParametros_MetodoFun.size() == nodoValores.children.length)
                    if(Fn.LstParametros_MetodoFun.size() == nodoValores.children.length)
                    {
                        //colocar nuevo ambito
                        ConstantesUSQL.PilaEjecucion.push(new elementoStack(Fn.Nombre_MetodoFun));
                        
//                        for (int i = 0; i < nodoValores.children.length; i++) {
                        
//                        Vector v = new Vector(Fn.HashParametros_MetodoFun.keySet());
//                        Collections.sort(v);
//                        Iterator it = v.iterator();
//
//                        int i =0; 
                        Parametro Par;
//
//                        while (it.hasNext()) {  
//                            //obtener llaves de hash
//                            String element =  (String)it.next();
//                        for (int i = 0; i < Fn.LstParametros_MetodoFun.size(); i++) {
                        
                        for (int i = 0; i < Fn.LstParametros_MetodoFun.size(); i++) {
//                            //obtengo el campo
//                            Par = (Parametro) Fn.HashParametros_MetodoFun.get(element);
                            Par = (Parametro) Fn.LstParametros_MetodoFun.get(i);
                            
                            //por cada dato declarar la variable en ele ultimo ambito
                            ElementoExp val = RecorrerEXP((SimpleNode)nodoValores.children[i], ambito);
                            
                            if(val != null)
                            {
                                if(rXML1.ObtenerTipoInt(Par.TIPO) == val.Tipo)
                                {                                
                                    DeclararVariableFun(Par.Nombre_Parametro,val.Tipo,val.Valor,Fn.Nombre_MetodoFun);
                                }else
                                {
                                    ConstantesUSQL.AddError("Semantico", "Declaracion de variables con distinto tipo en funcion "+nombre.name, nombre.linea, nombre.columna);
                                }
                            }else
                            {
                                //ERROR
                                ConstantesUSQL.AddError("Semantico", "Declaracion de variables con distinto tipo en funcion "+nombre.name, nombre.linea, nombre.columna);
                            }
                                
//                            iV++;
                        }
//                        }
                        //mandar a recorrer el cuerpo del metodo 
                        
                        String texto = "CREAR PROCEDIMIENTO funcionPaja(){"+Fn.Codigo_MetodoFun + "}";
                        InputStream is = new ByteArrayInputStream(texto.getBytes());
                        Analizador analisis = new Analizador(is);
                        try {
                            SimpleNode raiz = analisis.INICIO();

                            //cargar todas las BD
                            recorridoXML rXML = new recorridoXML();
                            rXML.CargarBD_Completa();

//                            //HACER METODO SENTENCIAS INTERNAS
                            ConstantesUSQL.EjecutarAdentro = true;
                            ejecucion ejec = new ejecucion();
                            ejec.ejecutar(raiz,Fn.Nombre_MetodoFun);
                            
//                            JOptionPane.showMessageDialog(null, "Se ejecuto INTERNO", "EXITO", JOptionPane.INFORMATION_MESSAGE);

                        } catch (Exception e) {
                            System.err.println(e.toString());
                            JOptionPane.showMessageDialog(null, e.toString(), "ERROR", JOptionPane.INFORMATION_MESSAGE);
                            Server_Interface.EscribirBitacora("Error Fatal", "El server esta Sad :'v");
                        }
                        
                        //QUitamos el ultimo ambito
                        ConstantesUSQL.PilaEjecucion.pop();
                        ConstantesUSQL.CadenaMsj += " Llamada -> Procedimineto/Funcion "+nombre.name+" ejecutada. "+ConstantesUSQL.Separador;
                        ConstantesUSQL.ultimoResultado = " Procedimineto/Funcion "+nombre.name+" ejecutada. ";
                        
                        if ( Fn.Retorno_MetodoFun!= null &&(ConstantesUSQL.variableRetorno == null || (rXML1.ObtenerTipoInt(Fn.Retorno_MetodoFun) != ConstantesUSQL.variableRetorno.Tipo)))
                        {
                            if(!Fn.Retorno_MetodoFun.equals(""))
                            {
                                if (ConstantesUSQL.variableRetorno == null)
                                {
                                    //ERROR
                                    ConstantesUSQL.AddError("Semantico", "Procedimineto/Funcion " + Fn.Nombre_MetodoFun + " requiere un Retorno", nombre.linea, nombre.columna);                                
                                }else
                                {
                                    ConstantesUSQL.AddError("Semantico", "El valor del Procedimineto/Funcion " + Fn.Nombre_MetodoFun + " es distinto a "+Fn.Retorno_MetodoFun, nombre.linea, nombre.columna);
                                }
                            }
                        }
                        
                    }else
                    {
                        //error semantico
                        ConstantesUSQL.AddError("Semantico", "La llamada al procedimiento o funsion "+nombre.name+" no posee la cantidad de campos necesaria.", nombre.linea, nombre.columna);
                    }

                }else
                {
                    //error semantico
                    ConstantesUSQL.AddError("Semantico", "El procedimineto o funcion no existen el la base de datos actual.", nombre.linea, nombre.columna);
                }
                
                
                break;
                
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    //Metodo que retorna la base de datos a utilizar
    public Base_Datos UsarBD(String nombreBD){
        
        Base_Datos Base;

        if(ConstantesXML.BaseDatosGeneral.HashBasesSistema.containsKey(nombreBD))
        {
            Base = (Base_Datos) ConstantesXML.BaseDatosGeneral.HashBasesSistema.get(nombreBD);
            return Base;
        }else
        {
            return null;
        }        
    }
    
//    //con librerias xml NO LAS USO 
//    public void VerificarExistenciaBD(String nombreBD){
//        //buscar y leer el archivo
//        String aux,cadena="";
////        String myDocumentPath = System.getProperty("user.home") + "\\Documents\\repcompi\\ARCHIVOS\\";
//        String myDocumentPath = "C:\\compi2\\repcompi\\ARCHIVOS\\";
//        myDocumentPath += "Maestro.usac";
//        try {
//            FileReader f = new FileReader(myDocumentPath);
//            BufferedReader b = new BufferedReader(f);
//            while((aux = b.readLine())!=null) {
//                //System.out.println(aux);
//                cadena += aux +"\n";
//            }
//            b.close();
//            
//            System.out.println(cadena);
//            
//            
//        } catch (Exception e) {
//            
//        }
//
//        //mandarlo a analizar        
//        InputStream is = new ByteArrayInputStream(cadena.getBytes());
//        AnalizadorXML analisis = new AnalizadorXML(is);
//        try {
//            interpreteXML.SimpleNode raiz = analisis.INICIO();
//                                    
//            Handler_XML handler = new Handler_XML();
//            handler.agregarNumero(raiz);
//            handler.iniciarGrafica(raiz);
//            handler.GenerarImagen();
//            System.out.println("Analisis XML maestro...");
//            JOptionPane.showMessageDialog(null, "Se grafico XML maestro", "EXITO", JOptionPane.INFORMATION_MESSAGE);                       
//                        
//        } catch (Exception e) {
//            System.err.println(e.toString());
//            JOptionPane.showMessageDialog(null, e.toString(), "ERROR XML", JOptionPane.INFORMATION_MESSAGE);
//        }
//        //XPATH
//        
//        try {
//            // La expresion xpath de busqueda
//		String xPathExpression = "/Maestro/DB[nombre_base=\"DB1\"]";
//		
//                // Carga del documento xml
// 		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
// 		DocumentBuilder builder = factory.newDocumentBuilder();
// 		Document documento = builder.parse(new File(myDocumentPath));
//
//                
//		// Preparación de xpath
// 		XPath xpath = XPathFactory.newInstance().newXPath();
//
// 		// Consultas
// 		NodeList nodos = (NodeList) xpath.evaluate(xPathExpression, documento, XPathConstants.NODESET);
//
//		for (int i=0;i<nodos.getLength();i++){
//			System.out.println(nodos.item(i).getNodeName()+" : " +
//                           nodos.item(i).getAttributes().getNamedItem("nombre_base"));
//		}
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//    }
//    
    public String ObtenerTextoSentencias(SimpleNode nodo){
        String codigo ="";
        
        String  nombreNodo = AnalizadorTreeConstants.jjtNodeName[nodo.id];
        
        switch (nombreNodo){

            case "SENTENCIAS_INTERNAS":
                for(int i = 0; i<nodo.children.length;i++)
                {
                    SimpleNode sentencia = (SimpleNode)nodo.children[i];
                    codigo += ObtenerTextoSentencias((SimpleNode)sentencia.children[0])+" \n";  
                }
                return codigo;
                
            case "LLAMADA_METODOS_FUNCIONES":
                //identificador2() <TK_PA> (VALORES_PARAMETROS())? <TK_PC> 
                if(nodo.children.length == 2)
                {
                    codigo = ((SimpleNode)nodo.children[0]).name +"(" + ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"); ";
                    return codigo;
                }else
                {
                    codigo = ((SimpleNode)nodo.children[0]).name +"( ); ";
                    return codigo;
                }
                
                
            case "VALORES_PARAMETROS":
                
                if(nodo.children.length == 1)
                {                    
                    //BOOLEANO()
                    codigo = TxtEXP((SimpleNode) nodo.children[0]);
                    return codigo;
                    
                }else
                {
                    //BOOLEANO() (<TK_COMA> BOOLEANO())*
                    for(int i = 0; i<nodo.children.length;i++)
                    {
                        SimpleNode valor = (SimpleNode)nodo.children[i];
                        codigo += TxtEXP((SimpleNode)valor.children[0]);  
                        
                        if(i+1<nodo.children.length) codigo += " , ";  
                    }
                return codigo;
                }
                
            case "INSERTAR":
                //INSERTAR_TABLA() <TK_PUNTO_COMA>                
                //INSERTAR_TABLA_ESPECIAL() <TK_PUNTO_COMA>
                
                codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0]) +"; ";
                return codigo;
                
            case "CREAR":
                
                codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0]) +"; ";
                return codigo;
            
            case "INSERTAR_TABLA":
                //<TK_INSERTAR> <TK_EN> <TK_TABLA> identificador2() <TK_PA> VALORES_PARAMETROS() <TK_PC> 
                codigo = "INSERTAR EN TABLA "+((SimpleNode)nodo.children[0]).name + " ("+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+")";
                return codigo;
            
            case "INSERTAR_TABLA_ESPECIAL":
                //<TK_INSERTAR> <TK_EN> <TK_TABLA> identificador2()                <TK_PA>                  LISTA_ID()                   <TK_PC> <TK_VALORES> <TK_PA>       VALORES_PARAMETROS()               <TK_PC> 
                codigo = "INSERTAR EN TABLA "+((SimpleNode)nodo.children[0]).name + " ("+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+") VALORES ("+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+")";
                return codigo;
            
            case "LISTA_ID_SELECT":
                //ID_SELECT() (<TK_COMA> ID_SELECT())*
                for(int i = 0; i<nodo.children.length;i++)
                {
                    SimpleNode id_sel = (SimpleNode)nodo.children[i];
                    codigo += ObtenerTextoSentencias(id_sel);
                    
                    if(i+1 < nodo.children.length) codigo+= ", "; 
                }
                return codigo;                            
                
            case "ID_SELECT":
                if(nodo.children.length==1){
                    //identificador2() 
                    return codigo = ((SimpleNode)nodo.children[0]).name;                    
                }
                else
                {
                    //identificador2() (<TK_PUNTO> identificador2())?
                    return codigo = ((SimpleNode)nodo.children[0]).name +"."+((SimpleNode)nodo.children[1]).name;                    
                }                            
                
            case "LISTA_ID":
                //identificador2() (<TK_COMA> identificador2())*
                for(int i = 0; i<nodo.children.length;i++)
                {
                    SimpleNode valor = (SimpleNode)nodo.children[i];
                    //codigo += TxtEXP((SimpleNode)valor.children[0]);  
                    codigo += valor.name;  

                    if(i+1<nodo.children.length) codigo += ", ";  
                }
                return codigo;
            
            case "ACTUALIZAR":
                if(nodo.children.length == 3){
                // <TK_ACTUALIZAR> <TK_TABLA> identificador2()               <TK_PA>                   LISTA_ID()                      <TK_PC> <TK_VALORES> <TK_PA>               VALORES_PARAMETROS()        <TK_PC> 
                codigo = "ACTUALIZAR TABLA "+((SimpleNode)nodo.children[0]).name +" ("+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+") VALORES ("+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"); ";
                }else{
                    
                //<TK_ACTUALIZAR> <TK_TABLA>                        identificador2() <TK_PA>             LISTA_ID()                   <TK_PC> <TK_VALORES> <TK_PA>             VALORES_PARAMETROS()            <TK_PC> (<TK_DONDE> CONDICIONES())? 
                    codigo = "ACTUALIZAR TABLA "+((SimpleNode)nodo.children[0]).name +" ("+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+") VALORES ("+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+") DONDE "+ObtenerTextoSentencias((SimpleNode)nodo.children[3])+"; ";
                }
                return codigo;
                
            case "CONDICIONES":
                for (int i = 0; i < nodo.children.length; i++) {
                    SimpleNode Nd = (SimpleNode) nodo.children[i];
                    
                    if(AnalizadorTreeConstants.jjtNodeName[Nd.id].equals("tk_or") || AnalizadorTreeConstants.jjtNodeName[Nd.id].equals("tk_and"))
                    {
                        codigo += " "+ Nd.name+" ";
                    }else{
                        codigo += ObtenerTextoSentencias(Nd);
                    }
                }
            return codigo;
                
            case "CONDICION":
                
                codigo = TxtEXP((SimpleNode)nodo.children[0]);
                return codigo;
                
            case "BORRAR":
                if(nodo.children.length == 1)
                {
                    //<TK_BORRAR> <TK_EN> <TK_TABLA> identificador2() 
                    codigo = "BORRAR EN TABLA "+((SimpleNode)nodo.children[0]).name+"; ";
                }else
                {
                    //<TK_BORRAR> <TK_EN> <TK_TABLA> identificador2()                              (<TK_DONDE>                CONDICIONES())? 
                    codigo = "BORRAR EN TABLA "+((SimpleNode)nodo.children[0]).name+" DONDE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"; ";
                }
                
                return codigo;
            
            case "SELECCIONAR":
                switch(nodo.children.length)
                {
                    case 1:
                        //<TK_SELECCIONAR> <TK_POR> <TK_DE> LISTA_ID() 
                        codigo = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[0])+"; ";
                        return codigo;                        
                    
                    case 2:
                        if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)nodo.children[0]).id].equals("LISTA_ID"))
                        {
                            //<TK_SELECCIONAR> <TK_POR> <TK_DE>                   LISTA_ID()                (<TK_DONDE> CONDICIONES())? 
                            codigo = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[0]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"; ";
                            return codigo;
           
                        }else
                        {
                            //<TK_SELECCIONAR>               LISTA_ID_SELECT()                          <TK_DE>                   LISTA_ID()
                            codigo = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"; ";
                            return codigo;
                            
                        }
                    case 3:
                        if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)nodo.children[0]).id].equals("LISTA_ID"))
                        {
                            //<TK_SELECCIONAR> <TK_POR> <TK_DE>                     LISTA_ID()              <TK_ORDENAR_POR>             identificador2()                                  (tk_asc()|tk_desc())
                            codigo = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[0]) + " ORDENAR_POR  "+((SimpleNode)nodo.children[1]).name+" "+((SimpleNode)nodo.children[2]).name.toUpperCase()+"; ";
                            return codigo;
                            
                        }else
                        {
                            //<TK_SELECCIONAR>                LISTA_ID_SELECT()                         <TK_DE>                  LISTA_ID()                       (<TK_DONDE>              CONDICIONES())? 
                            codigo = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+ " DONDE "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"; ";
                            return codigo;
                            
                        }
                        
                    case 4:
                        if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)nodo.children[0]).id].equals("LISTA_ID"))
                        {
                            if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)nodo.children[1]).id].equals("CONDICIONES"))
                            {
                                //<TK_SELECCIONAR> <TK_POR> <TK_DE>           LISTA_ID()                         <TK_DONDE>              CONDICIONES()                           <TK_ORDENAR_POR>             identificador2()                                (tk_asc()|tk_desc()))?
                                codigo = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[0]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+ " ORDENAR_POR  "+((SimpleNode)nodo.children[2]).name+" "+((SimpleNode)nodo.children[3]).name.toUpperCase()+"; ";
                                return codigo;
                            }else{
                                //<TK_SELECCIONAR> <TK_POR> <TK_DE>                     LISTA_ID()              <TK_ORDENAR_POR>             identificador2()              (<TK_PUNTO> (identificador2()|identificador()))?                 (tk_asc()|tk_desc())
                                codigo = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[0]) + " ORDENAR_POR  "+((SimpleNode)nodo.children[1]).name+"."+((SimpleNode)nodo.children[2]).name+" "+((SimpleNode)nodo.children[3]).name.toUpperCase()+"; ";
                                return codigo;
                            }
                                
                            
                        }else
                        {
                            //<TK_SELECCIONAR>                 LISTA_ID_SELECT()                        <TK_DE>                LISTA_ID()                        (<TK_ORDENAR_POR>                 identificador2()                             (tk_asc()|tk_desc()))?
                            codigo = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+ " ORDENAR_POR  "+((SimpleNode)nodo.children[2]).name+" "+((SimpleNode)nodo.children[3]).name.toUpperCase()+"; ";
                            return codigo;
                        }                        
                        
                    case 5:
                        if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)nodo.children[0]).id].equals("LISTA_ID_SELECT"))
                        {
                            if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)nodo.children[2]).id].equals("CONDICIONES"))
                            {
                                //<TK_SELECCIONAR>              LISTA_ID_SELECT()                          <TK_DE>                       LISTA_ID()                     (<TK_DONDE>              CONDICIONES())?                      (<TK_ORDENAR_POR>            identificador2()                              (tk_asc()|tk_desc()))?                
                                codigo = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+ " ORDENAR_POR  "+((SimpleNode)nodo.children[3]).name+" "+((SimpleNode)nodo.children[4]).name.toUpperCase()+"; ";
                                return codigo;
                            }else
                            {
                                //<TK_SELECCIONAR>                 LISTA_ID_SELECT()                        <TK_DE>                LISTA_ID()                        (<TK_ORDENAR_POR>                 identificador2()        (<TK_PUNTO> (identificador2()|identificador()))                     (tk_asc()|tk_desc()))?
                                codigo = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+ " ORDENAR_POR  "+((SimpleNode)nodo.children[2]).name+"."+((SimpleNode)nodo.children[3]).name+" "+((SimpleNode)nodo.children[4]).name.toUpperCase()+"; ";
                                return codigo;
                            }
                        }else
                        {
                             //<TK_SELECCIONAR> <TK_POR> <TK_DE>           LISTA_ID()                         <TK_DONDE>              CONDICIONES()                           <TK_ORDENAR_POR>             identificador2()         (<TK_PUNTO> (identificador2()|identificador()))                       (tk_asc()|tk_desc()))?
                            codigo = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[0]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+ " ORDENAR_POR  "+((SimpleNode)nodo.children[2]).name+"."+((SimpleNode)nodo.children[3]).name+" "+((SimpleNode)nodo.children[4]).name.toUpperCase()+"; ";
                            return codigo;
                        }
                    case 6:
                        //<TK_SELECCIONAR>              LISTA_ID_SELECT()                          <TK_DE>                       LISTA_ID()                     (<TK_DONDE>              CONDICIONES())?                      (<TK_ORDENAR_POR>            identificador2()           (<TK_PUNTO> (identificador2()|identificador()))                   (tk_asc()|tk_desc()))?                
                        codigo = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)nodo.children[1]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+ " ORDENAR_POR  "+((SimpleNode)nodo.children[3]).name+"."+((SimpleNode)nodo.children[4]).name+" "+((SimpleNode)nodo.children[5]).name.toUpperCase()+"; ";
                        return codigo;     
                
                }                                
                break;                            
            
            case "DECLARAR":
                codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0]);
                return codigo;
                
            case "DECLARAR_VARIABLE":
                if(nodo.children.length==2){
                    //<TK_DECLARAR>                              LISTA_VAR()                                                IPO_DATO() 
                    codigo= "DECLARAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[0]) + " " + ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"; ";
                    return codigo;
                }else
                {
                    //<TK_DECLARAR>                               LISTA_VAR()                                    IPO_DATO()                             (<TK_IGUAL>                  VALOR())?
                    codigo= "DECLARAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[0]) + " " + ObtenerTextoSentencias((SimpleNode)nodo.children[1])+" = "+TxtEXP((SimpleNode)((SimpleNode)nodo.children[2]).children[0])+"; ";
                    return codigo;
                }                                
                
            case "LISTA_VAR":
                //identificador() (<TK_COMA> identificador())*
                for(int i = 0; i<nodo.children.length;i++)
                    {
                        SimpleNode valor = (SimpleNode)nodo.children[i];
                        codigo += valor.name;
                        
                        if(i+1<nodo.children.length) codigo += " , ";  
                    }
                return codigo;
            
            case "TIPO_DATO":
                
                return codigo = ((SimpleNode)nodo.children[0]).name.toUpperCase();
                
            case "DECLARAR_INSTANCIA_OBJETO":
                //<TK_DECLARAR> identificador() identificador2() 
                codigo = "DECLARAR " +((SimpleNode)nodo.children[0]).name +" " + ((SimpleNode)nodo.children[1]).name+ "; ";
                return codigo;
                
            case "ASIGNAR":
                if(nodo.children.length==2)
                {
                    ////identificador()  <TK_IGUAL> VALOR()
                    codigo = ((SimpleNode)nodo.children[0]).name +" = "+TxtEXP((SimpleNode)((SimpleNode)nodo.children[1]).children[0])+"; ";
                }else{
                                       //identificador()     (<TK_PUNTO>             identificador2())?   <TK_IGUAL>              VALOR()
                    codigo = ((SimpleNode)nodo.children[0]).name +"."+((SimpleNode)nodo.children[1]).name +" = "+TxtEXP((SimpleNode)((SimpleNode)nodo.children[2]).children[0])+"; ";
                }
                return codigo;
                
            case "SI":
                if(nodo.children.length ==2)
                {
                    //<TK_SI> <TK_PA>          BOOLEANO()         <TK_PC> <TK_LLA>         SENTENCIAS_INTERNAS()                <TK_LLC> 
                    codigo= "SI ("+TxtEXP((SimpleNode)nodo.children[0])+") {\n "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"\n} ";
                }else
                {
                    //<TK_SI> <TK_PA>            BOOLEANO()       <TK_PC> <TK_LLA>               SENTENCIAS_INTERNAS()        <TK_LLC> (<TK_SINO> <TK_LLA>        SENTENCIAS_INTERNAS()        <TK_LLC>)?
                    codigo= "SI ("+TxtEXP((SimpleNode)nodo.children[0])+") {\n"+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"\n} SINO {\n "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"\n} ";
                }
                return codigo;
                
            case "SELEC":
                if(nodo.children.length==2)
                {
                    //<TK_SELECCIONA> <TK_PA>                VALOR()                              <TK_PC> <TK_LLA>                        CASOS()                     <TK_LLC>
                    codigo="SELECCIONA ("+TxtEXP((SimpleNode)((SimpleNode)nodo.children[0]).children[0])+") {\n" +ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"\n} ";
                }else
                {
                    //<TK_SELECCIONA> <TK_PA>              VALOR()                                 <TK_PC> <TK_LLA>                        CASOS()                                               (CASO_DEFECTO())?              <TK_LLC>
                    codigo="SELECCIONA ("+TxtEXP((SimpleNode)((SimpleNode)nodo.children[0]).children[0])+") {\n" +ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"\n "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"\n} ";
                }
                return codigo;
                
            case "CASOS":
                
                for (int i = 0; i < nodo.children.length; i++) {
                    SimpleNode caso = (SimpleNode)nodo.children[i];
                    
                    codigo += ObtenerTextoSentencias(caso)+"\n ";
                }                
                return codigo;
                
            case "CASO":
                //<TK_CASO>                      VALOR()                                    <TK_DOS_PUNTOS>         SENTENCIAS_INTERNAS()
                codigo = "CASO "+TxtEXP((SimpleNode)((SimpleNode)nodo.children[0]).children[0])+" : \n" + ObtenerTextoSentencias((SimpleNode)nodo.children[1]);
                return codigo;
            case "CASO_DEFECTO":
                //<TK_DEFECTO>   <TK_DOS_PUNTOS> SENTENCIAS_INTERNAS()
                codigo = "DEFECTO : \n" + ObtenerTextoSentencias((SimpleNode)nodo.children[0]);
                return codigo;
            
            case "PARA":
                //<TK_PARA> <TK_PA> <TK_DECLARAR>      identificador()     <TK_INTEGER> <TK_IGUAL>                                    VALOR()           <TK_PUNTO_COMA>           BOOLEANO()                            <TK_PUNTO_COMA> (tk_aumento()|tk_decremento()) <TK_PC> <TK_LLA>        SENTENCIAS_INTERNAS()                <TK_LLC>
                codigo = "PARA ( DECLARAR "+((SimpleNode)nodo.children[0]).name + " INTEGER = "+TxtEXP((SimpleNode)((SimpleNode)nodo.children[1]).children[0])+" ; " + TxtEXP((SimpleNode)nodo.children[2])+" ; " +((SimpleNode)nodo.children[3]).name+" ) \n {\n "+ObtenerTextoSentencias((SimpleNode)nodo.children[4])+" \n} ";
                return codigo;
                
            case "MIENTRAS":
                //<TK_MIENTRAS> <TK_PA>                   BOOLEANO()    <TK_PC> <TK_LLA>                SENTENCIAS_INTERNAS()            <TK_LLC> 
                codigo = "MIENTRAS ("+TxtEXP((SimpleNode)nodo.children[0])+") { \n" + ObtenerTextoSentencias((SimpleNode)nodo.children[1])+" \n} ";
                return codigo;
            
            case "tk_detener":
                return "DETENER;";
                
            case "IMPRIMIR":
                //<TK_IMPRIMIR> <TK_PA>               VALOR()                                       <TK_PC>
                codigo = "IMPRIMIR ("+TxtEXP((SimpleNode)((SimpleNode)nodo.children[0]).children[0])+"); ";
                return codigo;
            
            case "RETORNO":
                //<TK_RETORNO> VALOR() 
                codigo="RETORNO "+TxtEXP((SimpleNode)((SimpleNode)nodo.children[0]).children[0])+"; ";
                return codigo;
                
            case "CREAR_BD":
                //<TK_CREAR> <TK_BD> identificador2() <TK_PUNTO_COMA>
                codigo ="CREAR BASE_DATOS "+((SimpleNode)nodo.children[0]).name +"; ";
                return codigo;
                
            case "CREAR_TABLA":
                //<TK_CREAR> <TK_TABLA> identificador2()                 <TK_PA>                PARAMETROS_TABLA()                  <TK_PC> <TK_PUNTO_COMA>
                codigo = "CREAR TABLA "+((SimpleNode)nodo.children[0]).name +" ("+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"); ";
                return codigo;
                
            case "PARAMETROS_TABLA":
                for (int i = 0; i < nodo.children.length; i++) {
                    //PARAMETRO_TABLA() (<TK_COMA> PARAMETRO_TABLA())*
                    SimpleNode param = (SimpleNode)nodo.children[i];
                    codigo += ObtenerTextoSentencias(param);
                    
                    if(i+1 < nodo.children.length) codigo+= ", ";
                }
                return codigo;
                
            case "PARAMETRO_TABLA":
                if(nodo.children.length==2)
                {
                    //TIPO_DATO() (identificador()|identificador2()) 
                    codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0])+ " "+((SimpleNode)nodo.children[1]).name;
                }else
                {
                    //TIPO_DATO() (identificador()|identificador2()) (COMPLEMENTOS_CAMPO())?
                    codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0])+ " "+((SimpleNode)nodo.children[1]).name+ " "+ObtenerTextoSentencias((SimpleNode)nodo.children[2]);
                }
                
                return codigo;
            
            case "COMPLEMENTOS_CAMPO":
                for (int i = 0; i < nodo.children.length; i++) {
                    SimpleNode compl = (SimpleNode)nodo.children[i];
                    if(compl.name.toUpperCase().equals("LLAVE_FORANEA"))
                    {
                        codigo += compl.name.toUpperCase()+" "+((SimpleNode)nodo.children[i+1]).name+" ";
                        i++;
                    }else
                    {
                        codigo += compl.name.toUpperCase()+" ";
                    }
                }                
                return codigo;
                
            case "CREAR_OBJETO":
                //<TK_CREAR> <TK_OBJETO>             identificador2()      <TK_PA>          PARAMETROS_OBJETO()                  <TK_PC> <TK_PUNTO_COMA>
                codigo = "CREAR OBJETO "+((SimpleNode)nodo.children[0]).name+" ("+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"); ";
                return codigo;
            
            case "PARAMETROS_OBJETO":
                //PARAMETRO() (<TK_COMA> PARAMETRO())*
                for (int i = 0; i < nodo.children.length; i++) {
                    SimpleNode param = (SimpleNode)nodo.children[i];
                    codigo += ObtenerTextoSentencias(param);
                    
                    if(i+1 < nodo.children.length) codigo+= ", ";
                }
                return codigo;
                
            case "PARAMETRO":
                //TIPO_DATO() (identificador()|identificador2())                
                codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0])+ " "+((SimpleNode)nodo.children[1]).name;
                
                return codigo;
            
            case "CREAR_PROCEDIMINENTO":
                if(nodo.children.length==3)
                {
                    //<TK_CREAR> <TK_PROCEDIMIENTO>             identificador2()      <TK_PA> (PARAMETROS_OBJETO())?                            <TK_PC> <TK_LLA>             SENTENCIAS_INTERNAS()           <TK_LLC> 
                    codigo = "CREAR PROCEDIMIENTO "+((SimpleNode)nodo.children[0]).name +" ("+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+") {\n "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"\n} ";
                }else
                {
                    //<TK_CREAR> <TK_PROCEDIMIENTO>             identificador2()     <TK_PA> <TK_PC> <TK_LLA> SENTENCIAS_INTERNAS() <TK_LLC> 
                    codigo = "CREAR PROCEDIMIENTO "+((SimpleNode)nodo.children[0]).name +" ( ) {\n "+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+"\n} ";
                }
                return codigo;
            
            case "CREAR_FUNCION":
                if(nodo.children.length==4)
                {
                    //<TK_CREAR> <TK_FUNCION>                 identificador2()    <TK_PA>        (PARAMETROS_OBJETO())?                  <TK_PC>                              TIPO_DATO()              <TK_LLA>                          SENTENCIAS_INTERNAS()      <TK_LLC> 
                    codigo = "CREAR FUNCION "+((SimpleNode)nodo.children[0]).name +" ("+ObtenerTextoSentencias((SimpleNode)nodo.children[1])+") "+ObtenerTextoSentencias((SimpleNode)nodo.children[2]) +" {\n "+ObtenerTextoSentencias((SimpleNode)nodo.children[3])+"\n} ";
                }else
                {
                    //<TK_CREAR> <TK_FUNCION>                 identificador2()    <TK_PA>  <TK_PC>                        TIPO_DATO()              <TK_LLA>                          SENTENCIAS_INTERNAS()      <TK_LLC> 
                    codigo = "CREAR FUNCION "+((SimpleNode)nodo.children[0]).name +" ( ) "+ObtenerTextoSentencias((SimpleNode)nodo.children[1]) +" {\n "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"\n} ";
                }
                return codigo;
            
            case "CREAR_USUARIO":
                //<TK_CREAR> <TK_USUARIO> identificador2()  <TK_COLOCAR> <TK_PASSWORD> <TK_IGUAL> cadena() 
                codigo = "CREAR USUARIO "+((SimpleNode)nodo.children[0]).name +" COLOCAR PASSWORD = "+((SimpleNode)nodo.children[1]).name+"; ";
                return codigo;
                
            case "USAR_BD":
                //<TK_USAR> identificador2() 
                codigo = "USAR " + ((SimpleNode)nodo.children[0]).name+"; ";
                return codigo;
                
            case "ALTERAR":
                codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0]);
                return codigo;
                
            case "ALTERAR_TABLA":
                if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)nodo.children[2]).id].equals("PARAMETROS_TABLA"))
                {
                    //<TK_ALTERAR> <TK_TABLA> identificador2() tk_agregar() <TK_PA> PARAMETROS_TABLA() <TK_PC> 
                    codigo = "ALTERAR TABLA " +((SimpleNode)nodo.children[0]).name+" AGREGAR ("+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"); ";
                }else
                {
                    // <TK_ALTERAR> <TK_TABLA> identificador2() tk_quitar() LISTA_ID()
                    codigo = "ALTERAR TABLA " +((SimpleNode)nodo.children[0]).name+" QUITAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"; ";
                }
                return codigo;
                
            case "ALTERAR_OBJETO":
                if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)nodo.children[2]).id].equals("PARAMETROS_OBJETO"))
                {
                    //<TK_ALTERAR> <TK_OBJETO> identificador2() tk_agregar() <TK_PA> PARAMETROS_OBJETO() <TK_PC> 
                    codigo = "ALTERAR OBJETO " +((SimpleNode)nodo.children[0]).name+" AGREGAR ("+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"); ";
                }else
                {
                    // <TK_ALTERAR> <TK_OBJETO> identificador2() tk_quitar() LISTA_ID()
                    codigo = "ALTERAR OBJETO " +((SimpleNode)nodo.children[0]).name+" QUITAR "+ObtenerTextoSentencias((SimpleNode)nodo.children[2])+"; ";
                }
            
            return codigo;

            case "ALTERAR_USUARIO":
                //<TK_ALTERAR> <TK_USUARIO> identificador2() <TK_CAMBIAR> <TK_PASSWORD> <TK_IGUAL> cadena() 
                codigo = "ALTERAR USUARIO "+((SimpleNode)nodo.children[0]).name +" CAMBIAR PASSWORD = "+((SimpleNode)nodo.children[1]).name+"; ";
                return codigo;
                
            case "ELIMINAR":
                //<TK_ELIMINAR> (tk_procedimiento()|tk_funcion()|tk_tabla()|tk_objeto()|tk_base_datos()|tk_user()) identificador2()
                codigo = "ELIMINAR "+ ((SimpleNode)nodo.children[0]).name.toUpperCase() + " "+((SimpleNode)nodo.children[1]).name+"; ";
                return codigo;
                
            case "OTORGAR":
                //<TK_OTORGAR> <TK_PERMISOS> identificador2() <TK_COMA> identificador2() <TK_PUNTO>( identificador2() | por() )
                codigo = "OTORGAR PERMISOS "+((SimpleNode)nodo.children[0]).name+", "+((SimpleNode)nodo.children[1]).name+"."+((SimpleNode)nodo.children[2]).name+"; ";
                return codigo;
            
            case "DENEGAR":
                //<TK_DENEGAR> <TK_PERMISOS> identificador2() <TK_COMA> identificador2() <TK_PUNTO>( identificador2() | por() ) 
                codigo = "DENEGAR PERMISOS "+((SimpleNode)nodo.children[0]).name+", "+((SimpleNode)nodo.children[1]).name+"."+((SimpleNode)nodo.children[2]).name+"; ";
                return codigo;
            
            case "BACKUP":
                return codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0]);
                
            case "BACKUP_USQLDUMP":
                //<TK_BACKUP> <TK_USQLDUMP> identificador2() identificador2() 
                codigo = "BACKUP USQLDUMP "+((SimpleNode)nodo.children[0]).name+ " "+ ((SimpleNode)nodo.children[1]).name+"; ";
                return codigo;
                
            case "BACKUP_COMPLETO":
                //<TK_BACKUP> <TK_COMPLETO> identificador2() identificador2() 
                codigo = "BACKUP COMPLETO "+((SimpleNode)nodo.children[0]).name+ " "+ ((SimpleNode)nodo.children[1]).name+"; ";
                return codigo;
            
//            case "RESTAURAR":
//                return codigo = ObtenerTextoSentencias((SimpleNode)nodo.children[0]);
            
            case "RESTAURAR_USQLDUMP":
                //<TK_RESTAURAR> <TK_USQLDUMP> cadena() 
                codigo = "RESTAURAR USQLDUMP "+((SimpleNode)nodo.children[0]).name+ "; ";
                return codigo;
                
            case "RESTAURAR_COMPLETO":
                //<TK_RESTAURAR> <TK_COMPLETO> cadena() 
                codigo = "RESTAURAR COMPLETO "+((SimpleNode)nodo.children[0]).name+ "; ";
                return codigo;                
        }
        
        return codigo;
    }
    
    public String TxtEXP(SimpleNode exp)
    {        
        try {
            String Izquierda, Derecha, Resultado = null;
            
            String nombreExp = AnalizadorTreeConstants.jjtNodeName[exp.id];
            switch (nombreExp)
            {
                case "BOOLEANO":
                    
                    switch (exp.children.length)
                    {
                        case 3:
                        //J() (tk_or() BOOLEANO())?
                            Izquierda = TxtEXP((SimpleNode)exp.children[0]);
                            Derecha  = TxtEXP((SimpleNode)exp.children[2]);
                            
                            Resultado = Izquierda + " || " + Derecha +" ";
                            
                            break;
                        case 1:
                        //J()
                            Resultado  = TxtEXP((SimpleNode)exp.children[0]);
                            break;
                    }
                    break;
                case "J":
                    switch (exp.children.length)
                    {
                        case 3:
                        //K() (tk_and() J())?
                            Izquierda = TxtEXP((SimpleNode)exp.children[0]);
                            Derecha  = TxtEXP((SimpleNode)exp.children[2]);
                            
                            Resultado = Izquierda + " && " + Derecha+" ";
                            
                            break;
                        case 1:
                        //K()
                            Resultado  = TxtEXP((SimpleNode)exp.children[0]);
                            break;
                    }
                    break;
                case "K":
                    switch (exp.children.length)
                    {
                        case 2:
                        //tk_not() BOOLEANO()
                            Izquierda = TxtEXP((SimpleNode)exp.children[1]);
                            
                            Resultado = "!" + Izquierda+" ";
                            
                            break;
                            
                        case 1:
                        //RELACIONAL()
                            Resultado  = TxtEXP((SimpleNode)exp.children[0]);
                            break;
                    }
                    break;
                case "RELACIONAL":
                    
                    switch (exp.children.length)
                    {
                        case 3:
                        //ARIT() ((tk_igual_igual()|tk_no_igual()|tk_mayor_igual()|tk_menor_igual()|tk_mayor()|tk_menor()) ARIT())?
                            Izquierda = TxtEXP((SimpleNode)exp.children[0]);
                            SimpleNode operador = (SimpleNode) exp.children[1];
                            //signo
                            String operacion = operador.name;
                            
                            Derecha  = TxtEXP((SimpleNode)exp.children[2]);
                                                                                   
                            switch (operacion)
                            {
                                //casos de operaciones < > == .....
                                case "==":
                                    return Resultado = Izquierda + " == " + Derecha +" ";
                                    
                                case "!=":
                                    return Resultado = Izquierda + " != " + Derecha +" ";                                       
                                
                                case "<":
                                    return Resultado = Izquierda + " < " + Derecha +" ";
                                    
                                case ">":
                                    return Resultado = Izquierda + " > " + Derecha +" ";
                                
                                case "<=":
                                    return Resultado = Izquierda + " <= " + Derecha +" ";
                                    
                                case ">=":
                                    return Resultado = Izquierda + " >= " + Derecha +" ";
                                        
                                }                                                                                
                            break;
                        case 1:
                        //ARIT()
                            Resultado  = TxtEXP((SimpleNode)exp.children[0]);
                            break;
                    }
                    break;
                case "ARIT":                    
                    switch (exp.children.length)
                    {
                        case 3:
                        //T() ((mas()|menos()) ARIT())?
                            Izquierda = TxtEXP((SimpleNode)exp.children[0]);
                            
                            SimpleNode operador = (SimpleNode) exp.children[1];
                            //signo
                            String operacion = operador.name;
                            
                            Derecha  = TxtEXP((SimpleNode)exp.children[2]);
                                                        
                            switch (operacion)
                            {
                                //casos de operaciones + o -
                                case "+":
                                    return Resultado = Izquierda + " + " + Derecha +" ";
                                case "-":
                                    return Resultado = Izquierda + " - " + Derecha +" ";
                            }
                            
                            break;
                        case 1:
                        //T()
                            Resultado  = TxtEXP((SimpleNode)exp.children[0]);
                            break;
                    }
                    break;
                case "T":                    
                    switch (exp.children.length)
                    {
                        case 3:
                        //F() ((por()|div())  T())?
                            Izquierda = TxtEXP((SimpleNode)exp.children[0]);
                            
                            SimpleNode operador = (SimpleNode) exp.children[1];
                            //signo
                            String operacion = operador.name;
                            
                            Derecha  = TxtEXP((SimpleNode)exp.children[2]);
                           
                            
                            switch (operacion)
                            {
                                //casos de operaciones * o /
                                case "*":
                                    return Resultado = Izquierda + " * " + Derecha +" ";
                                case "/":
                                   return Resultado = Izquierda + " / " + Derecha +" "; 
                            }
                            
                            break;
                        case 1:
                        //F()
                            Resultado  = TxtEXP((SimpleNode)exp.children[0]);
                            break;
                    }
                    break;
                case "F":
                    switch (exp.children.length)
                    {
                        case 3:
                        //G() (potencia() F())?
                            Izquierda = TxtEXP((SimpleNode)exp.children[0]);                                                        
                            //^
                            Derecha  = TxtEXP((SimpleNode)exp.children[2]);
                            
                            return Resultado = Izquierda + " ^ " + Derecha +" ";
                            
                        case 1:
                        //G()
                            Resultado  = TxtEXP((SimpleNode)exp.children[0]);
                            break;
                    }
                    break;
                case "G":
                    switch (exp.children.length)
                    {
                        case 2:
                        SimpleNode NodoIzquierda = (SimpleNode) exp.children[0];
                        SimpleNode NodoDrecha = (SimpleNode) exp.children[1];
                        String caso = AnalizadorTreeConstants.jjtNodeName[NodoIzquierda.id];
                        switch(caso)
                        {
                            case "identificador":
                                //| identificador() (<TK_PUNTO> identificador2())?
                                //VERIFICAR CUAL DE LOS DOS CASOS SERIA EL DE OBJETO.VARIABLE
                                return Resultado = NodoIzquierda.name + "." + NodoDrecha.name +" "; 
                                
                            case "identificador2":
                                //| identificador2() (<TK_PUNTO> identificador2())?   
                                //VERIFICAR CUAL DE LOS DOS CASOS SERIA EL DE OBJETO.VARIABLE
                                return Resultado = NodoIzquierda.name + "." + NodoDrecha.name +" "; 
                                
                            case "menos":
                                //| menos() G()
                                Derecha = TxtEXP(NodoDrecha);
                                Resultado = "-" + Derecha +" "; 
                                return Resultado;
                                
                        }                            
                            
                            break;
                        case 1:
                            SimpleNode nombreNodo = (SimpleNode)exp.children[0];
                            String casoTerminal = AnalizadorTreeConstants.jjtNodeName[nombreNodo.id];
                            
                            switch(casoTerminal){
                                case "CONTAR":
                                    //FUNCION PROPIA CONTAR
                                    //<TK_CONTAR> <TK_PA> <TK_MENOR> <TK_MENOR> SELECCIONAR() <TK_MAYOR> <TK_MAYOR><TK_PC>
                                    Resultado = "CONTAR (<<"+TxtEXP((SimpleNode)nombreNodo.children[0])+">>)";
                                    return Resultado;
                                    
                                case "FUN_FECHA":
                                    //FUNCION PROPIA FECHA
                                    //<TK_FECHA> <TK_PA> <TK_PC>
                                    return Resultado = "FECHA()";
                                    
                                case "FUN_FECHA_HORA":
                                    //FUNCION PROPIA FECHA_HORA
                                    //<TK_FECHA_HORA> <TK_PA> <TK_PC>
                                    return Resultado = "FECHA_HORA()";
                                    
                                case "LLAMADA_METODOS_FUNCIONES":
                                    return Resultado = ObtenerTextoSentencias(nombreNodo);
                                    //FUNCION PROPIA LLAMADA
                                    
                                case "identificador":
                                    //identificador
                                    return Resultado = (nombreNodo.name);
                                    //buscar el identificador de variable en la tabla de simbolos con el ultimo ambito
                                    
                                case "identificador2":
                                    //Buscar el nombre del objeto o hay q ver en q casos aplica
                                    return Resultado = (nombreNodo.name);
                                    
                                case "entero":
                                    return Resultado = (nombreNodo.name);                                        
                                                                      
                                case "decimal":
                                    return Resultado = (nombreNodo.name);                                    
                                case "tk_verdadero":
                                    return Resultado = (nombreNodo.name);
                                case "tk_falso":
                                    return Resultado = (nombreNodo.name);
                                case "cadena":
                                    return Resultado = (nombreNodo.name);
                                case "fecha":
                                    return Resultado = (nombreNodo.name);
                                case "fecha_hora":
                                    return Resultado = (nombreNodo.name);
                                case "nulo":
                                    return Resultado = (nombreNodo.name);
                                case "BOOLEANO":
                                    
                                    return TxtEXP((SimpleNode)exp.children[0]);
                            }
                            
                            break;
                    }
                    break;
                
                case "SELECCIONAR":
                    switch(exp.children.length)
                {
                    case 1:
                        //<TK_SELECCIONAR> <TK_POR> <TK_DE> LISTA_ID() 
                        Resultado = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)exp.children[0]);
                        return Resultado;
                    
                    case 2:
                        if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)exp.children[0]).id].equals("LISTA_ID"))
                        {
                            //<TK_SELECCIONAR> <TK_POR> <TK_DE>                   LISTA_ID()                (<TK_DONDE> CONDICIONES())? 
                            Resultado = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)exp.children[0]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)exp.children[1]);
                            return Resultado;
                        }else
                        {
                            //<TK_SELECCIONAR>               LISTA_ID_SELECT()                          <TK_DE>                   LISTA_ID()
                            Resultado = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)exp.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)exp.children[1]);
                            return Resultado;
                        }
                    case 3:
                        if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)exp.children[0]).id].equals("LISTA_ID"))
                        {
                            //<TK_SELECCIONAR> <TK_POR> <TK_DE>                     LISTA_ID()              <TK_ORDENAR_POR>             identificador2()                                  (tk_asc()|tk_desc())
                            Resultado = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)exp.children[0]) + " ORDENAR_POR  "+((SimpleNode)exp.children[1]).name+" "+((SimpleNode)exp.children[2]).name.toUpperCase();
                            return Resultado;
                        }else
                        {
                            //<TK_SELECCIONAR>                LISTA_ID_SELECT()                         <TK_DE>                  LISTA_ID()                       (<TK_DONDE>              CONDICIONES())? 
                            Resultado = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)exp.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)exp.children[1])+ " DONDE "+ObtenerTextoSentencias((SimpleNode)exp.children[2]);
                            return Resultado;
                        }
                        
                    case 4:
                        if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)exp.children[0]).id].equals("LISTA_ID"))
                        {
                            if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)exp.children[1]).id].equals("CONDICIONES"))
                            {
                                //<TK_SELECCIONAR> <TK_POR> <TK_DE>           LISTA_ID()                         <TK_DONDE>              CONDICIONES()                           <TK_ORDENAR_POR>             identificador2()                                (tk_asc()|tk_desc()))?
                                Resultado = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)exp.children[0]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)exp.children[1])+ " ORDENAR_POR  "+((SimpleNode)exp.children[2]).name+" "+((SimpleNode)exp.children[3]).name.toUpperCase();
                                return Resultado;
                            }else
                            {
                                //<TK_SELECCIONAR> <TK_POR> <TK_DE>                     LISTA_ID()              <TK_ORDENAR_POR>             identificador2()              (<TK_PUNTO> (identificador2()|identificador()))?                 (tk_asc()|tk_desc())
                                Resultado = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)exp.children[0]) + " ORDENAR_POR  "+((SimpleNode)exp.children[1]).name+"."+((SimpleNode)exp.children[2]).name+" "+((SimpleNode)exp.children[3]).name.toUpperCase();
                                return Resultado;
                            }
                            
                        }else
                        {
                            //<TK_SELECCIONAR>                 LISTA_ID_SELECT()                        <TK_DE>                LISTA_ID()                        (<TK_ORDENAR_POR>                 identificador2()                             (tk_asc()|tk_desc()))?
                            Resultado = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)exp.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)exp.children[1])+ " ORDENAR_POR  "+((SimpleNode)exp.children[2]).name+" "+((SimpleNode)exp.children[3]).name.toUpperCase();
                            return Resultado;
                        }                        
                        
                    case 5:
                        if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)exp.children[0]).id].equals("LISTA_ID_SELECT"))
                        {
                            if(AnalizadorTreeConstants.jjtNodeName[((SimpleNode)exp.children[2]).id].equals("CONDICIONES"))
                            {
                                //<TK_SELECCIONAR>              LISTA_ID_SELECT()                          <TK_DE>                       LISTA_ID()                     (<TK_DONDE>              CONDICIONES())?                      (<TK_ORDENAR_POR>            identificador2()                              (tk_asc()|tk_desc()))?                
                                Resultado = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)exp.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)exp.children[1]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)exp.children[2])+ " ORDENAR_POR  "+((SimpleNode)exp.children[3]).name+" "+((SimpleNode)exp.children[4]).name.toUpperCase();
                                return Resultado;
                            }else
                            {
                                //<TK_SELECCIONAR>                 LISTA_ID_SELECT()                        <TK_DE>                LISTA_ID()                        (<TK_ORDENAR_POR>                 identificador2()        (<TK_PUNTO> (identificador2()|identificador()))                     (tk_asc()|tk_desc()))?
                                Resultado = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)exp.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)exp.children[1])+ " ORDENAR_POR  "+((SimpleNode)exp.children[2]).name+"."+((SimpleNode)exp.children[3]).name+" "+((SimpleNode)exp.children[4]).name.toUpperCase();
                                return Resultado;
                            }
                            
                        }else
                        {
                            //<TK_SELECCIONAR> <TK_POR> <TK_DE>           LISTA_ID()                         <TK_DONDE>              CONDICIONES()                           <TK_ORDENAR_POR>             identificador2()         (<TK_PUNTO> (identificador2()|identificador()))                       (tk_asc()|tk_desc()))?
                            Resultado = "SELECCIONAR * DE "+ObtenerTextoSentencias((SimpleNode)exp.children[0]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)exp.children[1])+ " ORDENAR_POR  "+((SimpleNode)exp.children[2]).name+"."+((SimpleNode)exp.children[3]).name+" "+((SimpleNode)exp.children[4]).name.toUpperCase();
                            return Resultado;
                        }
                    
                    case 6:
                        //<TK_SELECCIONAR>              LISTA_ID_SELECT()                          <TK_DE>                       LISTA_ID()                     (<TK_DONDE>              CONDICIONES())?                      (<TK_ORDENAR_POR>            identificador2()           (<TK_PUNTO> (identificador2()|identificador()))                   (tk_asc()|tk_desc()))?                
                        Resultado = "SELECCIONAR "+ObtenerTextoSentencias((SimpleNode)exp.children[0])+" DE "+ObtenerTextoSentencias((SimpleNode)exp.children[1]) + " DONDE "+ObtenerTextoSentencias((SimpleNode)exp.children[2])+ " ORDENAR_POR  "+((SimpleNode)exp.children[3]).name+"."+((SimpleNode)exp.children[4]).name+" "+((SimpleNode)exp.children[5]).name.toUpperCase();
                        return Resultado;                                              
                
                }
                    break;
            }
            
            return Resultado;
        
        } catch (Exception e) {
            return null; 
        }        
    }

    //declarar Variables
    public void DeclararVariables(SimpleNode Variables, int Tipo, String Valor, String Ambito) {
        
        String ambitoActual, nombreVAr;
        
        if (ConstantesUSQL.PilaEjecucion.size() == 0)
        {
            for (int i = 0; i < Variables.children.length; i++) {
                SimpleNode variable = (SimpleNode) Variables.children[i];
                nombreVAr = variable.name;
                
                ambitoActual = Ambito + "|" + nombreVAr;
                
                if(!ConstantesUSQL.TablaSimbolos.containsKey(ambitoActual)){
                    ConstantesUSQL.TablaSimbolos.put(ambitoActual, new variable(nombreVAr, Tipo, Valor, variable.linea+"", variable.columna+""));
                    
                    Server_Interface.EscribirBitacora("Declaracion de Variable", "Variable "+nombreVAr+" delcarada exitosamente.");
                    
                    ConstantesUSQL.CadenaMsj += " Declarar -> Variable "+nombreVAr+" delcarada exitosamente. "+ConstantesUSQL.Separador;
                    ConstantesUSQL.ultimoResultado = " Variable "+nombreVAr+" delcarada exitosamente.";
                    
                }else
                {
                    ConstantesUSQL.AddError("Semantico", "Variable "+nombreVAr+" ya declarada en ambito "+Ambito, variable.linea, variable.columna);
                }
            }

        }
        else
        {
            //insertar la o las variables en el ultimo ambito
            
            for (int i = 0; i < Variables.children.length; i++) {
                SimpleNode variable = (SimpleNode) Variables.children[i];
                nombreVAr = variable.name;
                
                ambitoActual = Ambito + "|" + nombreVAr;
                
                if(!ExisteEnAmbito(Ambito,nombreVAr)){
                    
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoActual, new variable(nombreVAr, Tipo, Valor, variable.linea+"", variable.columna+""));
                    Server_Interface.EscribirBitacora("Declaracion de Variable", "Variable "+nombreVAr+" delcarada exitosamente.");
                    
                    ConstantesUSQL.CadenaMsj += " Declarar -> Variable "+nombreVAr+" delcarada exitosamente. "+ConstantesUSQL.Separador;
                    ConstantesUSQL.ultimoResultado = " Variable "+nombreVAr+" delcarada exitosamente.";
                    
                }else
                {
                    ConstantesUSQL.AddError("Semantico", "Variable "+nombreVAr+" ya declarada en ambito "+Ambito, variable.linea, variable.columna);
                }
            }
                
        }
    }
    
    //declararVariableFuncion
    public void DeclararVariableFun(String nombreVAr, int Tipo, String Valor, String Ambito) {
        
        String ambitoActual;
        
        if (ConstantesUSQL.PilaEjecucion.size() == 0)
        {

            ambitoActual = Ambito + "|" + nombreVAr;

            if(!ConstantesUSQL.TablaSimbolos.containsKey(ambitoActual)){
                ConstantesUSQL.TablaSimbolos.put(ambitoActual, new variable(nombreVAr, Tipo, Valor, "", ""));
                
                Server_Interface.EscribirBitacora("Declaracion de Variable", "Variable "+nombreVAr+" delcarada exitosamente.");
                
                ConstantesUSQL.CadenaMsj += " Declarar -> Variable "+nombreVAr+" delcarada exitosamente. "+ConstantesUSQL.Separador;
                ConstantesUSQL.ultimoResultado = " Variable "+nombreVAr+" delcarada exitosamente.";
            }else
            {
                ConstantesUSQL.AddError("Semantico", "Variable "+nombreVAr+" ya declarada en ambito "+Ambito, 0, 0);
            }
        }
        else
        {
            //insertar la o las variables en el ultimo ambito
            ambitoActual = Ambito + "|" + nombreVAr;
                
            if(!ExisteEnAmbito(Ambito,nombreVAr)){

                ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoActual, new variable(nombreVAr, Tipo, Valor, "", ""));
            
                Server_Interface.EscribirBitacora("Declaracion de Variable", "Variable "+nombreVAr+" delcarada exitosamente.");
                ConstantesUSQL.CadenaMsj += " Declarar -> Variable "+nombreVAr+" delcarada exitosamente. "+ConstantesUSQL.Separador;
                ConstantesUSQL.ultimoResultado = " Variable "+nombreVAr+" delcarada exitosamente.";
            
            }else
            {
                ConstantesUSQL.AddError("Semantico", "Variable "+nombreVAr+" ya declarada en ambito "+Ambito, 0,0);
            }                       
        }
    }
    
    public boolean ExisteEnAmbito(String ambto, String var)
    {
        Boolean existe = false;

            String Buscar = ambto + "|" + var;

            if (ambto != "")
            {
                if (ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.containsKey(Buscar))
                {
                    existe = true;
                }
                else
                {
                    ambto = ReducirLlave(ambto);
                    existe = ExisteEnAmbito(ambto, var);
                }
            }

            return existe;
    }
    
    //metodo que quita un ambito del contexto actual pra crear nueva llave de busqueda
    public static String ReducirLlave(String Llave)
    {
        String NuevaLLave = "";

        String[] words = Llave.split("\\|");

        int i = 0;
        for(String s : words)
        {
            i++;

            if (i < words.length)
            {

                if (NuevaLLave.equals(""))
                {
                    NuevaLLave += s;
                }
                else
                {
                    NuevaLLave += "|" + s;
                }
            }
        }
        return NuevaLLave;
    }
    
    public variable ObtenerValorVar(String var, String ambito)
    {
        String Buscar = ambito + "|" + var;
        variable varEnPila = null;

            if (ambito != "")
            {
                if (ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.containsKey(Buscar))
                {
                    varEnPila = (variable)ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.get(Buscar);

                }
                else
                {
                    ambito = ReducirLlave(ambito);
                    varEnPila = ObtenerValorVar(var, ambito);
                }
            }
            else
            {
                // buscar en las globales
                Buscar = "Global" + "|" + var;
                if (ConstantesUSQL.TablaSimbolos.containsKey(Buscar))
                {
                    varEnPila = (variable)ConstantesUSQL.TablaSimbolos.get(Buscar);

                }
            }

            return varEnPila;
    }

    public void AsignarValorVariable(String name, String Valor, int Tipo, String ambitoActual) {
        
        String ambitoTrabajo = ambitoActual + "|" + name;
        
        if (ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.containsKey(ambitoTrabajo))
        {
            variable var = (variable)ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.get(ambitoTrabajo);
            if (var.tipo == Tipo)
            {
                // si los tipos coinsiden
                //se le asigna el valor nuevo
                var.valor = Valor;

                //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                
                Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;
                ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
            
            }else if(var.tipo == ConstantesUSQL.TipoBooleano && Tipo == ConstantesUSQL.TipoNumero ){
                if(Valor.equals("1")){
                    var.valor = "VERDADERO";

                    //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                    Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                    ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                    ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                }else if(Valor.equals("0")){
                    var.valor = "FALSO";

                    //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                    Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                    ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                    ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                }
                else
                {
                    ConstantesUSQL.AddError("Semantico", "Asignacion de tipos distintos para la variable "+name, 0,0);
                }
            }else if(var.tipo == ConstantesUSQL.TipoNumero && Tipo == ConstantesUSQL.TipoBooleano  ){
                if(Valor.equals("VERDADERO")){
                    var.valor = "1";

                    //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                    Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                    ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                    ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                }else if(Valor.equals("FALSO")){
                    var.valor = "0";

                    //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                    ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                    Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                    ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                    ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                }
                else
                {
                    ConstantesUSQL.AddError("Semantico", "Asignacion de tipos distintos para la variable "+name, 0,0);
                }
            }else
            {
                ConstantesUSQL.AddError("Semantico", "Asignacion de tipos distintos para la variable "+name, 0,0);
            }
            //se puede agregar validacion de tipos dintitntos en el caso de booleanos
        }else
        {
            // si no pertenece a ese ambito
            ambitoTrabajo = ReducirLlave(ambitoActual);
            
            if (ambitoTrabajo != "")
            {
                AsignarValorVariable(name,Valor,Tipo, ambitoTrabajo);
            }else
            {
                //busca en variables globales
                ambitoTrabajo = "Global" + "|" + name;
                
                if (ConstantesUSQL.TablaSimbolos.containsKey(ambitoTrabajo))
                {
                    variable var = (variable)ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.get(ambitoTrabajo);
                    if (var.tipo == Tipo)
                    {
                        // si los tipos coinsiden
                        //se le asigna el valor nuevo
                        var.valor = Valor;

                        //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                        ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                        ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                        Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                        ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                        ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                        
                    }else if(var.tipo == ConstantesUSQL.TipoBooleano && Tipo == ConstantesUSQL.TipoNumero ){
                        if(Valor.equals("1")){
                            var.valor = "VERDADERO";

                            //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                            Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                            ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                            ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                        }else if(Valor.equals("0")){
                            var.valor = "FALSO";

                            //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                            
                            Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                            ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                            ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                        }
                        else
                        {
                            ConstantesUSQL.AddError("Semantico", "Asignacion de tipos distintos para la variable "+name, 0,0);
                        }
                    }else if(var.tipo == ConstantesUSQL.TipoNumero && Tipo == ConstantesUSQL.TipoBooleano  ){
                        if(Valor.equals("VERDADERO")){
                            var.valor = "1";

                            //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                            Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                            ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                            ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                        }else if(Valor.equals("FALSO")){
                            var.valor = "0";

                            //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
                            Server_Interface.EscribirBitacora("Asignacion de Variable", "Variable "+name+" asignada exitosamente.");
                            ConstantesUSQL.CadenaMsj += " Asignacion -> Variable "+name+" asignada exitosamente. "+ConstantesUSQL.Separador;                
                            ConstantesUSQL.ultimoResultado = "Variable "+name+" asignada exitosamente. ";
                        }
                        else
                        {
                            ConstantesUSQL.AddError("Semantico", "Asignacion de tipos distintos para la variable "+name, 0,0);
                        }

                    }                    
                }else
                {
                    ConstantesUSQL.AddError("Semantico", "Variable "+name+" no declarada", 0,0);
                }
            }        
        }
    }

    public void InstanciarObjeto(String name, Objeto OB, String ambito) {
        String ambitoActual;
        
        if (ConstantesUSQL.PilaEjecucion.size() == 0)
        {

            ambitoActual = ambito + "|" + name;

            if(!ConstantesUSQL.TablaSimbolos.containsKey(ambitoActual)){
                
                //se puede instanciar ahora recorremos cada atributo y lo agregamos a una nueva arraylist
                
                ConstantesUSQL.TablaSimbolos.put(ambitoActual, new variableObjeto(OB.Nombre_Objero, ConstantesUSQL.TipoObjeto, new Hashtable(),OB.LstParametros_Objeto ,"", ""));
                Server_Interface.EscribirBitacora("Instanica Objeto", "Objeto "+name+" instanciado exitosamente.");
                
                ConstantesUSQL.CadenaMsj += " Declarar -> Instanica delcarada exitosamente. "+ConstantesUSQL.Separador;
                ConstantesUSQL.ultimoResultado = " Instanica delcarada exitosamente.";
                
            }else
            {
                ConstantesUSQL.AddError("Semantico", "Objeto "+name+" ya instanciado en ambito "+ambito, 0, 0);
            }
        }
        else
        {
            //insertar la o las variables en el ultimo ambito
            ambitoActual = ambito + "|" + name;
                
            if(!ExisteEnAmbito(ambito,name)){

                ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoActual, new variableObjeto(OB.Nombre_Objero, ConstantesUSQL.TipoObjeto, new Hashtable(),OB.LstParametros_Objeto ,"", ""));
                Server_Interface.EscribirBitacora("Instanica Objeto", "Objeto "+name+" instanciado exitosamente.");
                
                ConstantesUSQL.CadenaMsj += " Declarar -> Instanica delcarada exitosamente. "+ConstantesUSQL.Separador;
                ConstantesUSQL.ultimoResultado = " Instanica delcarada exitosamente.";
            }else
            {
                ConstantesUSQL.AddError("Semantico", "Objeto "+name+" ya instanciado en ambito "+ambito, 0,0);
            }                       
        }
    }

    public void AsignarValorObjeto(String name, String nameAtr, String Valor, int Tipo, String ambitoActual) {
        
        String ambitoTrabajo = ambitoActual + "|" + name;
        
        if (ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.containsKey(ambitoTrabajo))
        {
            variableObjeto var = (variableObjeto)ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.get(ambitoTrabajo);
            
            if(ConstantesUSQL.BDActual.HashObjetos.containsKey(var.nombre))
            {
                
                Objeto OB = (Objeto)ConstantesUSQL.BDActual.HashObjetos.get(var.nombre);
                            
                Parametro par = null;
                for (int i = 0; i < OB.LstParametros_Objeto.size(); i++) {
                    if(OB.LstParametros_Objeto.get(i).Nombre_Parametro.equals(nameAtr)){ par = OB.LstParametros_Objeto.get(i); break;}
                }
                            
                if(par != null){
                               
                    recorridoXML rXML = new recorridoXML();
                    
                    if(Tipo == rXML.ObtenerTipoInt(par.TIPO))
                    {                        
                        //se le asigna el valor nuevo
                        variable v = new variable(nameAtr, Tipo, Valor, "", "");
                        var.valores.put(v.nombre, v);

                        //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                        ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                        ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);

                        Server_Interface.EscribirBitacora("Asignacion de valor a atributo", "Atributo "+nameAtr+" asignado exitosamente.");
                        
                        ConstantesUSQL.CadenaMsj += " Asignacion -> Asignacion a atributo exitosa. "+ConstantesUSQL.Separador;
                        ConstantesUSQL.ultimoResultado = " Asignacion a atributo exitosa.";
                        
                    }else
                    {
                        ConstantesUSQL.AddError("Semantico", "El valor a asignar no es valido para el atributo "+nameAtr, 0, 0);
                    }                                    
                                
                }else
                {
                    ConstantesUSQL.AddError("Semantico", "El atributo "+nameAtr+" no pertenece al objeto"+OB.Nombre_Objero, 0, 0);
                }                            
                            
            }else
            {
                ConstantesUSQL.AddError("Semantico", "El objeto "+var.nombre+" no pertenece a la base de datos", 0, 0);
            }                                    
        }else
        {
            // si no pertenece a ese ambito
            ambitoTrabajo = ReducirLlave(ambitoActual);
            
            if (ambitoTrabajo != "")
            {
                AsignarValorObjeto(name,nameAtr,Valor,Tipo, ambitoTrabajo);
            }else
            {
                //busca en variables globales
                ambitoTrabajo = "Global" + "|" + name;
                
                if (ConstantesUSQL.TablaSimbolos.containsKey(ambitoTrabajo))
                {
                    variableObjeto var = (variableObjeto)ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.get(ambitoTrabajo);
            
                    if(ConstantesUSQL.BDActual.HashObjetos.containsKey(var.nombre))
                    {

                        Objeto OB = (Objeto)ConstantesUSQL.BDActual.HashObjetos.get(var.nombre);

                        Parametro par = null;
                        for (int i = 0; i < OB.LstParametros_Objeto.size(); i++) {
                            if(OB.LstParametros_Objeto.get(i).Nombre_Parametro.equals(nameAtr)){ par = OB.LstParametros_Objeto.get(i); break;}
                        }

                        if(par != null){

                            recorridoXML rXML = new recorridoXML();

                            if(Tipo == rXML.ObtenerTipoInt(par.TIPO))
                            {                        
                                //se le asigna el valor nuevo
                                variable v = new variable(nameAtr, Tipo, Valor, "", "");
                                var.valores.put(v.nombre, v);

                                //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
                                ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
                                ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);

                                Server_Interface.EscribirBitacora("Asignacion de valor a atributo", "Atributo "+nameAtr+" asignada exitosamente.");
                                
                                ConstantesUSQL.CadenaMsj += " Asignacion -> Asignacion a atributo exitosa. "+ConstantesUSQL.Separador;
                                ConstantesUSQL.ultimoResultado = " Asignacion a atributo exitosa.";

                            }else
                            {
                                ConstantesUSQL.AddError("Semantico", "El valor a asignar no es valido para el atributo "+nameAtr, 0, 0);
                            }                                    

                        }else
                        {
                            ConstantesUSQL.AddError("Semantico", "El atributo "+nameAtr+" no pertenece al objeto"+OB.Nombre_Objero, 0, 0);
                        }                            

                    }else
                    {
                        ConstantesUSQL.AddError("Semantico", "El objeto "+var.nombre+" no pertenece a la base de datos", 0, 0);
                    }
                    
                }else
                {
                    ConstantesUSQL.AddError("Semantico", "Variable "+name+" no declarada", 0,0);
                }
            }        
        }        
    }

    public variable ObtenerValorVarAtr(String nameInstanica, String nameAtr, String ambito) {
                
        String Buscar = ambito + "|" + nameInstanica;
        
        variable varEnPila = null;

            if (ambito != "")
            {
                if (ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.containsKey(Buscar))
                {
                    variableObjeto obj = (variableObjeto)ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.get(Buscar);
                    
                    if(obj.valores.containsKey(nameAtr))
                    {
                        varEnPila =(variable) obj.valores.get(nameAtr);
                    }else
                    {
                        varEnPila = null;
                    }
                    
                }
                else
                {
                    ambito = ReducirLlave(ambito);
                    varEnPila = ObtenerValorVarAtr(nameInstanica,nameAtr, ambito);
                }
            }
            else
            {
                // buscar en las globales
                Buscar = "Global" + "|" + nameInstanica;
                if (ConstantesUSQL.TablaSimbolos.containsKey(Buscar))
                {
                    variableObjeto obj = (variableObjeto)ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.get(Buscar);
                    
                    if(obj.valores.containsKey(nameAtr))
                    {
                        varEnPila =(variable) obj.valores.get(nameAtr);
                    }else
                    {
                        varEnPila = null;
                    }
                }
            }

            return varEnPila;
        
    }

    private void recorrerIf(SimpleNode nodo, String ambitoActual) {

        String nuevoAmbito;
        ElementoExp val;        
        
        switch (nodo.children.length)
        {
            ////BOOLEANO() SENTENCIAS_INTERNAS() (<TK_SINO> SENTENCIAS_INTERNAS() )?
            case 3:
                val = RecorrerEXP((SimpleNode) nodo.children[0], ambitoActual);

                if (val.Tipo == ConstantesUSQL.TipoBooleano)
                {
                    nuevoAmbito = ambitoActual + "|" + "if";
                    if (val.Valor.equals(ConstantesUSQL.verdadero))
                    {
                        ejecutar((SimpleNode)nodo.children[1], nuevoAmbito);
                    }
                    else
                    {
                        ejecutar((SimpleNode)nodo.children[2], nuevoAmbito);
                    }

                }
                else
                {
                    // no es booleano
                    ConstantesUSQL.AddError("Semantico", "Error en sentencia SI La condicion no es del tipo Booleano", 0, 0);                        
                }
                break;

                
            case 2:
                //BOOLEANO() SENTENCIAS_INTERNAS() 
                val = RecorrerEXP((SimpleNode) nodo.children[0], ambitoActual);                    

                if (val.Tipo == ConstantesUSQL.TipoBooleano)
                {
                    nuevoAmbito = ambitoActual + "|" + "if";
                    if (val.Valor.equals(ConstantesUSQL.verdadero))
                    {
                        ejecutar((SimpleNode)nodo.children[1], nuevoAmbito);
                    }
                }
                else
                {
                    // no es booleano
                    ConstantesUSQL.AddError("Semantico", "Error en sentencia SI La condicion no es del tipo Booleano", 0, 0);                        
                }
                break;
            }
            nuevoAmbito = ambitoActual + "|" + "if";
            eliminarVariables(nuevoAmbito);
    }
    
    public static void eliminarVariables(String ambi)
    {
        ArrayList<String> ListaEliminar = new ArrayList<String>() ;

        Vector v = new Vector(ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.keySet());
        Collections.sort(v);
        Iterator it = v.iterator();

        int i =0; 
        
        while (it.hasNext()) {  
            //obtener llaves de hash
            String element =  (String)it.next();

            if(element.contains(ambi))
            {
                ListaEliminar.add(element);
            }                
        }
        
        for (int j = 0; j < ListaEliminar.size(); j++) {
            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ListaEliminar.get(j));
        }

    }
    
    public void RecorrerMientras(SimpleNode nodoMientras, String ambitoActual)
    {
        // BOOLEANO()  SENTENCIAS_INTERNAS()         
        String AmbitoTrabajo = ambitoActual + "|" + "mientras";

        ElementoExp valExp = RecorrerEXP((SimpleNode)nodoMientras.children[0], ambitoActual);

        if (valExp.Tipo == (ConstantesUSQL.TipoBooleano))
        {
            String ambitoAux = AmbitoTrabajo + "|" + "dentroMientras";
            while (valExp.Valor.equals(ConstantesUSQL.verdadero))
            {
                //realizar las acciones

                ejecutar((SimpleNode) nodoMientras.children[1], ambitoAux);

                eliminarVariables(ambitoAux);

                valExp = RecorrerEXP((SimpleNode) nodoMientras.children[0], ambitoActual);
            }
        }
        else
        {
            //error semantico en exprecion de comparacion
            ConstantesUSQL.AddError("Semantico", "Error en ciclo MIENTRAS Condicon no es del tipo Bool", 0, 0);

        }
    }
    
    public  void RecorrerSelecciona(SimpleNode nodoSwitch, String ambitoActual)
    {
        ElementoExp val, valorComparar;
        String nuevoAmbito; 
        
        switch (nodoSwitch.children.length) {
            case 3:
                //VALOR() CASOS() (CASO_DEFECTO())? 
                           
                nuevoAmbito = ambitoActual + "|" + "Selec";

                val = RecorrerEXP((SimpleNode) nodoSwitch.children[0], ambitoActual);

                if (val.Tipo == ConstantesUSQL.TipoCadena || val.Tipo == ConstantesUSQL.TipoNumero || val.Tipo == ConstantesUSQL.TipoDouble)
                {
                    SimpleNode casos = (SimpleNode)nodoSwitch.children[1];

                    boolean cumple = true;
                    for (int i = 0; i < casos.children.length; i++) {                    
                        SimpleNode caso = (SimpleNode)casos.children[i];
                        //VALOR()  SENTENCIAS_INTERNAS()

                        valorComparar = RecorrerEXP((SimpleNode)caso.children[0], ambitoActual);
                        
                        if(valorComparar.Tipo == ConstantesUSQL.TipoBooleano || valorComparar.Tipo == ConstantesUSQL.TipoNumero)
                        {
                            valorComparar.Valor = String.valueOf(Double.parseDouble(valorComparar.Valor));
                            val.Valor = String.valueOf(Double.parseDouble(val.Valor));
                        }

                        if (valorComparar.Tipo == val.Tipo && valorComparar.Valor.equals(val.Valor))
                        {
                            ejecutar((SimpleNode)caso.children[1], nuevoAmbito);
                            cumple =false;
                            break;
                        }                            
                    }
                    if (cumple) {
                        ejecutar((SimpleNode)nodoSwitch.children[2], nuevoAmbito);

                    }

                }
                else
                {
                    // error por Exprecion bool
                    ConstantesUSQL.AddError("Semantico", "Error en flujo Selecciona La exprecion a comparar no es Numero o Cadena", 0, 0);

                }

                eliminarVariables(nuevoAmbito);                
                break;
            case 2:
                //VALOR() CASOS()
                           
                nuevoAmbito = ambitoActual + "|" + "Selec";

                val = RecorrerEXP((SimpleNode) nodoSwitch.children[0], ambitoActual);

                if (val.Tipo == ConstantesUSQL.TipoCadena || val.Tipo == ConstantesUSQL.TipoNumero || val.Tipo == ConstantesUSQL.TipoBooleano)
                {
                    SimpleNode casos = (SimpleNode)nodoSwitch.children[1];
                    
                    for (int i = 0; i < casos.children.length; i++) {                    
                        SimpleNode caso = (SimpleNode)casos.children[i];
                        //VALOR()  SENTENCIAS_INTERNAS()

                        valorComparar = RecorrerEXP((SimpleNode)caso.children[0], ambitoActual);
                        
                        if(valorComparar.Tipo == ConstantesUSQL.TipoBooleano || valorComparar.Tipo == ConstantesUSQL.TipoNumero)
                        {
                            valorComparar.Valor = String.valueOf(Double.parseDouble(valorComparar.Valor));
                            val.Valor = String.valueOf(Double.parseDouble(val.Valor));
                        }

                        if (valorComparar.Tipo == val.Tipo && valorComparar.Valor.equals(val.Valor))
                        {
                            ejecutar((SimpleNode)caso.children[1], nuevoAmbito);
                            break;
                        }                            
                    }                    
                }
                else
                {
                    // error por Exprecion bool
                    ConstantesUSQL.AddError("Semantico", "Error en flujo Selecciona La exprecion a comparar no es Numero o Cadena", 0, 0);

                }
                eliminarVariables(nuevoAmbito);
                break;
        }                    
    }
            
     public void RecorrerPara(SimpleNode nodoPara, String ambitoActual)
    {
        // identificador() = VALOR() ; BOOLEANO() ; (tk_aumento()|tk_decremento()) SENTENCIAS_INTERNAS()             
        String nuevoAmbito;
        ElementoExp valor, valorCondicion;

        nuevoAmbito = ambitoActual + "|" + "Para";

        //declarar la variable para el for        
            
        valor = RecorrerEXP((SimpleNode)nodoPara.children[1], ambitoActual);
            
        if (valor.Tipo == ConstantesUSQL.TipoNumero)
        {
            DeclararVariableFun(((SimpleNode)nodoPara.children[0]).name, ConstantesUSQL.TipoNumero, valor.Valor, nuevoAmbito);
        }
        else
        {
            //error semantico
            ConstantesUSQL.AddError("Semantico", "Error en declaracion de variable de control en ciclo PARA", 0, 0);
        }

        //evaluar condicion

        valorCondicion = RecorrerEXP((SimpleNode)nodoPara.children[2], nuevoAmbito);
            
        if (valorCondicion.Tipo == (ConstantesUSQL.TipoBooleano))
        {
            String ambitoAux = nuevoAmbito + "|" + "dentroFor";
            while (valorCondicion.Valor.equals(ConstantesUSQL.verdadero))
            {
                //realizar las acciones
                ejecutar((SimpleNode)nodoPara.children[4], ambitoAux);

                eliminarVariables(ambitoAux);

                //aumentar o decrementar segun sea el caso
                AumentarDecrementar(((SimpleNode)nodoPara.children[0]).name, ((SimpleNode)nodoPara.children[3]).name, nuevoAmbito);

                // actualizar la condicion
                valorCondicion = RecorrerEXP((SimpleNode)nodoPara.children[2], nuevoAmbito);
            }
        }
        else
        {
            //error semantico en condicion
            ConstantesUSQL.AddError("Semantico", "Error condicion en ciclo PARA, La condicion no es un valor booleano", 0, 0);
        }
            eliminarVariables(nuevoAmbito);
    }
     
     public void AumentarDecrementar(String nombreVAr, String AumentoDecremento, String ambitoActual)
    {
        String ambitoTrabajo = ambitoActual + "|" + nombreVAr;

        if (ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.containsKey(ambitoTrabajo))
        {
            variable var = (variable)ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.get(ambitoTrabajo);

            if (AumentoDecremento.equals("++"))
            {
                var.valor = String.valueOf(Double.valueOf(var.valor) + 1);
            }
            else
            {
                var.valor = String.valueOf(Double.valueOf(var.valor) - 1);
            }
            //remuevo el elemento de la tabla hash para agregarlo con el nuevo valor
            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.remove(ambitoTrabajo);
            ConstantesUSQL.PilaEjecucion.peek().LstVarLocales.put(ambitoTrabajo, var);
        }
        else
        {
            // si no pertenece a ese ambito la variable no existe                
            ConstantesUSQL.AddError("Semantico", "Error variable de control en ciclo PARA no existe.", 0, 0);
        }
    }
     
    private static Date obtenerFecha() {
        Date date = new Date();   
        
        return date;
    }
    
    public static String obtenerFormatoFechaHora(){
        DateFormat formatoFechaHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        
        return formatoFechaHora.format(obtenerFecha());
    }
    
    public static String obtenerFormatoFecha(){
        DateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
        
        return formatoFecha.format(obtenerFecha());
    }
    
    public static String obtenerFormatoHora(){
        DateFormat formatoHora = new SimpleDateFormat("dd/MM/yyyy");
        
        return formatoHora.format(obtenerFecha());
    }

    private void EscribirBitacora(String bitacora) {
        if(ConstantesUSQL.BDActual != null)
        {
            String ruta = ConstantesXML.pathArchivos+"\\BK\\"+ConstantesUSQL.BDActual.Nombre_BD+".bk";
            
            File archivo = new File(ruta);
            
            String texto, cadena="";
            BufferedWriter out;
            if(archivo.exists()) {
            try {
                out = new BufferedWriter(new FileWriter(ruta, true));
                out.newLine();
                out.write(bitacora);
                out.close();
            } catch (Exception e) {
                // error processing code  
                System.out.println("Error al escribir");
            }
            
                
        } else {
                try
                {

                    //Crear objeto FileWriter que sera el que nos ayude a escribir sobre archivo
                    FileWriter escribir=new FileWriter(archivo,true);

                    //Escribimos en el archivo con el metodo write 
                    escribir.write(cadena+bitacora);

                //Cerramos la conexion
                escribir.close();
                }

                //Si existe un problema al escribir cae aqui
                catch(Exception e)
                {
                    System.out.println("Error al escribir");
                }
            }            
        }                
    }
     
    private void EscribirHistorial(String bitacora) {
        if(ConstantesUSQL.BDActual != null)
        {
            String ruta = ConstantesXML.pathArchivos+"\\BK\\"+ConstantesUSQL.BDActual.Nombre_BD+"_Historial.bk";
            
            File archivo = new File(ruta);
            
            String texto, cadena="";
            BufferedWriter out;
            if(archivo.exists()) {
            try {
                out = new BufferedWriter(new FileWriter(ruta, true));
                out.newLine();
                out.write(bitacora);
                out.close();
            } catch (Exception e) {
                // error processing code  
                System.out.println("Error al escribir");
            }
                
        } else {
                try
                {

                    //Crear objeto FileWriter que sera el que nos ayude a escribir sobre archivo
                    FileWriter escribir=new FileWriter(archivo,true);

                    //Escribimos en el archivo con el metodo write 
                    escribir.write(cadena+bitacora);

                //Cerramos la conexion
                escribir.close();
                }

                //Si existe un problema al escribir cae aqui
                catch(Exception e)
                {
                    System.out.println("Error al escribir");
                }
            }            
        }                
    }
}
