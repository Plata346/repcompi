/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecutarUSQL;

import java.util.ArrayList;
import java.util.Hashtable;
import recorridoXML.Parametro;

/**
 *
 * @author Plata
 */
public class variableObjeto {
    public String nombre;
    public int tipo;
    public Hashtable valores;
    public String fila;
    public String columna;
    public ArrayList<Parametro> LstAtributos = new ArrayList<>();
    
    public variableObjeto( String nombreVar, int tipoVar, Hashtable valorVar, ArrayList<Parametro> lstParametros,String filaVar, String columnaVar)
    {
        nombre = nombreVar;
        tipo = tipoVar;
        valores = valorVar;
        fila = filaVar;
        columna = columnaVar;
        LstAtributos = lstParametros;
    }
}
