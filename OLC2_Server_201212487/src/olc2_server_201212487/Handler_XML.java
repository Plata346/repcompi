/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olc2_server_201212487;

import java.io.File;
import java.io.FileWriter;
import javax.swing.JOptionPane;


/**
 *
 * @author Plata
 */
public class Handler_XML {
    int contador = 0;
    String grafica="";
    boolean value = false;
    public Handler_XML(){
        
    }
    
    public void agregarNumero(interpreteXML.SimpleNode nodo){
        nodo.numero = contador++;
        if(nodo.children != null){
            for (interpreteXML.Node children : nodo.children) {
                interpreteXML.SimpleNode hijo = (interpreteXML.SimpleNode) children;
                agregarNumero(hijo);
            }   
        }
    }
    
    public void iniciarGrafica(interpreteXML.SimpleNode nodo){
        grafica = "digraph g{\n" +
                    "rankdir = TB;\n" +
                    "node [shape = \"rectangle\", style = \"filled\", color = \"\"];\n";
        
        graficarArbol(nodo);
        
        grafica += "}";
    
        crearArchivo("C:\\compi2\\ArbolJJT_XML.txt", grafica);
    }
    
    public void graficarArbol(interpreteXML.SimpleNode itemnodo){

        if (value == false){
            grafica += "\"" + interpreteXML.AnalizadorXMLTreeConstants.jjtNodeName[itemnodo.id] + itemnodo.numero + "\" [label=\"" + interpreteXML.AnalizadorXMLTreeConstants.jjtNodeName[itemnodo.id] + "\"];\n";        
            value = true;
        }

        if(itemnodo.children != null){

            for (interpreteXML.Node children : itemnodo.children){
                interpreteXML.SimpleNode temporal = (interpreteXML.SimpleNode) children;
                
                if (temporal.name != null)
                    grafica += "\"" + interpreteXML.AnalizadorXMLTreeConstants.jjtNodeName[temporal.id] + temporal.numero + "\" [label=\"" + interpreteXML.AnalizadorXMLTreeConstants.jjtNodeName[temporal.id] + " \\n " +  (temporal.name).replace('\"', ' ') + "\"];\n";
                else
                    grafica += "\"" + interpreteXML.AnalizadorXMLTreeConstants.jjtNodeName[temporal.id] + temporal.numero + "\" [label=\"" + interpreteXML.AnalizadorXMLTreeConstants.jjtNodeName[temporal.id] + "\"];\n";

                grafica += interpreteXML.AnalizadorXMLTreeConstants.jjtNodeName[itemnodo.id] + itemnodo.numero + "->" + interpreteXML.AnalizadorXMLTreeConstants.jjtNodeName[temporal.id] + temporal.numero + ";\n";

                graficarArbol(temporal);

                //System.out.println("tipo: " + itemnodo.tipo + ", valor: " + itemnodo.valor + ", contador: " + itemnodo.contador);

            }

        }else{

            //System.out.println("tipo: " + itemnodo.tipo + ", valor: " + itemnodo.valor + ", contador: " + itemnodo.contador);

        }
    }
    
    public void crearArchivo(String nombre, String dato){
        File archivo = new File(nombre);
        try{
        FileWriter escritura = new FileWriter(archivo);
        escritura.write(dato);
        escritura.close();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public void GenerarImagen(){
        
        try {
            String dotPath = "C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
            String archivoEntrada = "C:\\compi2\\ArbolJJT_XML.txt";
            String archivoSalida = "C:\\compi2\\Arbol_XML.jpg";

            String tParam = "-Tjpg";
            String tOParam = "-o";
            String[] cmd = new String[5];
            cmd[0] = dotPath;
            cmd[1] = tParam;
            cmd[2] = archivoEntrada;
            cmd[3] = tOParam;
            cmd[4] = archivoSalida;

            Runtime rt = Runtime.getRuntime();

            rt.exec( cmd );                       
            /*
            try {
                Desktop desktop = Desktop.getDesktop();
                desktop.open(new java.io.File("C:\\Users\\kevin3316\\Pictures\\Proyecto1\\Arbol.jpg"));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error al abrir imagen del arbol: " + e.toString());
            }
            */
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al generar imagen del arbol: " + e.toString());
        }
        
        
    }
}
