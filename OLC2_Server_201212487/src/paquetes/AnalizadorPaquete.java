/* AnalizadorPaquete.java */
/* Generated By:JJTree&JavaCC: Do not edit this line. AnalizadorPaquete.java */
package paquetes; //El nombre del paquete en java (netbeans)
import java.io.*; // Librerias

public class AnalizadorPaquete/*@bgen(jjtree)*/implements AnalizadorPaqueteTreeConstants, AnalizadorPaqueteConstants {/*@bgen(jjtree)*/
  protected JJTAnalizadorPaqueteState jjtree = new JJTAnalizadorPaqueteState();

  final public SimpleNode INICIO() throws ParseException {/*@bgen(jjtree) INICIO */
  SimpleNode jjtn000 = new SimpleNode(JJTINICIO);
  boolean jjtc000 = true;
  jjtree.openNodeScope(jjtn000);
    try {
      PAQUETES();
      jj_consume_token(0);
jjtree.closeNodeScope(jjtn000, true);
                         jjtc000 = false;
{if ("" != null) return jjtn000;}
    } catch (Throwable jjte000) {
if (jjtc000) {
            jjtree.clearNodeScope(jjtn000);
            jjtc000 = false;
          } else {
            jjtree.popNode();
          }
          if (jjte000 instanceof RuntimeException) {
            {if (true) throw (RuntimeException)jjte000;}
          }
          if (jjte000 instanceof ParseException) {
            {if (true) throw (ParseException)jjte000;}
          }
          {if (true) throw (Error)jjte000;}
    } finally {
if (jjtc000) {
            jjtree.closeNodeScope(jjtn000, true);
          }
    }
    throw new Error("Missing return statement in function");
  }

  final public void PAQUETES() throws ParseException {/*@bgen(jjtree) PAQUETES */
  SimpleNode jjtn000 = new SimpleNode(JJTPAQUETES);
  boolean jjtc000 = true;
  jjtree.openNodeScope(jjtn000);
    try {
      if (jj_2_1(5)) {
        PAQUETE_LOGIN();
      } else if (jj_2_2(5)) {
        PAQUETE_FIN();
      } else if (jj_2_3(5)) {
        PAQUETE_INSTRUCCION();
      } else if (jj_2_4(5)) {
        PAQUETE_REPORTE();
      } else {
        jj_consume_token(-1);
        throw new ParseException();
      }
    } catch (Throwable jjte000) {
if (jjtc000) {
            jjtree.clearNodeScope(jjtn000);
            jjtc000 = false;
          } else {
            jjtree.popNode();
          }
          if (jjte000 instanceof RuntimeException) {
            {if (true) throw (RuntimeException)jjte000;}
          }
          if (jjte000 instanceof ParseException) {
            {if (true) throw (ParseException)jjte000;}
          }
          {if (true) throw (Error)jjte000;}
    } finally {
if (jjtc000) {
            jjtree.closeNodeScope(jjtn000, true);
          }
    }
  }

  final public void PAQUETE_LOGIN() throws ParseException {/*@bgen(jjtree) PAQUETE_LOGIN */
  SimpleNode jjtn000 = new SimpleNode(JJTPAQUETE_LOGIN);
  boolean jjtc000 = true;
  jjtree.openNodeScope(jjtn000);
    try {
      jj_consume_token(12);
      jj_consume_token(13);
      jj_consume_token(14);
      NUMERO();
      jj_consume_token(15);
      jj_consume_token(16);
      jj_consume_token(14);
      jj_consume_token(12);
      jj_consume_token(17);
      jj_consume_token(18);
      INST();
      jj_consume_token(19);
      jj_consume_token(19);
    } catch (Throwable jjte000) {
if (jjtc000) {
        jjtree.clearNodeScope(jjtn000);
        jjtc000 = false;
      } else {
        jjtree.popNode();
      }
      if (jjte000 instanceof RuntimeException) {
        {if (true) throw (RuntimeException)jjte000;}
      }
      if (jjte000 instanceof ParseException) {
        {if (true) throw (ParseException)jjte000;}
      }
      {if (true) throw (Error)jjte000;}
    } finally {
if (jjtc000) {
        jjtree.closeNodeScope(jjtn000, true);
      }
    }
  }

  final public void PAQUETE_FIN() throws ParseException {/*@bgen(jjtree) PAQUETE_FIN */
  SimpleNode jjtn000 = new SimpleNode(JJTPAQUETE_FIN);
  boolean jjtc000 = true;
  jjtree.openNodeScope(jjtn000);
    try {
      jj_consume_token(12);
      jj_consume_token(20);
      jj_consume_token(14);
      fin();
      jj_consume_token(19);
    } catch (Throwable jjte000) {
if (jjtc000) {
        jjtree.clearNodeScope(jjtn000);
        jjtc000 = false;
      } else {
        jjtree.popNode();
      }
      if (jjte000 instanceof RuntimeException) {
        {if (true) throw (RuntimeException)jjte000;}
      }
      if (jjte000 instanceof ParseException) {
        {if (true) throw (ParseException)jjte000;}
      }
      {if (true) throw (Error)jjte000;}
    } finally {
if (jjtc000) {
        jjtree.closeNodeScope(jjtn000, true);
      }
    }
  }

  final public void PAQUETE_INSTRUCCION() throws ParseException {/*@bgen(jjtree) PAQUETE_INSTRUCCION */
  SimpleNode jjtn000 = new SimpleNode(JJTPAQUETE_INSTRUCCION);
  boolean jjtc000 = true;
  jjtree.openNodeScope(jjtn000);
    try {
      jj_consume_token(12);
      jj_consume_token(20);
      jj_consume_token(14);
      jj_consume_token(21);
      jj_consume_token(15);
      jj_consume_token(22);
      jj_consume_token(14);
      INST();
      jj_consume_token(19);
    } catch (Throwable jjte000) {
if (jjtc000) {
        jjtree.clearNodeScope(jjtn000);
        jjtc000 = false;
      } else {
        jjtree.popNode();
      }
      if (jjte000 instanceof RuntimeException) {
        {if (true) throw (RuntimeException)jjte000;}
      }
      if (jjte000 instanceof ParseException) {
        {if (true) throw (ParseException)jjte000;}
      }
      {if (true) throw (Error)jjte000;}
    } finally {
if (jjtc000) {
        jjtree.closeNodeScope(jjtn000, true);
      }
    }
  }

  final public void PAQUETE_REPORTE() throws ParseException {/*@bgen(jjtree) PAQUETE_REPORTE */
  SimpleNode jjtn000 = new SimpleNode(JJTPAQUETE_REPORTE);
  boolean jjtc000 = true;
  jjtree.openNodeScope(jjtn000);
    try {
      jj_consume_token(12);
      jj_consume_token(20);
      jj_consume_token(14);
      jj_consume_token(23);
      jj_consume_token(15);
      jj_consume_token(22);
      jj_consume_token(14);
      INST();
      jj_consume_token(19);
    } catch (Throwable jjte000) {
if (jjtc000) {
        jjtree.clearNodeScope(jjtn000);
        jjtc000 = false;
      } else {
        jjtree.popNode();
      }
      if (jjte000 instanceof RuntimeException) {
        {if (true) throw (RuntimeException)jjte000;}
      }
      if (jjte000 instanceof ParseException) {
        {if (true) throw (ParseException)jjte000;}
      }
      {if (true) throw (Error)jjte000;}
    } finally {
if (jjtc000) {
        jjtree.closeNodeScope(jjtn000, true);
      }
    }
  }

  final public void CADENA() throws ParseException {/*@bgen(jjtree) CADENA_TEXTO */
 SimpleNode jjtn000 = new SimpleNode(JJTCADENA_TEXTO);
 boolean jjtc000 = true;
 jjtree.openNodeScope(jjtn000);Token tk;
    try {
      tk = jj_consume_token(CADENA);
jjtree.closeNodeScope(jjtn000, true);
                        jjtc000 = false;
jjtn000.setName(tk.image); jjtn000.linea = tk.beginLine; jjtn000.columna = tk.beginColumn;
    } finally {
if (jjtc000) {
            jjtree.closeNodeScope(jjtn000, true);
          }
    }
  }

  final public void NUMERO() throws ParseException {/*@bgen(jjtree) NUMERO */
 SimpleNode jjtn000 = new SimpleNode(JJTNUMERO);
 boolean jjtc000 = true;
 jjtree.openNodeScope(jjtn000);Token tk;
    try {
      tk = jj_consume_token(NUMERO);
jjtree.closeNodeScope(jjtn000, true);
                        jjtc000 = false;
jjtn000.setName(tk.image); jjtn000.linea = tk.beginLine; jjtn000.columna = tk.beginColumn;
    } finally {
if (jjtc000) {
            jjtree.closeNodeScope(jjtn000, true);
          }
    }
  }

  final public void INST() throws ParseException {/*@bgen(jjtree) INSTRUCCIONES */
 SimpleNode jjtn000 = new SimpleNode(JJTINSTRUCCIONES);
 boolean jjtc000 = true;
 jjtree.openNodeScope(jjtn000);Token tk;
    try {
      tk = jj_consume_token(INST);
jjtree.closeNodeScope(jjtn000, true);
                      jjtc000 = false;
jjtn000.setName(tk.image); jjtn000.linea = tk.beginLine; jjtn000.columna = tk.beginColumn;
    } finally {
if (jjtc000) {
            jjtree.closeNodeScope(jjtn000, true);
          }
    }
  }

  final public void fin() throws ParseException {/*@bgen(jjtree) fin */
 SimpleNode jjtn000 = new SimpleNode(JJTFIN);
 boolean jjtc000 = true;
 jjtree.openNodeScope(jjtn000);Token tk;
    try {
      tk = jj_consume_token(FIN);
jjtree.closeNodeScope(jjtn000, true);
                     jjtc000 = false;
jjtn000.setName(tk.image); jjtn000.linea = tk.beginLine; jjtn000.columna = tk.beginColumn;
    } finally {
if (jjtc000) {
            jjtree.closeNodeScope(jjtn000, true);
          }
    }
  }

  private boolean jj_2_1(int xla)
 {
    jj_la = xla; jj_lastpos = jj_scanpos = token;
    try { return !jj_3_1(); }
    catch(LookaheadSuccess ls) { return true; }
    finally { jj_save(0, xla); }
  }

  private boolean jj_2_2(int xla)
 {
    jj_la = xla; jj_lastpos = jj_scanpos = token;
    try { return !jj_3_2(); }
    catch(LookaheadSuccess ls) { return true; }
    finally { jj_save(1, xla); }
  }

  private boolean jj_2_3(int xla)
 {
    jj_la = xla; jj_lastpos = jj_scanpos = token;
    try { return !jj_3_3(); }
    catch(LookaheadSuccess ls) { return true; }
    finally { jj_save(2, xla); }
  }

  private boolean jj_2_4(int xla)
 {
    jj_la = xla; jj_lastpos = jj_scanpos = token;
    try { return !jj_3_4(); }
    catch(LookaheadSuccess ls) { return true; }
    finally { jj_save(3, xla); }
  }

  private boolean jj_3R_5()
 {
    if (jj_scan_token(NUMERO)) return true;
    return false;
  }

  private boolean jj_3_4()
 {
    if (jj_3R_4()) return true;
    return false;
  }

  private boolean jj_3R_1()
 {
    if (jj_scan_token(12)) return true;
    if (jj_scan_token(13)) return true;
    if (jj_scan_token(14)) return true;
    if (jj_3R_5()) return true;
    if (jj_scan_token(15)) return true;
    return false;
  }

  private boolean jj_3_3()
 {
    if (jj_3R_3()) return true;
    return false;
  }

  private boolean jj_3_2()
 {
    if (jj_3R_2()) return true;
    return false;
  }

  private boolean jj_3R_3()
 {
    if (jj_scan_token(12)) return true;
    if (jj_scan_token(20)) return true;
    if (jj_scan_token(14)) return true;
    if (jj_scan_token(21)) return true;
    if (jj_scan_token(15)) return true;
    return false;
  }

  private boolean jj_3_1()
 {
    if (jj_3R_1()) return true;
    return false;
  }

  private boolean jj_3R_6()
 {
    if (jj_scan_token(FIN)) return true;
    return false;
  }

  private boolean jj_3R_2()
 {
    if (jj_scan_token(12)) return true;
    if (jj_scan_token(20)) return true;
    if (jj_scan_token(14)) return true;
    if (jj_3R_6()) return true;
    if (jj_scan_token(19)) return true;
    return false;
  }

  private boolean jj_3R_4()
 {
    if (jj_scan_token(12)) return true;
    if (jj_scan_token(20)) return true;
    if (jj_scan_token(14)) return true;
    if (jj_scan_token(23)) return true;
    if (jj_scan_token(15)) return true;
    return false;
  }

  /** Generated Token Manager. */
  public AnalizadorPaqueteTokenManager token_source;
  SimpleCharStream jj_input_stream;
  /** Current token. */
  public Token token;
  /** Next token. */
  public Token jj_nt;
  private int jj_ntk;
  private Token jj_scanpos, jj_lastpos;
  private int jj_la;
  private int jj_gen;
  final private int[] jj_la1 = new int[0];
  static private int[] jj_la1_0;
  static {
      jj_la1_init_0();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {};
   }
  final private JJCalls[] jj_2_rtns = new JJCalls[4];
  private boolean jj_rescan = false;
  private int jj_gc = 0;

  /** Constructor with InputStream. */
  public AnalizadorPaquete(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public AnalizadorPaquete(java.io.InputStream stream, String encoding) {
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new AnalizadorPaqueteTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jjtree.reset();
    jj_gen = 0;
    for (int i = 0; i < 0; i++) jj_la1[i] = -1;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Constructor. */
  public AnalizadorPaquete(java.io.Reader stream) {
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new AnalizadorPaqueteTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Reinitialise. */
  public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jjtree.reset();
    jj_gen = 0;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Constructor with generated Token Manager. */
  public AnalizadorPaquete(AnalizadorPaqueteTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  /** Reinitialise. */
  public void ReInit(AnalizadorPaqueteTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jjtree.reset();
    jj_gen = 0;
    for (int i = 0; i < jj_2_rtns.length; i++) jj_2_rtns[i] = new JJCalls();
  }

  private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      if (++jj_gc > 100) {
        jj_gc = 0;
        for (int i = 0; i < jj_2_rtns.length; i++) {
          JJCalls c = jj_2_rtns[i];
          while (c != null) {
            if (c.gen < jj_gen) c.first = null;
            c = c.next;
          }
        }
      }
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }

  @SuppressWarnings("serial")
  static private final class LookaheadSuccess extends java.lang.Error { }
  final private LookaheadSuccess jj_ls = new LookaheadSuccess();
  private boolean jj_scan_token(int kind) {
    if (jj_scanpos == jj_lastpos) {
      jj_la--;
      if (jj_scanpos.next == null) {
        jj_lastpos = jj_scanpos = jj_scanpos.next = token_source.getNextToken();
      } else {
        jj_lastpos = jj_scanpos = jj_scanpos.next;
      }
    } else {
      jj_scanpos = jj_scanpos.next;
    }
    if (jj_rescan) {
      int i = 0; Token tok = token;
      while (tok != null && tok != jj_scanpos) { i++; tok = tok.next; }
      if (tok != null) jj_add_error_token(kind, i);
    }
    if (jj_scanpos.kind != kind) return true;
    if (jj_la == 0 && jj_scanpos == jj_lastpos) throw jj_ls;
    return false;
  }


/** Get the next Token. */
  final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  private int jj_ntk_f() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  private int[] jj_expentry;
  private int jj_kind = -1;
  private int[] jj_lasttokens = new int[100];
  private int jj_endpos;

  private void jj_add_error_token(int kind, int pos) {
    if (pos >= 100) return;
    if (pos == jj_endpos + 1) {
      jj_lasttokens[jj_endpos++] = kind;
    } else if (jj_endpos != 0) {
      jj_expentry = new int[jj_endpos];
      for (int i = 0; i < jj_endpos; i++) {
        jj_expentry[i] = jj_lasttokens[i];
      }
      jj_entries_loop: for (java.util.Iterator<?> it = jj_expentries.iterator(); it.hasNext();) {
        int[] oldentry = (int[])(it.next());
        if (oldentry.length == jj_expentry.length) {
          for (int i = 0; i < jj_expentry.length; i++) {
            if (oldentry[i] != jj_expentry[i]) {
              continue jj_entries_loop;
            }
          }
          jj_expentries.add(jj_expentry);
          break jj_entries_loop;
        }
      }
      if (pos != 0) jj_lasttokens[(jj_endpos = pos) - 1] = kind;
    }
  }

  /** Generate ParseException. */
  public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[24];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 0; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 24; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    jj_endpos = 0;
    jj_rescan_token();
    jj_add_error_token(0, 0);
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  final public void enable_tracing() {
  }

  /** Disable tracing. */
  final public void disable_tracing() {
  }

  private void jj_rescan_token() {
    jj_rescan = true;
    for (int i = 0; i < 4; i++) {
    try {
      JJCalls p = jj_2_rtns[i];
      do {
        if (p.gen > jj_gen) {
          jj_la = p.arg; jj_lastpos = jj_scanpos = p.first;
          switch (i) {
            case 0: jj_3_1(); break;
            case 1: jj_3_2(); break;
            case 2: jj_3_3(); break;
            case 3: jj_3_4(); break;
          }
        }
        p = p.next;
      } while (p != null);
      } catch(LookaheadSuccess ls) { }
    }
    jj_rescan = false;
  }

  private void jj_save(int index, int xla) {
    JJCalls p = jj_2_rtns[index];
    while (p.gen > jj_gen) {
      if (p.next == null) { p = p.next = new JJCalls(); break; }
      p = p.next;
    }
    p.gen = jj_gen + xla - jj_la; p.first = token; p.arg = xla;
  }

  static final class JJCalls {
    int gen;
    Token first;
    int arg;
    JJCalls next;
  }

        //Codigo de java que se quiera incluir en la ejecucion del archivo
}
