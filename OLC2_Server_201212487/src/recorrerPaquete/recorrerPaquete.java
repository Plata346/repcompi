/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recorrerPaquete;

import ejecutarUSQL.ConstantesUSQL;
import olc2_server_201212487.Server_Interface;
import paquetes.AnalizadorPaqueteTreeConstants;
import paquetes.SimpleNode;

/**
 *
 * @author Plata
 */
public class recorrerPaquete {
    
    public String ejecutarPack(SimpleNode nodo){
        
        String respuesta = "";
        String  nombreNodo = AnalizadorPaqueteTreeConstants.jjtNodeName[nodo.id];
        
        switch (nombreNodo){
            case "INICIO":
                respuesta =ejecutarPack((SimpleNode)nodo.children[0]);
                break;
            case "PAQUETES":
                
                respuesta =ejecutarPack((SimpleNode)nodo.children[0]);  
                
                break;
            case "PAQUETE_LOGIN":
                //NUMERO() INST()
                ConstantesUSQL.PaqueteLoguin = true;
                SimpleNode instrucciones= (SimpleNode) nodo.children[1];                
                Server_Interface.RecorrerUsql(instrucciones.name.replace("\'", ""));
                ConstantesUSQL.PaqueteLoguin = false;
                
                return ConstantesUSQL.RespuestaLoguin;
                
            case "PAQUETE_INSTRUCCION":
                //INST
                ConstantesUSQL.CadenaTablas="";
                ConstantesUSQL.CadenaMsj="";
                SimpleNode instruccionesUSQL= (SimpleNode) nodo.children[0];                
                Server_Interface.RecorrerUsql(instruccionesUSQL.name.replace("\'", ""));
                
                return ConstantesUSQL.CadenaMsj+" $ "+ ConstantesUSQL.CadenaTablas;
                
            case "PAQUETE_REPORTE":
                ConstantesUSQL.CadenaTablas="";
                ConstantesUSQL.CadenaMsj="";
                SimpleNode instruccionesUSQLR= (SimpleNode) nodo.children[0];                
                Server_Interface.RecorrerUsql(instruccionesUSQLR.name.replace("\'", ""));
                
                return ConstantesUSQL.CadenaMsj+" $ "+ ConstantesUSQL.CadenaTablas;
                
        }
        return respuesta;
    }
}
