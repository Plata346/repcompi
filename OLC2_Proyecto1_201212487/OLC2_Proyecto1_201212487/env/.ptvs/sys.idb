�}q (X   docqXX  This module provides access to some objects used or maintained by the
interpreter and to functions that interact strongly with the interpreter.

Dynamic objects:

argv -- command line arguments; argv[0] is the script pathname if known
path -- module search path; path[0] is the script directory, else ''
modules -- dictionary of loaded modules

displayhook -- called to show results in an interactive session
excepthook -- called to handle any uncaught exception other than SystemExit
  To customize printing in an interactive session or to install a custom
  top-level exception handler, assign other functions to replace these.

stdin -- standard input file object; used by input()
stdout -- standard output file object; used by print()
stderr -- standard error object; used for error messages
  By assigning other file objects (or objects that behave like files)
  to these, it is possible to redirect all of the interpreter's I/O.

last_type -- type of last uncaught exception
last_value -- value of last uncaught exception
last_traceback -- traceback of last uncaught exception
  These three are only available in an interactive session after a
  traceback has been printed.

Static objects:

builtin_module_names -- tuple of module names built into this interpreter
copyright -- copyright notice pertaining to this interpreter
exec_prefix -- prefix used to find the machine-specific Python library
executable -- absolute path of the executable binary of the Python interpreter
float_info -- a struct sequence with information about the float implementation.
float_repr_style -- string indicating the style of repr() output for floats
hash_info -- a struct sequence with information about the hash algorithm.
hexversion -- version information encoded as a single integer
implementation -- Python implementation information.
int_info -- a struct sequence with information about the int implementation.
maxsize -- the largest supported length of containers.
maxunicode -- the value of the largest Unicode code point
platform -- platform identifier
prefix -- prefix used to find the Python library
thread_info -- a struct sequence with information about the thread implementation.
version -- the version of this interpreter as a string
version_info -- version information as a named tuple
dllhandle -- [Windows only] integer handle of the Python DLL
winver -- [Windows only] version number of the Python DLL
_enablelegacywindowsfsencoding -- [Windows only] 
__stdin__ -- the original stdin; don't touch!
__stdout__ -- the original stdout; don't touch!
__stderr__ -- the original stderr; don't touch!
__displayhook__ -- the original displayhook; don't touch!
__excepthook__ -- the original excepthook; don't touch!

Functions:

displayhook() -- print an object to the screen, and save it in builtins._
excepthook() -- print an exception and its traceback to sys.stderr
exc_info() -- return thread-safe information about the current exception
exit() -- exit the interpreter by raising SystemExit
getdlopenflags() -- returns flags to be used for dlopen() calls
getprofile() -- get the global profiling function
getrefcount() -- return the reference count for an object (plus one :-)
getrecursionlimit() -- return the max recursion depth for the interpreter
getsizeof() -- return the size of an object in bytes
gettrace() -- get the global debug tracing function
setcheckinterval() -- control how often the interpreter checks for events
setdlopenflags() -- set the flags to be used for dlopen() calls
setprofile() -- set the global profiling function
setrecursionlimit() -- set the max recursion depth for the interpreter
settrace() -- set the global debug tracing function
qX   membersq}q(X   thread_infoq}q(X   kindqX   dataqX   valueq	}q
X   typeq]qX   sysqX   thread_infoq�qasuX   base_prefixq}q(hhh	}qh]qX   builtinsqX   strq�qasuX   get_coroutine_wrapperq}q(hX   functionqh	}q(hXc   get_coroutine_wrapper()

Return the wrapper for coroutine objects set by sys.set_coroutine_wrapper.qX	   overloadsq]q}q(X   argsq)hXJ   Return the wrapper for coroutine objects set by sys.set_coroutine_wrapper.q uauuX   _debugmallocstatsq!}q"(hhh	}q#(hX�   _debugmallocstats()

Print summary info to stderr about the state of
pymalloc's structures.

In Py_DEBUG mode, also perform some expensive internal consistency
checks.
q$h]q%}q&(h)hX�   Print summary info to stderr about the state of
pymalloc's structures.

In Py_DEBUG mode, also perform some expensive internal consistency
checks.
q'uauuX   exc_infoq(}q)(hhh	}q*(hX�   exc_info() -> (type, value, traceback)

Return information about the most recent exception caught by an except
clause in the current stack frame or in an older stack frame.q+h]q,(}q-(h)hX�   (type, value, traceback)

Return information about the most recent exception caught by an except
clause in the current stack frame or in an older stack frame.q.X   ret_typeq/]q0X    q1h1�q2au}q3(X   ret_typeq4]q5X   __builtin__q6X   tupleq7�q8aX   argsq9)ueuuX   getswitchintervalq:}q;(hhh	}q<(hXO   getswitchinterval() -> current thread switch interval; see setswitchinterval().q=h]q>(}q?(h)hX   ().q@h/]qAh1X   currentqB�qCau}qD(X   ret_typeqE]qFh6X   floatqG�qHaX   argsqI)ueuuX   __spec__qJ}qK(hhh	}qLh]qMX   _frozen_importlibqNX
   ModuleSpecqO�qPasuX   maxsizeqQ}qR(hhh	}qSh]qT(hX   intqU�qVh6X   intqW�qXesuX   set_asyncgen_hooksqY}qZ(hhh	}q[(hXd   set_asyncgen_hooks(*, firstiter=None, finalizer=None)

Set a finalizer for async generators objects.q\h]q]}q^(h}q_(X   nameq`hX
   arg_formatqaX   *qbu}qc(h`X	   firstiterqdX   default_valueqeX   Noneqfu}qg(h`X	   finalizerqhheX   Noneqiu�qjhX-   Set a finalizer for async generators objects.qkuauuX   __package__ql}qm(hhh	}qnh]qo(hh6X   NoneTypeqp�qqesuX
   hexversionqr}qs(hhh	}qth]qu(hVhXesuX
   __loader__qv}qw(hX   typerefqxh	]qyhNX   BuiltinImporterqz�q{auX
   _mercurialq|}q}(hhh	}q~h]qhX   tupleq��q�asuX	   dllhandleq�}q�(hhh	}q�h]q�(hVhXesuX   warnoptionsq�}q�(hhh	}q�h]q�(hX   listq��q�h6X   listq��q�esuX	   meta_pathq�}q�(hhh	}q�h]q�(h�h�esuX	   hash_infoq�}q�(hhh	}q�h]q�(hX	   hash_infoq��q�X   sysq�X	   hash_infoq��q�esuX   winverq�}q�(hhh	}q�h]q�(hh6X   strq��q�esuX   setcheckintervalq�}q�(hhh	}q�(hX�   setcheckinterval(n)

Tell the Python interpreter to check for asynchronous events every
n instructions.  This also affects how often thread switches occur.q�h]q�(}q�(h}q�h`X   nq�s�q�hX�   Tell the Python interpreter to check for asynchronous events every
n instructions.  This also affects how often thread switches occur.q�u}q�(h4]q�hqah9}q�(X   typeq�]q�hXaX   nameq�X   valueq�u�q�ueuuX   call_tracingq�}q�(hhh	}q�(hX�   call_tracing(func, args) -> object

Call func(*args), while tracing is enabled.  The tracing state is
saved, and restored afterwards.  This is intended to be called from
a debugger from a checkpoint, to recursively debug some other code.q�h]q�(}q�(h}q�h`X   funcq�s}q�h`X   argsq�s�q�hX�   Call func(*args), while tracing is enabled.  The tracing state is
saved, and restored afterwards.  This is intended to be called from
a debugger from a checkpoint, to recursively debug some other code.q�h/]q�hX   objectq��q�au}q�(h4]q�hqah9}q�(h�]q�h6X   objectqƆq�ah�X   funcq�u}q�(h�]q�h8ah�X   argsq�u�q�ueuuX	   copyrightq�}q�(hhh	}q�h]q�(hh�esuX   implementationq�}q�(hhh	}q�h]q�(X   typesq�X   SimpleNamespaceqֆq�h6X   sys.implementationq؆q�esuX   builtin_module_namesq�}q�(hhh	}q�h]q�(h�h8esuX   getwindowsversionq�}q�(hhh	}q�(hX�  getwindowsversion()

Return information about the running version of Windows as a named tuple.
The members are named: major, minor, build, platform, service_pack,
service_pack_major, service_pack_minor, suite_mask, and product_type. For
backward compatibility, only the first 5 items are available by indexing.
All elements are numbers, except service_pack and platform_type which are
strings, and platform_version which is a 3-tuple. Platform is always 2.
Product_type may be 1 for a workstation, 2 for a domain controller, 3 for a
server. Platform_version is a 3-tuple containing a version number that is
intended for identifying the OS rather than feature detection.q�h]q�(}q�(h)hX�  Return information about the running version of Windows as a named tuple.
The members are named: major, minor, build, platform, service_pack,
service_pack_major, service_pack_minor, suite_mask, and product_type. For
backward compatibility, only the first 5 items are available by indexing.
All elements are numbers, except service_pack and platform_type which are
strings, and platform_version which is a 3-tuple. Platform is always 2.
Product_type may be 1 for a workstation, 2 for a domain controller, 3 for a
server. Platform_version is a 3-tuple containing a version number that is
intended for identifying the OS rather than feature detection.q�u}q�(h4]q�h8ah9)ueuuX   _clear_type_cacheq�}q�(hhh	}q�(hXA   _clear_type_cache() -> None
Clear the internal type lookup cache.q�h]q�}q�(h)hX%   Clear the internal type lookup cache.q�h/]q�hX   NoneTypeq�q�auauuX   modulesq�}q�(hhh	}q�h]q�(hX   dictq��q�h6X   dictq��q�esuX   pathq�}q�(hhh	}q�h]q�(h�h�esuX   exec_prefixq�}q�(hhh	}q�h]r   (hh�esuX   setrecursionlimitr  }r  (hhh	}r  (hX�   setrecursionlimit(n)

Set the maximum depth of the Python interpreter stack to n.  This
limit prevents infinite recursion from causing an overflow of the C
stack and crashing Python.  The highest possible limit is platform-
dependent.r  h]r  (}r  (h}r  h`h�s�r  hX�   Set the maximum depth of the Python interpreter stack to n.  This
limit prevents infinite recursion from causing an overflow of the C
stack and crashing Python.  The highest possible limit is platform-
dependent.r	  u}r
  (h4]r  hqah9}r  (h�]r  hXah�X   limitr  u�r  ueuuX
   maxunicoder  }r  (hhh	}r  h]r  (hVhXesuX
   setprofiler  }r  (hhh	}r  (hX�   setprofile(function)

Set the profiling function.  It will be called on each function call
and return.  See the profiler chapter in the library manual.r  h]r  }r  (h}r  h`X   functionr  s�r  hX�   Set the profiling function.  It will be called on each function call
and return.  See the profiler chapter in the library manual.r  uauuX   exitr  }r  (hhh	}r   (hX>  exit([status])

Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).r!  h]r"  (}r#  (h}r$  (h`X   statusr%  heX   Noner&  u�r'  hX.  Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).r(  u}r)  (h4]r*  hqah9)u}r+  (h4]r,  hqah9}r-  (h�]r.  h�ah�X   coder/  u�r0  ueuuX   get_asyncgen_hooksr1  }r2  (hhh	}r3  (hXl   get_asyncgen_hooks()

Return a namedtuple of installed asynchronous generators hooks (firstiter, finalizer).r4  h]r5  }r6  (h)hXV   Return a namedtuple of installed asynchronous generators hooks (firstiter, finalizer).r7  uauuX
   executabler8  }r9  (hhh	}r:  h]r;  (hh�esuX	   __stdin__r<  }r=  (hhh	}r>  h]r?  (X   _ior@  X   TextIOWrapperrA  �rB  h6X   filerC  �rD  esuX	   _xoptionsrE  }rF  (hhh	}rG  h]rH  (h�h�esuX
   float_inforI  }rJ  (hhh	}rK  h]rL  (hX
   float_inforM  �rN  X   sysrO  X   sys.float_inforP  �rQ  esuX   __excepthook__rR  }rS  (hhh	}rT  (hXt   excepthook(exctype, value, traceback) -> None

Handle an exception by displaying it with a traceback on sys.stderr.
rU  h]rV  (}rW  (h}rX  h`X   exctyperY  s}rZ  h`X   valuer[  s}r\  h`X	   tracebackr]  s�r^  hXE   Handle an exception by displaying it with a traceback on sys.stderr.
r_  h/]r`  h�au}ra  (h4]rb  hqah9}rc  (h�]rd  h�ah�X   exctypere  u}rf  (h�]rg  h�ah�X   valuerh  u}ri  (h�]rj  h�ah�X	   tracebackrk  u�rl  ueuuX	   byteorderrm  }rn  (hhh	}ro  h]rp  (hh�esuX   stderrrq  }rr  (hhh	}rs  h]rt  (jB  jD  esuX   argvru  }rv  (hhh	}rw  h]rx  (h�h�esuX   getrecursionlimitry  }rz  (hhh	}r{  (hX�   getrecursionlimit()

Return the current value of the recursion limit, the maximum depth
of the Python interpreter stack.  This limit prevents infinite
recursion from causing an overflow of the C stack and crashing Python.r|  h]r}  (}r~  (h)hX�   Return the current value of the recursion limit, the maximum depth
of the Python interpreter stack.  This limit prevents infinite
recursion from causing an overflow of the C stack and crashing Python.r  u}r�  (h4]r�  hXah9)ueuuX   flagsr�  }r�  (hhh	}r�  h]r�  (hX   flagsr�  �r�  jO  X   flagsr�  �r�  esuX   getallocatedblocksr�  }r�  (hhh	}r�  (hXr   getallocatedblocks() -> integer

Return the number of memory blocks currently allocated, regardless of their
size.r�  h]r�  }r�  (h)hXQ   Return the number of memory blocks currently allocated, regardless of their
size.r�  h/]r�  hX   intr�  �r�  auauuX   gettracer�  }r�  (hhh	}r�  (hX{   gettrace()

Return the global debug tracing function set with sys.settrace.
See the debugger chapter in the library manual.r�  h]r�  (}r�  (h)hXo   Return the global debug tracing function set with sys.settrace.
See the debugger chapter in the library manual.r�  u}r�  (h4]r�  h�ah9)ueuuX   getrefcountr�  }r�  (hhh	}r�  (hX�   getrefcount(object) -> integer

Return the reference count of object.  The count returned is generally
one higher than you might expect, because it includes the (temporary)
reference as an argument to getrefcount().r�  h]r�  (}r�  (h}r�  h`X   objectr�  s�r�  hX�   Return the reference count of object.  The count returned is generally
one higher than you might expect, because it includes the (temporary)
reference as an argument to getrefcount().r�  h/]r�  j�  au}r�  (h4]r�  hqah9)ueuuX   internr�  }r�  (hhh	}r�  (hX   intern(string) -> string

``Intern'' the given string.  This enters the string in the (global)
table of interned strings whose purpose is to speed up dictionary lookups.
Return the string itself or the previously interned string object with the
same value.r�  h]r�  (}r�  (h}r�  h`X   stringr�  s�r�  hX�   Intern'' the given string.  This enters the string in the (global)
table of interned strings whose purpose is to speed up dictionary lookups.
Return the string itself or the previously interned string object with the
same value.r�  h/]r�  h1X
   string

``r�  �r�  au}r�  (hE]r�  h�ahI}r�  (X   typer�  ]r�  h�aX   namer�  X   stringr�  u�r�  ueuuX   __displayhook__r�  }r�  (hhh	}r�  (hXZ   displayhook(object) -> None

Print an object to sys.stdout and also save it in builtins._
r�  h]r�  (}r�  (h}r�  h`X   objectr�  s�r�  hX=   Print an object to sys.stdout and also save it in builtins._
r�  h/]r�  h�au}r�  (h4]r�  hqah9}r�  (h�]r�  h�ah�X   valuer�  u�r�  ueuuX
   getprofiler�  }r�  (hhh	}r�  (hXt   getprofile()

Return the profiling function set with sys.setprofile.
See the profiler chapter in the library manual.r�  h]r�  }r�  (h)hXf   Return the profiling function set with sys.setprofile.
See the profiler chapter in the library manual.r�  uauuX   set_coroutine_wrapperr�  }r�  (hhh	}r�  (hXD   set_coroutine_wrapper(wrapper)

Set a wrapper for coroutine objects.r�  h]r�  }r�  (h}r�  h`X   wrapperr�  s�r�  hX$   Set a wrapper for coroutine objects.r�  uauuX   float_repr_styler�  }r�  (hhh	}r�  h]r�  hasuX   platformr�  }r�  (hhh	}r�  h]r�  (hh�esuX   path_importer_cacher�  }r�  (hhh	}r�  h]r�  (h�h�esuX   dont_write_bytecoder�  }r�  (hhh	}r�  h]r�  (hX   boolr�  �r�  h6X   boolr�  �r�  esuX   prefixr�  }r�  (hhh	}r�  h]r�  (hh�esuX   versionr�  }r�  (hhh	}r�  h]r�  (hh�esuX	   callstatsr�  }r�  (hhh	}r�  (hX�  callstats() -> tuple of integers

Return a tuple of function call statistics, if CALL_PROFILE was defined
when Python was built.  Otherwise, return None.

When enabled, this function returns detailed, implementation-specific
details about the number of function calls executed. The return value is
a 11-tuple where the entries in the tuple are counts of:
0. all function calls
1. calls to PyFunction_Type objects
2. PyFunction calls that do not create an argument tuple
3. PyFunction calls that do not create an argument tuple
   and bypass PyEval_EvalCodeEx()
4. PyMethod calls
5. PyMethod calls on bound methods
6. PyType calls
7. PyCFunction calls
8. generator calls
9. All other calls
10. Number of stack pops performed by call_function()r   h]r  (}r  (h)hX�  Return a tuple of function call statistics, if CALL_PROFILE was defined
when Python was built.  Otherwise, return None.

When enabled, this function returns detailed, implementation-specific
details about the number of function calls executed. The return value is
a 11-tuple where the entries in the tuple are counts of:
0. all function calls
1. calls to PyFunction_Type objects
2. PyFunction calls that do not create an argument tuple
3. PyFunction calls that do not create an argument tuple
   and bypass PyEval_EvalCodeEx()
4. PyMethod calls
5. PyMethod calls on bound methods
6. PyType calls
7. PyCFunction calls
8. generator calls
9. All other calls
10. Number of stack pops performed by call_function()r  h/]r  hX   tupler  �r  au}r  (h4]r  h�ah9)ueuuX   getfilesystemencodingr	  }r
  (hhh	}r  (hXw   getfilesystemencoding() -> string

Return the encoding used to convert Unicode filenames in
operating system filenames.r  h]r  (}r  (h)hXT   Return the encoding used to convert Unicode filenames in
operating system filenames.r  h/]r  hX   strr  �r  au}r  (h4]r  h�ah9)ueuuX   base_exec_prefixr  }r  (hhh	}r  h]r  hasuX   int_infor  }r  (hhh	}r  h]r  (hX   int_infor  �r  X   sysr  X   int_infor   �r!  esuX   version_infor"  }r#  (hhh	}r$  h]r%  (hX   version_infor&  �r'  h6X   sys.version_infor(  �r)  esuX   getfilesystemencodeerrorsr*  }r+  (hhh	}r,  (hX}   getfilesystemencodeerrors() -> string

Return the error mode used to convert Unicode filenames in
operating system filenames.r-  h]r.  }r/  (h)hXV   Return the error mode used to convert Unicode filenames in
operating system filenames.r0  h/]r1  j  auauuX   api_versionr2  }r3  (hhh	}r4  h]r5  (hVhXesuX   displayhookr6  }r7  (hhh	}r8  (hXZ   displayhook(object) -> None

Print an object to sys.stdout and also save it in builtins._
r9  h]r:  (}r;  (h}r<  h`X   objectr=  s�r>  hX=   Print an object to sys.stdout and also save it in builtins._
r?  h/]r@  h�au}rA  (h4]rB  hqah9}rC  (h�]rD  h�ah�j�  u�rE  ueuuX
   path_hooksrF  }rG  (hhh	}rH  h]rI  (h�h�esuX   _enablelegacywindowsfsencodingrJ  }rK  (hhh	}rL  (hX  _enablelegacywindowsfsencoding()

Changes the default filesystem encoding to mbcs:replace for consistency
with earlier versions of Python. See PEP 529 for more information.

This is equivalent to defining the PYTHONLEGACYWINDOWSFSENCODING 
environment variable before launching Python.rM  h]rN  }rO  (h)hX�   Changes the default filesystem encoding to mbcs:replace for consistency
with earlier versions of Python. See PEP 529 for more information.

This is equivalent to defining the PYTHONLEGACYWINDOWSFSENCODING 
environment variable before launching Python.rP  uauuX
   __stderr__rQ  }rR  (hhh	}rS  h]rT  (jB  jD  esuX   __name__rU  }rV  (hhh	}rW  h]rX  (hh�esuX
   __stdout__rY  }rZ  (hhh	}r[  h]r\  (jB  jD  esuX	   _getframer]  }r^  (hhh	}r_  (hX�  _getframe([depth]) -> frameobject

Return a frame object from the call stack.  If optional integer depth is
given, return the frame object that many calls below the top of the stack.
If that is deeper than the call stack, ValueError is raised.  The default
for depth is zero, returning the frame at the top of the call stack.

This function should be used for internal and specialized
purposes only.r`  h]ra  }rb  (h}rc  (h`X   depthrd  hej&  u�re  hXl  Return a frame object from the call stack.  If optional integer depth is
given, return the frame object that many calls below the top of the stack.
If that is deeper than the call stack, ValueError is raised.  The default
for depth is zero, returning the frame at the top of the call stack.

This function should be used for internal and specialized
purposes only.rf  h/]rg  h1X   frameobjectrh  �ri  auauuX   setswitchintervalrj  }rk  (hhh	}rl  (hX|  setswitchinterval(n)

Set the ideal thread switching delay inside the Python interpreter
The actual frequency of switching threads can be lower if the
interpreter executes long sequences of uninterruptible code
(this is implementation-specific and workload-dependent).

The parameter must represent the desired switching delay in seconds
A typical value is 0.005 (5 milliseconds).rm  h]rn  (}ro  (h}rp  h`h�s�rq  hXf  Set the ideal thread switching delay inside the Python interpreter
The actual frequency of switching threads can be lower if the
interpreter executes long sequences of uninterruptible code
(this is implementation-specific and workload-dependent).

The parameter must represent the desired switching delay in seconds
A typical value is 0.005 (5 milliseconds).rr  u}rs  (hE]rt  hqahI}ru  (j�  ]rv  hHaj�  h�u�rw  ueuuX   stdoutrx  }ry  (hhh	}rz  h]r{  (jB  jD  esuX   stdinr|  }r}  (hhh	}r~  h]r  (jB  jD  esuX   __interactivehook__r�  }r�  (hhh	}r�  hNsuX
   excepthookr�  }r�  (hhh	}r�  (hXt   excepthook(exctype, value, traceback) -> None

Handle an exception by displaying it with a traceback on sys.stderr.
r�  h]r�  (}r�  (h}r�  h`X   exctyper�  s}r�  h`X   valuer�  s}r�  h`X	   tracebackr�  s�r�  hXE   Handle an exception by displaying it with a traceback on sys.stderr.
r�  h/]r�  h�au}r�  (h4]r�  hqah9}r�  (h�]r�  h�ah�je  u}r�  (h�]r�  h�ah�jh  u}r�  (h�]r�  h�ah�jk  u�r�  ueuuX   getcheckintervalr�  }r�  (hhh	}r�  (hXE   getcheckinterval() -> current check interval; see setcheckinterval().r�  h]r�  (}r�  (h)hX   ().r�  h/]r�  hCau}r�  (h4]r�  hXah9)ueuuX	   getsizeofr�  }r�  (hhh	}r�  (hXF   getsizeof(object, default) -> int

Return the size of object in bytes.r�  h]r�  (}r�  (h}r�  h`X   objectr�  s}r�  h`X   defaultr�  s�r�  hX#   Return the size of object in bytes.r�  h/]r�  j�  au}r�  (h4]r�  hXah9}r�  (h�]r�  h�ah�X   or�  u�r�  ueuuX   _current_framesr�  }r�  (hhh	}r�  (hX�   _current_frames() -> dictionary

Return a dictionary mapping each current thread T's thread id to T's
current stack frame.

This function should be used for specialized purposes only.r�  h]r�  }r�  (h)hX�   Return a dictionary mapping each current thread T's thread id to T's
current stack frame.

This function should be used for specialized purposes only.r�  h/]r�  hX   dictr�  �r�  auauuX   _homer�  }r�  (hhh	}r�  h]r�  hasuX   is_finalizingr�  }r�  (hhh	}r�  (hX1   is_finalizing()
Return True if Python is exiting.r�  h]r�  }r�  (h)hX!   Return True if Python is exiting.r�  uauuX   getdefaultencodingr�  }r�  (hhh	}r�  (hXo   getdefaultencoding() -> string

Return the current default string encoding used by the Unicode 
implementation.r�  h]r�  (}r�  (h)hXO   Return the current default string encoding used by the Unicode 
implementation.r�  h/]r�  j  au}r�  (h4]r�  h�ah9)ueuuX   settracer�  }r�  (hhh	}r�  (hX�   settrace(function)

Set the global debug tracing function.  It will be called on each
function call.  See the debugger chapter in the library manual.r�  h]r�  (}r�  (h}r�  h`X   functionr�  s�r�  hX�   Set the global debug tracing function.  It will be called on each
function call.  See the debugger chapter in the library manual.r�  u}r�  (h4]r�  hqah9}r�  (h�]r�  h�ah�j�  u�r�  ueuuX   __doc__r�  }r�  (hhh	}r�  h]r�  (hh�esuhz}r�  (hhh	}r�  (X   mror�  ]r�  (h{hX   objectr�  �r�  eX   basesr�  ]r�  j�  ahX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  X	   is_hiddenr�  �h}r�  (X
   __reduce__r�  }r�  (hX   methodr�  h	}r�  (hX   helper for pickler�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`X   kwargsr�  haX   **r�  u�r   hX   helper for pickler  uauuX   __setattr__r  }r  (hj�  h	}r  (hX%   Implement setattr(self, name, value).r  h]r  }r  (h}r  (h`hhahbu}r	  (h`j�  haj�  u�r
  hX%   Implement setattr(self, name, value).r  uauuX   exec_moduler  }r  (hhh	}r  (hX   Exec a built-in moduler  h]r  }r  (h}r  (h`hhahbu}r  (h`j�  haj�  u�r  hX   Exec a built-in moduler  uauuX   __dict__r  }r  (hhh	}r  h]r  hX   mappingproxyr  �r  asuX   __gt__r  }r  (hj�  h	}r  (hX   Return self>value.r  h]r   }r!  (h}r"  (h`hhahbu}r#  (h`j�  haj�  u�r$  hX   Return self>value.r%  uauuX   __repr__r&  }r'  (hj�  h	}r(  (hX   Return repr(self).r)  h]r*  }r+  (h}r,  (h`hhahbu}r-  (h`j�  haj�  u�r.  hX   Return repr(self).r/  uauuX   __str__r0  }r1  (hj�  h	}r2  (hX   Return str(self).r3  h]r4  }r5  (h}r6  (h`hhahbu}r7  (h`j�  haj�  u�r8  hX   Return str(self).r9  uauuX   __delattr__r:  }r;  (hj�  h	}r<  (hX   Implement delattr(self, name).r=  h]r>  }r?  (h}r@  (h`hhahbu}rA  (h`j�  haj�  u�rB  hX   Implement delattr(self, name).rC  uauuX
   __module__rD  }rE  (hhh	}rF  h]rG  hasuX
   __format__rH  }rI  (hj�  h	}rJ  (hX   default object formatterrK  h]rL  }rM  (h}rN  (h`hhahbu}rO  (h`j�  haj�  u�rP  hX   default object formatterrQ  uauuX   __new__rR  }rS  (hhh	}rT  (hXG   Create and return a new object.  See help(type) for accurate signature.rU  h]rV  }rW  (h}rX  (h`hhahbu}rY  (h`j�  haj�  u�rZ  hXG   Create and return a new object.  See help(type) for accurate signature.r[  uauuX   get_coder\  }r]  (hhh	}r^  (hX9   Return None as built-in modules do not have code objects.r_  h]r`  }ra  (h}rb  (h`hhahbu}rc  (h`j�  haj�  u�rd  hX9   Return None as built-in modules do not have code objects.re  uauuX   __ne__rf  }rg  (hj�  h	}rh  (hX   Return self!=value.ri  h]rj  }rk  (h}rl  (h`hhahbu}rm  (h`j�  haj�  u�rn  hX   Return self!=value.ro  uauuX   __le__rp  }rq  (hj�  h	}rr  (hX   Return self<=value.rs  h]rt  }ru  (h}rv  (h`hhahbu}rw  (h`j�  haj�  u�rx  hX   Return self<=value.ry  uauuX   find_modulerz  }r{  (hhh	}r|  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r}  h]r~  }r  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  uauuX	   __class__r�  }r�  (hhxh	]r�  hX   typer�  �r�  auX   __reduce_ex__r�  }r�  (hj�  h	}r�  (hX   helper for pickler�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX   helper for pickler�  uauuX	   find_specr�  }r�  (hhh	}r�  h]r�  hX   methodr�  �r�  asuX   create_moduler�  }r�  (hhh	}r�  (hX   Create a built-in moduler�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX   Create a built-in moduler�  uauuX   __ge__r�  }r�  (hj�  h	}r�  (hX   Return self>=value.r�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX   Return self>=value.r�  uauuX   __subclasshook__r�  }r�  (hhh	}r�  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  uauuX
   is_packager�  }r�  (hhh	}r�  (hX4   Return False as built-in modules are never packages.r�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX4   Return False as built-in modules are never packages.r�  uauuX   __weakref__r�  }r�  (hX   propertyr�  h	}r�  (hX2   list of weak references to the object (if defined)r�  h]r�  j�  auuX
   get_sourcer�  }r�  (hhh	}r�  (hX8   Return None as built-in modules do not have source code.r�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX8   Return None as built-in modules do not have source code.r�  uauuX   __hash__r�  }r�  (hj�  h	}r�  (hX   Return hash(self).r�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX   Return hash(self).r�  uauuX   module_reprr�  }r�  (hhh	}r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  uauuX   __dir__r�  }r�  (hj�  h	}r�  (hX.   __dir__() -> list
default dir() implementationr�  h]r�  }r�  (h}r�  (h]r�  h�ah`X   selfr�  u�r�  hX   default dir() implementationr�  h/]r�  hX   listr�  �r�  auauuX   load_moduler�  }r�  (hhh	}r�  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  h]r�  }r�  (h}r�  (h`hhahbu}r�  (h`j�  haj�  u�r�  hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  uauuX
   __sizeof__r�  }r�  (hj�  h	}r�  (hX6   __sizeof__() -> int
size of object in memory, in bytesr   h]r  }r  (h}r  (h]r  h�ah`j�  u�r  hX"   size of object in memory, in bytesr  h/]r  j�  auauuX   __lt__r  }r	  (hj�  h	}r
  (hX   Return self<value.r  h]r  }r  (h}r  (h`hhahbu}r  (h`j�  haj�  u�r  hX   Return self<value.r  uauuj�  }r  (hhh	}r  h]r  hasuX   __init_subclass__r  }r  (hhh	}r  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r  h]r  }r  (h}r  (h`hhahbu}r  (h`j�  haj�  u�r  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r  uauuX   __eq__r  }r   (hj�  h	}r!  (hX   Return self==value.r"  h]r#  }r$  (h}r%  (h`hhahbu}r&  (h`j�  haj�  u�r'  hX   Return self==value.r(  uauuX   __init__r)  }r*  (hj�  h	}r+  (hX>   Initialize self.  See help(type(self)) for accurate signature.r,  h]r-  }r.  (h}r/  (h`hhahbu}r0  (h`j�  haj�  u�r1  hX>   Initialize self.  See help(type(self)) for accurate signature.r2  uauuuuuuu.