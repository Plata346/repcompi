from django import forms

class UsuarioForm():
    
    class Meta:
        
        fields = [
            'nombre',
            'clave',
            'msj',
            'msjHtml',
        ]
        labels ={
            'nombre' : 'Nombre',
            'clave' : 'Contraseña',
            'msj' : 'Mensaje',
            'msjHtml' : 'MensajeHTML',
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'clave' : forms.TextInput(attrs={'type':'password', 'class':'form-control'}),
            'msj' :  forms.TextInput(attrs={'class':'form-control'}),
            'msjHtml' :  forms.TextInput(attrs={'class':'form-control'}),            
        }
