from django.conf.urls import include, url

from Inicio.views import PaginaInicio, Editor, ValidarUsuario, EditorTexto, enviarServer

urlpatterns = [    
    url(r'^$', PaginaInicio, name='login'),
    #recibir parametros en una vista
    url(r'^editor/(?P<usuario_acceso>\w{0,30})/(?P<clave>\w{0,30})/$', Editor, name='Editor'),
    url(r'^editor2$', ValidarUsuario, name='ValidarUsuario'),
    url(r'^EditorTexto$', EditorTexto, name='EditorTexto'),
    url(r'^enviarServer$', enviarServer, name='enviarServer'),
]

