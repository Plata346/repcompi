from django.shortcuts import render, redirect
from django.http import HttpResponse

from Inicio.usuario import UsuarioForm


import socket


# Create your views here.

#vista para el login
def PaginaInicio(request):
    return render(request, 'Inicio/templates/index.html')

# vista para el editor de texto, recibe dos parametros
def Editor(request, usuario_acceso, clave):
    if request.method == 'GET':
        form = UsuarioForm()
        form.nombre = usuario_acceso
        form.clave = clave

        return render(request,'EditorTexto.html',{'form':form})
    
            
def ValidarUsuario(request):
    
    try:
        if (request.method == 'POST'):
            nombre = request.POST['usuario_acceso']
            clave = request.POST['clave']                 
        
            cadenaPaquete = "[ validar : 346, login : [ comando => ' USAR Administracion; SELECCIONAR * DE Usuarios DONDE nombre==\""+nombre+"\" && clave == \""+clave+"\";' ] ]"

            respuesta = enviarAServer(cadenaPaquete)
            strRespues = respuesta.decode('utf-8')
            vector = strRespues.split("$")
            valor = vector[1]

            if valor == 'OK':
                return redirect('Editor', nombre, clave)
            else:
                form = UsuarioForm()
                form.msj = "Usuario o Contraseña incorrectos"

                return render(request, 'index.html',{'form':form})
        
                #para mandar los parametros y redireccionar a otro metodo
                #return redirect('Editor', nombre, clave)        
        else:
            return render(request, 'index.html')
    
    except:
        form = UsuarioForm()
        form.msj = "No hay conexion al servidor."

        return render(request, 'index.html',{'form':form})
                

def enviarAServer(cadena):

    # CLIENTE PYTHON
    HOST = 'localhost'    # El host remoto
    PORT = 5000              # El puerto de escucha del servidor
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))                
        s.sendall(bytes(cadena, 'utf-8'))
        data = s.recv(1024*1024)
        s.close()
        print('Recibido ', repr(data))

    return data

def EditorTexto(request):
    if request.method == 'GET':
        form = UsuarioForm()
        form.nombre = "pepe"
        form.clave = "popo"

    return render(request,'EditorTexto.html',{'form':form})


    

def enviarServer(request):
    try:
        usuario = request.POST['usuario']
        TextoEditor = request.POST['TextoEditor']
        
        if TextoEditor != "":
            
            #Formar paquete USQL
            paquete = "[ paquete : usql , instruccion : '"+TextoEditor+"']"

            respuesta = enviarAServer(paquete)
            strRespues = str(respuesta)
            vector = strRespues.split("$")
            valor = vector[1]
            valorTablas = vector[2]

            VALORES = str(valor)
            vValores1 = VALORES.split("%")

            VALORES2 = str(valorTablas)
            vValores2 = VALORES2.split("%")

            CadenaResultados = ""
            CadenaResultados2 = ""

            for val in vValores1:
                CadenaResultados += val + "\n"

            for x in range(0, len(vValores2)):
                CadenaResultados2 += vValores2[x] + "\n"

            #ver que retorna
            
            if valor == 'OK':
                return redirect('Editor', nombre, clave)
            else:
                form = UsuarioForm()
                form.nombre = usuario
                form.msj = CadenaResultados 
                form.msjHtml = CadenaResultados2;

                return render(request,'EditorTexto.html',{'form':form})

            #return render(request,'EditorTexto.html',{'form':form})
        else:
            form = UsuarioForm()
            form.nombre = usuario
            form.msj = "Ingrese Una Instruccion."

            return render(request,'EditorTexto.html',{'form':form})
    except:
        form = UsuarioForm()
        form.msj = "No hay conexion al servidor."

        return render(request, 'EditorTexto.html',{'form':form})

    