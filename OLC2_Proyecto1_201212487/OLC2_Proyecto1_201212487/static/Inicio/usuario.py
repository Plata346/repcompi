from django import forms

class UsuarioForm():
    
    class Meta:
        
        fields = [
            'nombre',
            'clave',
            'msj',
        ]
        labels ={
            'nombre' : 'Nombre',
            'clave' : 'Contraseña',
            'msj' : 'Mensaje',
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'clave' : forms.TextInput(attrs={'type':'password', 'class':'form-control'}),
            'msj' :  forms.TextInput(attrs={'class':'form-control'}),
        }
